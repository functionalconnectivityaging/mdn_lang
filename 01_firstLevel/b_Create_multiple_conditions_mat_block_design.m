%% This script creates a mat-File for multiple conditions 1st level design for a block design 
% written by Sandra Martin for MDN_LANG
% 10.12.2018
% This script reads the VP*_run_*_log_b.mat that was created in the "prepare logfiles for matlab" function
% and gets names, onsets and durations for SPM.


%% Define important paths
data_dir = '/data/pt_02004/MDN_LANG/Derivatives/subjects/';

% sub = {'008','018'};
sub = {'024'};
% sub = {'002','003','004','005','006','007','008','009','011', ...
%        '012','013','014','015','016','017','018','019', ...
%         '021','022','023', '024','025','027','028','029','030','031'};
% In this experiment and script participant 031 has to be processed separately since one category was switched (see easy categories!)

run = {'2'}; %'2' - cannot be together in one loop due to different indexes for first pulse in logfiles


%% Start looping over subjects and runs
for i = 1:numel(sub)
for x = 1:numel(run)
  
% Display which participant and which run is currently processed
X = ['This is participant ' sub{i} ' and run ' run{x}];
    disp(X);
    
% Define the folders where the logfiles can be found
logfolder = [data_dir sub{i} '/logfiles/'];

% Load b struct, in which the information about the onset times is stored
matFile = dir([logfolder sub{i} '_run-' run{x} '_log_b.mat']);
mat = [matFile.folder '/' matFile.name];
load(mat);

% find row of first pulse after the prepvols which in MDN_LANG with 29
% prepVols is the last pulse before WAIT_for_Pulse
% for run-1: scan number 37, for run-2: scan number 33
firstPulse_idx = +33; % für run-1: 37; für run-2: 33
% get time of first pulse
firstPulse_time = b.time(firstPulse_idx);
disp('I used this index to find the first pulse: ');
disp(firstPulse_idx);

% % load the txt file with reaction (onset) times (averaged per block)
% csvfile = dir([logfolder sub{i} '_AE_run' run{x} '.csv']);
% csv = [csvfile.folder '/' csvfile.name];
% fileID = fopen(csv, 'r');
% ownlog = textscan(fileID, '%s %s %s', 'HeaderLines', 1, 'Delimiter', ',');
% names = {'Category', 'CatNumber', 'Avg'};
% ownlog_struct = cell2struct(ownlog, names, 2);
% % ownlog_struct.response_time = ownlog_struct.response_time * 10;
% % ownlog_struct.response_time = num2cell(ownlog_struct.response_time);
% ownlog_struct.Category = strrep(ownlog_struct.Category, 'Baeume', 'Bäume');
% ownlog_struct.Category = strrep(ownlog_struct.Category, 'Fruechte', 'Früchte');
% ownlog_struct.Category = strrep(ownlog_struct.Category, 'Aufwaerts zaehlen von 1', 'Aufwärts zählen von 1');
% ownlog_struct.Category = strrep(ownlog_struct.Category, 'Abwaerts zaehlen von 9', 'Abwärts zählen von 9');
% ownlog_struct.Category = strrep(ownlog_struct.Category, 'Suessigkeiten', 'Süßigkeiten');
% ownlog_struct.Category = strrep(ownlog_struct.Category, 'Gartengeraete', 'Gartengeräte');
% ownlog_struct.Category = strrep(ownlog_struct.Category, 'Kleidungsstuecke', 'Kleidungsstücke');
% ownlog_struct.Category = strrep(ownlog_struct.Category, 'Koerperteile', 'Körperteile');
% ownlog_struct.Category = strrep(ownlog_struct.Category, 'Kuechengeraete', 'Küchengeräte');
            
%% Find indexes, get event times and calculate stimulus onsets for different conditions

% find rows where counting forward was presented
% get times of events
% calculate picture onsets with respect to first pulse
count_forw = contains(b.event_type, 'Picture') & contains(b.code, 'Aufwärts zählen von 1');
count_forw_time = b.time(count_forw);
count_forw_onsets = (count_forw_time - firstPulse_time)/10000; 


% find rows where counting backward was presented
% get times of events
% calculate picture onsets with respect to first pulse
count_backw = contains(b.event_type, 'Picture') & contains(b.code, 'Abwärts zählen von 9');
count_backw_time = b.time(count_backw);
count_backw_onsets = (count_backw_time - firstPulse_time)/10000; 


% find rows where easy categories were presented
% get times of events
% calculate picture onsets with respect to first pulse
% Careful: 031 & 016 have 'Früchte' instead of 'Bäume'
easy_cats = {'Autoteile', 'Berufe', 'Farben', 'Bäume', 'Kleidungsstücke', 'Körperteile', 'Lebensmittel', 'Musikinstrumente'... 
    'Sportarten', 'Tiere', 'Früchte'};
cat_easy = contains(b.event_type, 'Picture') & ismember(b.code, easy_cats);
cat_easy_time = b.time(cat_easy);
cat_easy_onsets = (cat_easy_time - firstPulse_time)/10000; 


% find rows where difficult categories were presented
% get times of events
% calculate picture onsets with respect to first pulse
diff_cats = {'Gartengeräte', 'Fische', 'Blumen', 'Insekten', 'Kosmetik', 'Küchengeräte', 'Metalle', 'Spielzeug' ...
    'Süßigkeiten', 'Werkzeug'};
cat_diff = contains(b.event_type, 'Picture') & ismember(b.code, diff_cats);
cat_diff_time = b.time(cat_diff);
cat_diff_onsets = (cat_diff_time - firstPulse_time)/10000;


%% Write onsets, names and durations of conditions in vector for SPM (Have to be named 'onsets', 'names', 'durations' for SPM)
onsets = {cat_diff_onsets, cat_easy_onsets, count_forw_onsets, count_backw_onsets};
names = {'Cat_diff', 'Cat_easy', 'Count_forw', 'Count_backw'};
durations = {43, 43, 43, 43}; 
      
%% Save names,onsets,durations in onsets_explicit_stimduration.mat and the parametric modulation of RT as pmod.mat     
 save([logfolder sub{i} '_run-' run{x} '_1st_level_block_design_w.oRest' '.mat'],'names', 'durations', 'onsets');            
                
end
end
               
