%% Prepare logfiles from Presentation for SPM and Matlab
% This function gets the logfiles of all participants for session * and imports them with the
% importPresentationLog function found on the internet (renamed to ..._orig). The output of this
% function is saved as sub*_run*_log_a/b.mat in the subject logfile directory and
% can be accessed to get names, onsets and durations for SPM.


%display what is done at the moment
disp ('get logfiles')

%% Define important paths
data_dir = '/data/pt_02004/MDN_LANG/Derivatives/subjects/'

% Define subject ID for looping, 
% sub = {'002','003','005','006','007','009','011', ...
%        '012','013','014','015','016','017','019','020' ...
%   '021','022','023', '025','027','028','029','030','031'};
sub = {'024'}; %,'004','024'

run = {'2'};  

for i = 1:numel(sub)
for x = 1:numel(run)
    
% define the folders where the logfiles can be found
filename_logfolder = [data_dir sub{i} '/logfiles/'];

% %go to directory
% cd(filename_logfolder)
 
% get the file
fileName_path = dir([filename_logfolder sub{i} '_fMRI_run-' run{x} '.log'])
fileName = [fileName_path.folder '/' fileName_path.name]

% import using importPresentationLog_orig function
[a,b] = importPresentationLog_orig(fileName);

disp (['save logfiles ' sub{i} ]);

% save in each subjects' folder
save([data_dir sub{i} '/logfiles/' sub{i} '_run-' run{x} '_log_a.mat'],'a')
save([data_dir sub{i} '/logfiles/' sub{i} '_run-' run{x} '_log_b.mat'],'b')

% clearvars -except data_dir sub

end
end
