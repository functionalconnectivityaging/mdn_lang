%%  1st level batch script: Segmentation, Coregistration, Normalisation & Smoothing
% This script performs first level analyses on single subject level

%% Specify paths & folders
%  Data folder
script_path =  '/data/pt_02004/Scripts/Matlab/Preprocessing';
data_path = '/data/pt_02004/MDN_LANG/fmriprep/output/';
spm_path = '/data/pt_02004/spm12';

% Subject_folders
  sub = {'002','003','004','005','006','007','008','009','011', ...
         '012','013','014','015','016','017','018','019','020', ...
         '021','022','023','024','025','027','028','029','030'};
      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');
  
% Estimating the GLM can take some time, particularly if you have a lot of betas. If you just want to specify your
% design matrix so that you can assess it for singularities, turn this to 0.
% If you wish to do it later, estimating the GLM through the GUI is very quick.
ESTIMATE_GLM = 1;
  
% Loop through subject folders
for i = 1:numel(sub) 
    
     % Display which participant and which run is currently processed
    X = ['This is participant ' sub{i}];
        disp(X);
        
    %% Get smoothed epis for each run
    % Run-1
    epi_mainfolder = dir([data_path 'control' sub{i} '/fmriprep/sub-control' sub{i} '/func']);
    curr_epi_subfolder = epi_mainfolder.folder;
    nifti_files = dir([curr_epi_subfolder '/ssub-control' sub{i} '_task-SWG_run-1_echo-2_space-MNI152NLin2009cAsym_desc-preproc_bold_trimmed.nii']);
    func_images_path = [nifti_files.folder '/' nifti_files.name];
        func_vols = spm_vol(func_images_path);
        Vols_1 = {};
        for iVol = 1:numel(func_vols)
            Vols_1{iVol} = [func_images_path ',' num2str(iVol)];
        end
        Vols_1 = Vols_1';
        
    % Run-2    
    epi_mainfolder = dir([data_path 'control' sub{i} '/fmriprep/sub-control' sub{i} '/func']);
    curr_epi_subfolder = epi_mainfolder.folder;
    nifti_files = dir([curr_epi_subfolder '/ssub-control' sub{i} '_task-SWG_run-2_echo-2_space-MNI152NLin2009cAsym_desc-preproc_bold_trimmed.nii']);
    func_images_path = [nifti_files.folder '/' nifti_files.name];
        func_vols = spm_vol(func_images_path);
        Vols_2 = {};
        for iVol = 1:numel(func_vols)
            Vols_2{iVol} = [func_images_path ',' num2str(iVol)];
        end
        Vols_2 = Vols_2';   
        
    %% Get movement parameters
    movement_folder_1 = dir(['/data/pt_02004/MDN_LANG/Derivatives/subjects/' sub{i} '_JE/fmriprep/1st_level_fd/multiple_regressors_1.txt']);
    movement_run_1 = [movement_folder_1.folder '/' movement_folder_1.name];
    
    % Run-2
    movement_folder_2 = dir(['/data/pt_02004/MDN_LANG/Derivatives/subjects/' sub{i} '_JE/fmriprep/1st_level_fd/multiple_regressors_2.txt']);
    movement_run_2 = [movement_folder_2.folder '/' movement_folder_2.name];

    %% Get multiple conditions mat-file
    % Run-1
    multCond_folder_1 = dir(['/data/pt_02004/MDN_LANG/Derivatives/subjects/' sub{i} '_JE/logfiles/' sub{i} '_JE_run-1_1st_level_block_design.mat']);
    multCond_run_1 = [multCond_folder_1.folder '/' multCond_folder_1.name];

    % Run-2
    multCond_folder_2 = dir(['/data/pt_02004/MDN_LANG/Derivatives/subjects/' sub{i} '_JE/logfiles/' sub{i} '_JE_run-2_1st_level_block_design.mat']);
    multCond_run_2 = [multCond_folder_2.folder '/' multCond_folder_2.name];
        
    %% Define output directory
    output_dir = ['/data/pt_02004/MDN_LANG/Derivatives/subjects/' sub{i} '_JE/fmriprep/1st_level_fd'];
    
    %% Set up batch
    % Specify model
    clear matlabbatch
    matlabbatch{1}.spm.stats.fmri_spec.dir = {output_dir};
    matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
    matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2;
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 60;
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 30;
    
    % Run-1
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = Vols_1;
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = {multCond_run_1};
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress = struct('name', {}, 'val', {});
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi_reg = {movement_run_1};
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).hpf = 128;
    
    % Run-2
    matlabbatch{1}.spm.stats.fmri_spec.sess(2).scans = Vols_2;
    matlabbatch{1}.spm.stats.fmri_spec.sess(2).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
    matlabbatch{1}.spm.stats.fmri_spec.sess(2).multi = {multCond_run_2};
    matlabbatch{1}.spm.stats.fmri_spec.sess(2).regress = struct('name', {}, 'val', {});
    matlabbatch{1}.spm.stats.fmri_spec.sess(2).multi_reg = {movement_run_2};
    matlabbatch{1}.spm.stats.fmri_spec.sess(2).hpf = 128;
    
    matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
    matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0]; % set to 1 1 if I want time and dispersion derivatives
    matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
    matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
    matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
    matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
    matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';
    
    %% Navigate to output directory, specify and estimate GLM
     cd(output_dir);
     spm_jobman('run', matlabbatch)
     
    if ESTIMATE_GLM == 1
        load SPM;
        spm_spm(SPM);
    end
    
    %% Contrast setup: define all contrasts and run them
    clear matlabbatch
    spmmat = dir([output_dir '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    %matlabbatch{1}.spm.stats.con.spmmat = dir([output_dir '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Speech - All';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 1 1 1];
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Categories - Counting';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 1 -1 -1];
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'Difficult - Easy';
    matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [1 -1];
    matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'Easy - Difficult';
    matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [-1 1];
    matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{5}.tcon.name = 'Counting - Categories';
    matlabbatch{1}.spm.stats.con.consess{5}.tcon.weights = [-1 -1 1 1];
    matlabbatch{1}.spm.stats.con.consess{5}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{6}.tcon.name = 'CountingForw - CountingBackw';
    matlabbatch{1}.spm.stats.con.consess{6}.tcon.weights = [0 0 1 -1];
    matlabbatch{1}.spm.stats.con.consess{6}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{7}.tcon.name = 'CountingBackw - CountingForw';
    matlabbatch{1}.spm.stats.con.consess{7}.tcon.weights = [0 0 -1 1];
    matlabbatch{1}.spm.stats.con.consess{7}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{8}.fcon.name = 'EOI';
    matlabbatch{1}.spm.stats.con.consess{8}.fcon.weights = [1 0 0 0
                                                            0 1 0 0
                                                            0 0 1 0
                                                            0 0 0 1];
    matlabbatch{1}.spm.stats.con.consess{8}.fcon.sessrep = 'repl';
%     matlabbatch{1}.spm.stats.con.consess{9}.fcon.name = 'Motion';
%     matlabbatch{1}.spm.stats.con.consess{9}.fcon.weights = [0 0 0 0 1 0 0 0 0 0
%                                                             0 0 0 0 0 1 0 0 0 0
%                                                             0 0 0 0 0 0 1 0 0 0
%                                                             0 0 0 0 0 0 0 1 0 0
%                                                             0 0 0 0 0 0 0 0 1 0
%                                                             0 0 0 0 0 0 0 0 0 1];
%     matlabbatch{1}.spm.stats.con.consess{9}.fcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{9}.tcon.name = 'Difficult - All';
    matlabbatch{1}.spm.stats.con.consess{9}.tcon.weights = [1];
    matlabbatch{1}.spm.stats.con.consess{9}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{10}.tcon.name = 'Easy - All';
    matlabbatch{1}.spm.stats.con.consess{10}.tcon.weights = [0 1];
    matlabbatch{1}.spm.stats.con.consess{10}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{11}.tcon.name = 'CountForward - All';
    matlabbatch{1}.spm.stats.con.consess{11}.tcon.weights = [0 0 1];
    matlabbatch{1}.spm.stats.con.consess{11}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{12}.tcon.name = 'CountBackward - All';
    matlabbatch{1}.spm.stats.con.consess{12}.tcon.weights = [0 0 0 1];
    matlabbatch{1}.spm.stats.con.consess{12}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{13}.tcon.name = 'Categories - All';
    matlabbatch{1}.spm.stats.con.consess{13}.tcon.weights = [1 1];
    matlabbatch{1}.spm.stats.con.consess{13}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{14}.tcon.name = 'Counting - All';
    matlabbatch{1}.spm.stats.con.consess{14}.tcon.weights = [0 0 1 1];
    matlabbatch{1}.spm.stats.con.consess{14}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
%     %% Calculate and save and accumulate results in ps file
%     % There's a batch for each session since squared brackets will lead to
%     % an interactionanalysis of the contrasts
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 1;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 2;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 3;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 4;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 5;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 6;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 7;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 8;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 9;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 10;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 11;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 12;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
%     
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.results.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
%     matlabbatch{1}.spm.stats.results.conspec.contrasts = 13;
%     matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
%     matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
%     matlabbatch{1}.spm.stats.results.conspec.extent = 0;
%     matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
%     matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
%     matlabbatch{1}.spm.stats.results.units = 1;
%     matlabbatch{1}.spm.stats.results.export{1}.ps = true;
%     spm_jobman('run', matlabbatch)
end