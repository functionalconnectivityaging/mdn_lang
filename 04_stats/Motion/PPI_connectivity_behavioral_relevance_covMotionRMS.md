# Load packages

``` r
library(here)
library(psych)
library(stats)
library(reshape2)
library(plyr)
library(tidyverse)
library(data.table)
library(scales) 
library(lme4)
library(emmeans)
library(RColorBrewer)
library(ggeffects)
library(sjPlot)
library(ggpubr)
library(ppcor)
library(fitdistrplus)
```

# Theme for plots

# Load R data

``` r
load("RData/df_categories_04_2021.RData") # df with raw values for task "semantic fluency"
```

# Prepare df for response time

``` r
# prepare df
df_categories.RT = df_categories[!is.na(df_categories$Onset),] # Remove NAs from Onset column and create df
df_categories.RT <- droplevels(df_categories.RT) # drop unused factor levels
df_categories.RT$MDN_DMN_z <- scale(df_categories.RT$MDN_DMN, scale = F) # re-center connectivity measures
df_categories.RT$MDN_z <- scale(df_categories.RT$MDN, scale = F)
df_categories.RT$DMN_z <- scale(df_categories.RT$DMN, scale = F)
contrasts(df_categories.RT$AgeSum) <- contr.sum(2)/2 # re-apply sum coding to factor

# Decide whether Gamma distribution or log-transformation is better for RT data
# fit.norm <- fitdist(df_categories.RT$Onset, "lnorm")
# fit.gamma <- fitdist(df_categories.RT$Onset, "gamma")
# fit.norm$aic # AIC: 135231.1 --> log transformation is better
# fit.gamma$aic # AIC: 136392.8
```

# Regression analyses

## GLMM - accuracy

``` r
# model
Correct_Categories <- glmer(Correct ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + rms + (1|Subj) + (1|Category), data = df_categories, family = binomial(link = "logit"), control=glmerControl(optimizer = 'bobyqa'))
summary(Correct_Categories)
```

    ## Generalized linear mixed model fit by maximum likelihood (Laplace
    ##   Approximation) [glmerMod]
    ##  Family: binomial  ( logit )
    ## Formula: Correct ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + rms + (1 | Subj) +  
    ##     (1 | Category)
    ##    Data: df_categories
    ## Control: glmerControl(optimizer = "bobyqa")
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   4825.0   4904.1  -2401.5   4803.0     9826 
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -12.5814   0.1066   0.2059   0.3528   0.6498 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  Subj     (Intercept) 0.2351   0.4848  
    ##  Category (Intercept) 1.7107   1.3080  
    ## Number of obs: 9837, groups:  Subj, 31; Category, 20
    ## 
    ## Fixed effects:
    ##                   Estimate Std. Error z value Pr(>|z|)    
    ## (Intercept)        3.38207    0.33442  10.113  < 2e-16 ***
    ## MDN_z             -0.28030    0.83747  -0.335  0.73785    
    ## DMN_z             -0.16841    0.80997  -0.208  0.83529    
    ## MDN_DMN_z          0.25794    0.90350   0.285  0.77527    
    ## AgeSum1            0.08914    0.09231   0.966  0.33418    
    ## rms               -0.94748    0.36738  -2.579  0.00991 ** 
    ## MDN_z:AgeSum1      1.93420    1.58908   1.217  0.22353    
    ## DMN_z:AgeSum1      2.43295    1.45473   1.672  0.09444 .  
    ## MDN_DMN_z:AgeSum1 -3.44897    1.57995  -2.183  0.02904 *  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) MDN_z  DMN_z  MDN_DMN_z AgeSm1 rms    MDN_:A DMN_:A
    ## MDN_z       -0.045                                                    
    ## DMN_z        0.035 -0.017                                             
    ## MDN_DMN_z    0.018 -0.606 -0.410                                      
    ## AgeSum1      0.154 -0.273  0.035  0.259                               
    ## rms         -0.346  0.112 -0.111 -0.016    -0.414                     
    ## MDN_z:AgSm1  0.014 -0.088  0.098  0.059     0.099 -0.103              
    ## DMN_z:AgSm1  0.017 -0.228 -0.203  0.218     0.073 -0.089  0.282       
    ## MDN_DMN_:AS -0.052  0.126 -0.155  0.032    -0.144  0.224 -0.651 -0.571

``` r
# LRTs
drop1(Correct_Categories, test = "Chisq") # drop two-way interactions
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + rms + (1 | Subj) + 
    ##     (1 | Category)
    ##                  npar    AIC    LRT Pr(Chi)  
    ## <none>                4825.0                 
    ## rms                 1 4829.6 6.5896 0.01026 *
    ## MDN_z:AgeSum        1 4824.4 1.4398 0.23018  
    ## DMN_z:AgeSum        1 4825.7 2.7117 0.09961 .
    ## MDN_DMN_z:AgeSum    1 4827.6 4.6381 0.03127 *
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
Correct_Categories_mainEffects <- glmer(Correct ~ MDN_z + DMN_z + MDN_DMN_z + AgeSum + (1|Subj) + (1|Category), data = df_categories, family = binomial(link = "logit"), control=glmerControl(optimizer = 'bobyqa')) # model with main effects
drop1(Correct_Categories_mainEffects, test = "Chisq") # drop main effects
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ MDN_z + DMN_z + MDN_DMN_z + AgeSum + (1 | Subj) + (1 | 
    ##     Category)
    ##           npar    AIC      LRT Pr(Chi)
    ## <none>         4826.7                 
    ## MDN_z        1 4824.8 0.036436  0.8486
    ## DMN_z        1 4825.0 0.244672  0.6209
    ## MDN_DMN_z    1 4824.8 0.046802  0.8287
    ## AgeSum       1 4824.8 0.028624  0.8657

``` r
t2 <- emtrends(Correct_Categories, pairwise ~ AgeSum, var = "MDN_DMN_z", transform = "response", adjust = "bonferroni") # this gives simple slopes of MDN_DMN_z per factor level as well as pairwise comparisons
t2
```

    ## $emtrends
    ##  AgeSum MDN_DMN_z.trend     SE  df asymp.LCL asymp.UCL
    ##  OA             -0.0587 0.0524 Inf   -0.1614     0.044
    ##  YA              0.0860 0.0580 Inf   -0.0276     0.200
    ## 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ##  contrast estimate     SE  df z.ratio p.value
    ##  OA - YA    -0.145 0.0789 Inf -1.836  0.0664

``` r
Correct_Categories.MDN_DMN <- ggpredict(Correct_Categories, terms = c("MDN_DMN_z[all]", "AgeSum"))
Correct_Categories.plot.MDN_DMN <- plot(Correct_Categories.MDN_DMN) +
  scale_fill_manual(values = c("#52257a", "#018571")) +
  scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
  coord_cartesian(ylim = c(0.8, 1)) +
  apatheme +
  theme(plot.title = element_blank())
```

    ## Scale for 'colour' is already present. Adding another scale for 'colour',
    ## which will replace the existing scale.

``` r
Correct_Categories.plot.MDN_DMN
```

![](PPI_connectivity_behavioral_relevance_covMotionRMS_files/figure-markdown_github/unnamed-chunk-4-1.png)

## LMM - response time

``` r
# model
RT_Categories <- lmer(log(Onset) ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + rms + (1|Subj) + (1|Category), data = df_categories.RT, control = lmerControl(optimizer = "bobyqa"), REML = F) 
summary(RT_Categories)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: log(Onset) ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + rms + (1 |  
    ##     Subj) + (1 | Category)
    ##    Data: df_categories.RT
    ## Control: lmerControl(optimizer = "bobyqa")
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   7845.4   7931.6  -3910.7   7821.4     9663 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -9.3082 -0.6451 -0.1342  0.5021  4.8692 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  Subj     (Intercept) 0.013357 0.11557 
    ##  Category (Intercept) 0.004162 0.06451 
    ##  Residual             0.129234 0.35949 
    ## Number of obs: 9675, groups:  Subj, 31; Category, 20
    ## 
    ## Fixed effects:
    ##                    Estimate Std. Error t value
    ## (Intercept)        6.516455   0.027638 235.779
    ## MDN_z             -0.144352   0.076559  -1.886
    ## DMN_z             -0.343991   0.086570  -3.974
    ## MDN_DMN_z          0.530197   0.087123   6.086
    ## AgeSum1            0.062491   0.008775   7.122
    ## rms                0.030336   0.034843   0.871
    ## MDN_z:AgeSum1      0.970552   0.168978   5.744
    ## DMN_z:AgeSum1      0.888637   0.147962   6.006
    ## MDN_DMN_z:AgeSum1 -0.764944   0.165358  -4.626
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) MDN_z  DMN_z  MDN_DMN_z AgeSm1 rms    MDN_:A DMN_:A
    ## MDN_z       -0.028                                                    
    ## DMN_z        0.054 -0.109                                             
    ## MDN_DMN_z   -0.010 -0.531 -0.379                                      
    ## AgeSum1      0.169 -0.266  0.012  0.230                               
    ## rms         -0.376  0.070 -0.167  0.060    -0.410                     
    ## MDN_z:AgSm1 -0.022 -0.054  0.091 -0.001     0.013 -0.049              
    ## DMN_z:AgSm1  0.017 -0.096 -0.106  0.092     0.089 -0.121  0.343       
    ## MDN_DMN_:AS -0.047  0.044 -0.203  0.145    -0.080  0.253 -0.669 -0.606

``` r
# LRTs
drop1(RT_Categories, test = "Chisq") # drop two-way interactions
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + rms + (1 | 
    ##     Subj) + (1 | Category)
    ##                  npar    AIC    LRT   Pr(Chi)    
    ## <none>                7845.4                     
    ## rms                 1 7844.2  0.758     0.384    
    ## MDN_z:AgeSum        1 7875.7 32.291 1.327e-08 ***
    ## DMN_z:AgeSum        1 7879.0 35.550 2.486e-09 ***
    ## MDN_DMN_z:AgeSum    1 7864.6 21.178 4.185e-06 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
RT_Categories_mainEffects <- lmer(log(Onset) ~ MDN_z + DMN_z + MDN_DMN_z + AgeSum + (1|Subj) + (1|Category), data = df_categories.RT, control = lmerControl(optimizer = 'bobyqa'), REML = F)
drop1(RT_Categories_mainEffects, test = "Chisq") # drop main effects
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ MDN_z + DMN_z + MDN_DMN_z + AgeSum + (1 | Subj) + 
    ##     (1 | Category)
    ##           npar    AIC    LRT   Pr(Chi)    
    ## <none>         7891.3                     
    ## MDN_z        1 7890.8  1.414  0.234375    
    ## DMN_z        1 7902.0 12.653  0.000375 ***
    ## MDN_DMN_z    1 7918.8 29.429 5.801e-08 ***
    ## AgeSum       1 7952.5 63.149 1.917e-15 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
# post-hoc tests
t <- emtrends(RT_Categories, pairwise ~ AgeSum, var = "MDN_z", transform = "response", adjust = "bonferroni") # this gives simple slopes of MDN_z per factor level as well as pairwise comparisons
t
```

    ## $emtrends
    ##  AgeSum MDN_z.trend   SE  df asymp.LCL asymp.UCL
    ##  OA             240 78.0 Inf      87.3       393
    ##  YA            -416 77.5 Inf    -568.4      -265
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ##  contrast estimate  SE  df z.ratio p.value
    ##  OA - YA       657 116 Inf 5.673   <.0001 
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding

``` r
t$contrasts %>% confint()
```

    ##  contrast estimate  SE  df asymp.LCL asymp.UCL
    ##  OA - YA       657 116 Inf       430       883
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95

``` r
t1 <- emtrends(RT_Categories, pairwise ~ AgeSum, var = "DMN_z", transform = "response", adjust = "bonferroni") # this gives simple slopes of DMN_z per factor level as well as pairwise comparisons
t1
```

    ## $emtrends
    ##  AgeSum DMN_z.trend   SE  df asymp.LCL asymp.UCL
    ##  OA            70.7 75.8 Inf       -78       219
    ##  YA          -521.4 80.1 Inf      -678      -364
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ##  contrast estimate  SE  df z.ratio p.value
    ##  OA - YA       592 101 Inf 5.852   <.0001 
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding

``` r
t1$contrasts %>% confint()
```

    ##  contrast estimate  SE  df asymp.LCL asymp.UCL
    ##  OA - YA       592 101 Inf       394       790
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95

``` r
t2 <- emtrends(RT_Categories, pairwise ~ AgeSum, var = "MDN_DMN_z", transform = "response", adjust = "bonferroni") # this gives simple slopes of MDN_DMN_z per factor level as well as pairwise comparisons
t2
```

    ## $emtrends
    ##  AgeSum MDN_DMN_z.trend   SE  df asymp.LCL asymp.UCL
    ##  OA                 104 90.7 Inf     -73.7       282
    ##  YA                 604 74.1 Inf     458.7       749
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ##  contrast estimate  SE  df z.ratio p.value
    ##  OA - YA      -500 113 Inf -4.410  <.0001 
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding

``` r
t2$contrasts %>% confint()
```

    ##  contrast estimate  SE  df asymp.LCL asymp.UCL
    ##  OA - YA      -500 113 Inf      -722      -278
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95

``` r
# plot
RT_Categories.MDN <- ggpredict(RT_Categories, terms = c("MDN_z[all]", "AgeSum"))
RT_Categories.plot.MDN <- plot(RT_Categories.MDN) +
  scale_fill_manual(values = c("#52257a", "#018571")) +
  scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
  coord_cartesian(ylim = c(500, 900)) +
  apatheme +
  theme(plot.title = element_blank())
RT_Categories.plot.MDN
```

![](PPI_connectivity_behavioral_relevance_covMotionRMS_files/figure-markdown_github/unnamed-chunk-5-1.png)

``` r
RT_Categories.DMN <- ggpredict(RT_Categories, terms = c("DMN_z[all]", "AgeSum"))
RT_Categories.plot.DMN <- plot(RT_Categories.DMN) +
  scale_fill_manual(values = c("#52257a", "#018571")) +
  scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
  coord_cartesian(ylim = c(500, 900)) +
  apatheme +
  theme(plot.title = element_blank())
RT_Categories.plot.DMN
```

![](PPI_connectivity_behavioral_relevance_covMotionRMS_files/figure-markdown_github/unnamed-chunk-5-2.png)

``` r
RT_Categories.MDN_DMN <- ggpredict(RT_Categories, terms = c("MDN_DMN_z[all]", "AgeSum"))
RT_Categories.plot.MDN_DMN <- plot(RT_Categories.MDN_DMN) +
  scale_fill_manual(values = c("#52257a", "#018571")) +
  scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
  coord_cartesian(ylim = c(500, 900)) +
  apatheme +
  theme(plot.title = element_blank())
RT_Categories.plot.MDN_DMN
```

![](PPI_connectivity_behavioral_relevance_covMotionRMS_files/figure-markdown_github/unnamed-chunk-5-3.png)

## Extract table plot for both models

``` r
tab_model(Correct_Categories, RT_Categories,
  dv.labels = c("Accuracy", "Response time"),
  pred.labels = c("Intercept", "Within-MDN FC", "Within-DMN FC", "Between-network FC", "Age", "Within-MDN FC * Age", 
                  "Within-DMN FC * Age", "Between-network FC * Age"),
  string.pred = "Coefficient",
  string.ci = "Conf. Int (95%)", 
  transform = NULL,
  show.p = F,
  show.stat = T,
  file = "connectivity_models.doc")
```

    ## Length of `pred.labels` does not equal number of predictors, no labelling applied.

<table style="border-collapse:collapse; border:none;">
<tr>
<th style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm;  text-align:left; ">
 
</th>
<th colspan="3" style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm; ">
Accuracy
</th>
<th colspan="3" style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm; ">
Response time
</th>
</tr>
<tr>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  text-align:left; ">
Coefficient
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Log-Odds
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Conf. Int (95%)
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Statistic
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Estimates
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Conf. Int (95%)
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  col7">
Statistic
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
(Intercept)
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
3.38
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
2.73 – 4.04
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
10.11
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
6.52
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
6.46 – 6.57
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
235.78
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
MDN_z
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.28
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.92 – 1.36
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.33
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.14
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.29 – 0.01
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
-1.89
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
DMN_z
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.17
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.76 – 1.42
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.21
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.34
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.51 – -0.17
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
-3.97
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
MDN_DMN_z
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.26
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.51 – 2.03
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.29
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.53
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.36 – 0.70
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
6.09
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
AgeSum1
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.09
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.09 – 0.27
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.97
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.06
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.05 – 0.08
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
7.12
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
rms
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.95
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.67 – -0.23
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-2.58
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.03
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.04 – 0.10
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
0.87
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
MDN_z:AgeSum1
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
1.93
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.18 – 5.05
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
1.22
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.97
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.64 – 1.30
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
5.74
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
DMN_z:AgeSum1
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
2.43
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.42 – 5.28
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
1.67
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.89
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.60 – 1.18
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
6.01
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
MDN_DMN_z:AgeSum1
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-3.45
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-6.55 – -0.35
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-2.18
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.76
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.09 – -0.44
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
-4.63
</td>
</tr>
<tr>
<td colspan="7" style="font-weight:bold; text-align:left; padding-top:.8em;">
Random Effects
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
σ<sup>2</sup>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
3.29
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.13
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
τ<sub>00</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.24 <sub>Subj</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.01 <sub>Subj</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
1.71 <sub>Category</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.00 <sub>Category</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
ICC
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.37
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.12
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
N
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
31 <sub>Subj</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
31 <sub>Subj</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
20 <sub>Category</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
20 <sub>Category</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm; border-top:1px solid;">
Observations
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left; border-top:1px solid;" colspan="3">
9837
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left; border-top:1px solid;" colspan="3">
9675
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
Marginal R<sup>2</sup> / Conditional R<sup>2</sup>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.006 / 0.375
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.027 / 0.143
</td>
</tr>
</table>
