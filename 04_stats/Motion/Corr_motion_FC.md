# Load packages

``` r
library(here)                         
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(lme4)
library(emmeans)
library(stats)
library(ggplot2)
library(data.table)
library(ggpubr)
library(ppcor)
```

# Load data

``` r
load("RData/all_subjects_04_2021.RData")
load("RData/df_motion_FC.RData")
```

# Calculate RMS motion

``` r
# transform radians to mm assuming an average head size of 56 cm
all_subjects$rot_x_mm <- all_subjects$rot_x * 56
all_subjects$rot_y_mm <- all_subjects$rot_y * 56
all_subjects$rot_z_mm <- all_subjects$rot_z * 56
all_subjects <- all_subjects[-c(4:6)]

df_motion <- all_subjects %>% 
  gather(key = "motion_param", value = "motion_mm", "trans_x", "trans_y", "trans_z", "rot_x_mm", "rot_y_mm", "rot_z_mm")

df_motion_summary <- df_motion %>% 
  group_by(Age, Subject) %>% 
  summarize(rms = sqrt(mean(motion_mm^2)))
```

# Correlation analyses

``` r
# Correlations between functional connectivity measures and RMS motion

cor_plot_MDN <- ggscatter(df_motion_FC, x = "MDN", y = "rms", 
          color = "Age", palette = c("#52257a", "#018571"),
          add = "reg.line", conf.int = TRUE, cor.coef = F,
          xlab = "Within-MDN connectivity", ylab = "RMS") +
  stat_cor(aes(color = Age), method = "pearson") +
  apatheme
cor_plot_MDN
```

![](Corr_motion_FC_files/figure-markdown_github/unnamed-chunk-2-1.png)

``` r
cor_plot_DMN <- ggscatter(df_motion_FC, x = "DMN", y = "rms", 
          color = "Age", palette = c("#52257a", "#018571"),
          add = "reg.line", conf.int = TRUE, cor.coef = F,
          xlab = "Within-DMN connectivity", ylab = "RMS") +
  stat_cor(aes(color = Age), method = "pearson") +
  apatheme
cor_plot_DMN
```

![](Corr_motion_FC_files/figure-markdown_github/unnamed-chunk-2-2.png)

``` r
cor_plot_MDN_DMN <- ggscatter(df_motion_FC, x = "MDN_DMN", y = "rms", 
          color = "Age", palette = c("#52257a", "#018571"),
          add = "reg.line", conf.int = TRUE, cor.coef = F,
          xlab = "Between-network connectivity", ylab = "RMS") +
  stat_cor(aes(color = Age), method = "pearson") +
  apatheme
cor_plot_MDN_DMN
```

![](Corr_motion_FC_files/figure-markdown_github/unnamed-chunk-2-3.png)
