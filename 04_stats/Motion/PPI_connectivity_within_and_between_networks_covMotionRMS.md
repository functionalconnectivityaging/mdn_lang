# Load packages

``` r
library(here)                         
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(magrittr)
library(scales)
library(lme4)
library(emmeans)
library(RColorBrewer)
library(ggeffects)
library(sjPlot)
library(stats)
library(ggplot2)
library(data.table)
library(ggpubr)
library(ppcor)
library(fitdistrplus)
source('https://osf.io/ws6xc/download') # source Meff function
```

# Load data

``` r
load("RData/network_df_04_2021.RData") 
load("RData/alpha_meff_network.RData")
```

# Group comparison GLMs on networks with IV Age

``` r
# create empty data frame to store results
results.network <- data.frame()

# loop through the columns
for(var in names(network_df)[c(4:6)]){
         # dynamically generate formula
         fmla <- as.formula(paste0(var, "~ Age + rms")) # add Age and RMS motion as covariates to GLM

         # fit glm model
         fit <- glm(fmla, data = network_df)

         tab_model(fit) # optional idea for different output format

         ## capture summary stats
         intercept <- coef(summary(fit))[1]
         slope <- coef(summary(fit))[2]
         slope_rms <- coef(summary(fit))[3]
         std.error.intercept <- coef(summary(fit))[4]
         std.error.slope <- coef(summary(fit))[5]
         std.error.slope_rms <- coef(summary(fit))[6]
         t.value.intercept <- coef(summary(fit))[7]
         t.value.slope <- coef(summary(fit))[8]
         t.value.slope_rms <- coef(summary(fit))[9]
         p.value.intercept <- coef(summary(fit))[10]
         p.value.slope <- coef(summary(fit))[11]
         p.value.slope_rms <- coef(summary(fit))[12]
         AIC <- AIC(fit)
         Deviance <- deviance(fit)

         # get coefficents of fit
         cfit <- coef(summary(fit))

         # create temporary data frame
         df <- data.frame(var = var, intercept = cfit[1], slope = cfit[2], slope_rms = cfit[3],
                          std.error.intercept = cfit[4], std.error.slope = cfit[5], std.error.slope_rms = cfit[6],
                          t.value.intercept = cfit[7], t.value.slope = cfit[8], t.value.slope_rms = cfit[9],
                          p.value.intercept = cfit[10], p.value.slope = cfit[11], p.value.slope_rms = cfit[12], 
                          AIC = AIC(fit), Deviance = deviance(fit), stringsAsFactors = F)

         # bind rows of temporary data frame to the results data frame
         results.network <- rbind(results.network, df)
 }

results.network <- separate(results.network, var, sep = "_", fill = "left", into = c("Network", "Intercept"))
results.network[1, 1] <- "MDN"
results.network[2, 1] <- "DMN"
results.network$Network <- as.factor(results.network$Network)
results.network$Intercept <- as.factor(results.network$Intercept)
results.network$Network <- factor(results.network$Network, levels = c("DMN", "MDN"))
results.network$Intercept <- factor(results.network$Intercept, levels = c("DMN", "MDN"))
```

# Heatmaps for group comparisons

``` r
# This is the heatmap for between and within networks (Intercepts)
Networks_between_within <- ggplot(results.network, aes(Intercept, Network)) +
  geom_tile(aes(fill = t.value.intercept, color = p.value.intercept < alpha_meff_network), size = 1) +
  scale_fill_gradientn(colours = palet.positive, values=rescale(c(0,3)), limits = c(0, 3), breaks = seq(0, 3, by = 3), oob = scales::squish, na.value = "white") +
  scale_color_manual(guide = "none", values = c(NA, "black")) +
  apatheme +
  theme(
    #axis.text.x = element_text(angle = 90, vjust = 0.2, hjust = 0.95),
    axis.line = element_blank(),
    axis.ticks = element_blank())
Networks_between_within
```

![](PPI_connectivity_within_and_between_networks_covMotionRMS_files/figure-markdown_github/heatmaps-1.png)

``` r
# This is the heatmap for IV Age (Slope)
Networks_age <- ggplot(results.network, aes(Intercept, Network)) +
  geom_tile(aes(fill = t.value.slope, color = p.value.slope < alpha_meff_network), size = 1) +
  scale_fill_gradientn(colours = palet.positive, values=rescale(c(0,3)), limits = c(0, 3), breaks = seq(0, 3, by = 3), oob = scales::squish, na.value = "white") +
  scale_color_manual(guide = "none", values = c(NA, "black")) +
  apatheme +
  theme(
    #axis.text.x = element_text(angle = 90, vjust = 0.2, hjust = 0.95),
    axis.line = element_blank(),
    axis.ticks = element_blank())
Networks_age
```

![](PPI_connectivity_within_and_between_networks_covMotionRMS_files/figure-markdown_github/heatmaps-2.png)

``` r
# This is the heatmap for IV RMS (second slope)
Networks_rms <- ggplot(results.network, aes(Intercept, Network)) +
  geom_tile(aes(fill = t.value.slope_rms, color = p.value.slope_rms < alpha_meff_network), size = 1) +
  scale_fill_gradientn(colours = palet.positive, values=rescale(c(0,3)), limits = c(0, 3), breaks = seq(0, 3, by = 3), oob = scales::squish, na.value = "white") +
  scale_color_manual(guide = "none", values = c(NA, "black")) +
  apatheme +
  theme(
    #axis.text.x = element_text(angle = 90, vjust = 0.2, hjust = 0.95),
    axis.line = element_blank(),
    axis.ticks = element_blank())
Networks_rms
```

![](PPI_connectivity_within_and_between_networks_covMotionRMS_files/figure-markdown_github/heatmaps-3.png)
