# Load packages

``` r
library(psych)
library(mnormt)
library(ggplot2)
library(here)
library(tidyverse)
library(lme4)
library(sjPlot)
```

# Data for OA

``` r
# load file with responses for OA
file <- here("Pilot_MDN_LANG/Results_OA.txt")
SWG_AE <- read.table(file, header = TRUE)

df_OA <- SWG_AE %>% 
  gather(key = "category", value = "n", 2:31)

# create df with means per category
df_OA <- df_OA %>% 
  group_by(category) %>% 
  summarise(mean = mean(n))

df_OA$category <- as.factor(df_OA$category)

df_OA <- df_OA[order(df_OA$mean),]

# determine median value of mean number of items per category
median(df_OA$mean)
```

    ## [1] 12.95833

``` r
# difficult categories
OA_cat_diff <- df_OA %>% 
  slice(2:4, 6:10, 12:13)
OA_cat_diff$difficulty <- "difficult"

# easy categories
OA_cat_easy <- df_OA %>% 
  slice_max(mean, n = 10)
OA_cat_easy$difficulty <- "easy"

# create one df with 20 categories for OA
OA_cats <- bind_rows(OA_cat_diff, OA_cat_easy)
OA_cats$age <- "OA"

# check for difference between easy and difficult categories
t.test(OA_cats$mean[OA_cats$difficulty == "easy"], OA_cats$mean[OA_cats$difficulty == "difficult"])
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  OA_cats$mean[OA_cats$difficulty == "easy"] and OA_cats$mean[OA_cats$difficulty == "difficult"]
    ## t = 7.9723, df = 17.046, p-value = 3.756e-07
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##  4.256198 7.318802
    ## sample estimates:
    ## mean of x mean of y 
    ##  16.75833  10.97083

# Data for YA

``` r
# load file with responses for YA
file <- here("Pilot_MDN_LANG/Results_YA.txt")
SWG_YA <- read.table(file, header = TRUE)

df_YA <- SWG_YA %>% 
  gather(key = "category", value = "n", 2:31)

# create df with means per category
df_YA <- df_YA %>% 
  group_by(category) %>% 
  summarise(mean = mean(n))

df_YA$category <- as.factor(df_YA$category)

df_YA <- df_YA[order(df_YA$mean),]

# determine median value of mean number of items per category
median(df_YA$mean)
```

    ## [1] 13.89583

``` r
# difficult categories
YA_cat_diff <- df_YA %>% 
  slice(2:4, 6:11, 13)
YA_cat_diff$difficulty <- "difficult"

# easy categories
YA_cat_easy <- df_YA %>% 
  slice_max(mean, n = 10)
YA_cat_easy$difficulty <- "easy"

# create one df with 20 categories for YA
YA_cats <- bind_rows(YA_cat_diff, YA_cat_easy)
YA_cats$age <- "YA"

# check for difference between easy and difficult categories
t.test(YA_cats$mean[YA_cats$difficulty == "difficult"], YA_cats$mean[YA_cats$difficulty == "easy"])
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  YA_cats$mean[YA_cats$difficulty == "difficult"] and YA_cats$mean[YA_cats$difficulty == "easy"]
    ## t = -10.183, df = 13.9, p-value = 7.963e-08
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##  -11.022979  -7.185354
    ## sample estimates:
    ## mean of x mean of y 
    ##  10.30000  19.40417

# Merge df for OA and YA

``` r
# merge df for both age groups
cats <- bind_rows(OA_cats, YA_cats)

merge(OA_cat_diff, YA_cat_diff, by = "category")
```

    ##        category    mean.x difficulty.x    mean.y difficulty.y
    ## 1        Blumen 12.750000    difficult 10.041667    difficult
    ## 2        Fische 12.541667    difficult  9.583333    difficult
    ## 3  Gartengeräte  9.625000    difficult 10.250000    difficult
    ## 4      Insekten  9.208333    difficult 10.166667    difficult
    ## 5      Kosmetik 11.000000    difficult 12.875000    difficult
    ## 6  Küchengeräte 11.750000    difficult  8.666667    difficult
    ## 7       Metalle  9.875000    difficult  9.458333    difficult
    ## 8     Spielzeug 12.333333    difficult 10.500000    difficult
    ## 9   Süßigkeiten  9.083333    difficult  9.083333    difficult
    ## 10     Werkzeug 11.541667    difficult 12.375000    difficult

``` r
# descriptive stats
cats %>% 
  group_by(difficulty) %>% 
  summarise(sd = sd(mean), mean = mean(mean))
```

    ## # A tibble: 2 x 3
    ##   difficulty    sd  mean
    ## * <chr>      <dbl> <dbl>
    ## 1 difficult   1.39  10.6
    ## 2 easy        2.51  18.1

``` r
# check for difference between easy and difficult categories across age groups
t <- t.test(mean ~ difficulty, data = cats)

# linear model to check for potential age effect
m_age_diff <- lm(mean ~ age + difficulty, data = cats)
summary(m_age_diff)
```

    ## 
    ## Call:
    ## lm(formula = mean ~ age + difficulty, data = cats)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -3.0042 -1.2552 -0.5729  1.5021  4.8833 
    ## 
    ## Coefficients:
    ##                Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)     10.1417     0.5456  18.587   <2e-16 ***
    ## ageYA            0.9875     0.6300   1.567    0.126    
    ## difficultyeasy   7.4458     0.6300  11.818    4e-14 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 1.992 on 37 degrees of freedom
    ## Multiple R-squared:  0.7934, Adjusted R-squared:  0.7823 
    ## F-statistic: 71.06 on 2 and 37 DF,  p-value: 2.13e-13

``` r
## determine p values of main effects
drop1(m_age_diff, test = "F")
```

    ## Single term deletions
    ## 
    ## Model:
    ## mean ~ age + difficulty
    ##            Df Sum of Sq    RSS     AIC  F value    Pr(>F)    
    ## <none>                  146.87  58.026                       
    ## age         1      9.75 156.62  58.598   2.4567    0.1255    
    ## difficulty  1    554.40 701.27 118.561 139.6696 4.002e-14 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
# extract table
tab_model(m_age_diff, 
  dv.labels = c("Mean number of items"),
  pred.labels = c("Intercept", "Age", "Difficulty"),
  string.pred = "Coefficient",
  string.ci = "Conf. Int (95%)",
  show.p = F,
  show.stat = T)
```

<table style="border-collapse:collapse; border:none;">
<tr>
<th style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm;  text-align:left; ">
 
</th>
<th colspan="3" style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm; ">
Mean number of items
</th>
</tr>
<tr>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  text-align:left; ">
Coefficient
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Estimates
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Conf. Int (95%)
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Statistic
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Intercept
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
10.14
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
9.04 – 11.25
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
18.59
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Age
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.99
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.29 – 2.26
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
1.57
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Difficulty
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
7.45
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
6.17 – 8.72
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
11.82
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm; border-top:1px solid;">
Observations
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left; border-top:1px solid;" colspan="3">
40
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
R<sup>2</sup> / R<sup>2</sup> adjusted
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.793 / 0.782
</td>
</tr>
</table>

``` r
# plot
SF_diff_age <- 
  ggplot(cats, aes(x = difficulty, y = mean, color = age)) +
  geom_boxplot(width = 0.35) +
  geom_jitter(height = 0, width = 0.1, alpha=0.8) +
  scale_colour_manual(values = c("#52257a", "#018571")) +
  facet_wrap(~ age) +
  ylab("Mean number of items") +
  apatheme +
  theme(legend.position=("none"), 
        legend.title=element_blank(), 
        axis.title.x = element_blank())
SF_diff_age
```

![](Analyses_pilot_data_MDN_LANG_files/figure-markdown_github/merge%20df%20for%20OA%20and%20YA-1.png)
