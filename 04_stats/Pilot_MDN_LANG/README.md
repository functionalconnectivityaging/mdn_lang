- Scripts:
    - Analyses_pilot_data_MDN_LANG.md: Analyses of pilot data for both age groups
- Results_OA and Results_YA  
Contains number of items for 30 semantic categories for 24 young and 24 older adults, respectively
