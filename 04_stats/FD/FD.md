# Load packages

``` r
library(here)
library(tidyverse)
library(magrittr)
```

# Read in data

``` r
path = here::here("FD")

all_files <- ""
seeds <- list.files(path = path, pattern = "txt")

for (i in 1:length(seeds)) {
  filedir <- seeds[i]
  seed <- read.table(filedir, sep = "")
  seed$subject = substr(filedir,10,15)
  all_files <- rbind(all_files, seed)
}

all_files <- all_files[-1,]     # delete first row of df since it's only NAs

all_subjects <- all_files
all_subjects$Age <- 'OA'
all_subjects$Age[grepl('JE', all_subjects$subject)] = 'YA'
all_subjects$subject <- ifelse((all_subjects$Age == "YA"), gsub("_.*", "", all_subjects$subject), all_subjects$subject)
all_subjects$subject <- ifelse((all_subjects$Age == "OA"), gsub(".tx", "", all_subjects$subject), all_subjects$subject)

all_subjects$subject <- as.factor(all_subjects$subject)
all_subjects$Age <- as.factor(all_subjects$Age)
all_subjects$V1 <- as.numeric(all_subjects$V1)
```

# Check if there’s a significant difference in the amount of FD regressors between both groups

``` r
df_counts <- all_subjects %>% 
  group_by(Age, subject) %>% 
  tally()

t.test(n ~ Age, data = df_counts) # There isn't a difference between groups
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  n by Age
    ## t = 1.6635, df = 28.764, p-value = 0.1071
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##  -1.877438 18.210771
    ## sample estimates:
    ## mean in group OA mean in group YA 
    ##         15.66667          7.50000

``` r
sd(df_counts$n[df_counts$Age == "OA"])
```

    ## [1] 20.04079

``` r
sd(df_counts$n[df_counts$Age == "YA"])
```

    ## [1] 8.345888

``` r
median(df_counts$n[df_counts$Age == "OA"])
```

    ## [1] 7

``` r
median(df_counts$n[df_counts$Age == "YA"])
```

    ## [1] 3.5

``` r
boxplot(df_counts$n ~ df_counts$Age)
```

![](FD_files/figure-markdown_github/unnamed-chunk-1-1.png)
