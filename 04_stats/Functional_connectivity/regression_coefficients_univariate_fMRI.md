# Load packages

``` r
library(here)
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(wesanderson)
library(berryFunctions)
library(data.table)
library(ggridges)
library(extrafont)
library(scales)
library(lme4)
library(emmeans)
library(sjPlot)
library(performance)
```

# Load data

``` r
data_file <- here::here("Functional_connectivity/Data/Cat_all_Count_all_ROIs_YA_2021.txt")

df <- read.table(data_file, header = T, sep = "\t")

df$Subj <- as.factor(df$Subj)
df$Age <- as.factor(df$Age)
df$Contrast <- as.factor(df$Contrast)
```

# Average beta coefficients across multiple-demand & default mode networks

``` r
df$MDN <- ifelse(df$Age == "OA", (df$LMFG + df$preSMA + df$LInsula + df$RInsula + df$RMFG + df$RIntracalcarine + df$LIntracalcarine + df$LMFG2 + df$SPL)/9, (df$LInsula + df$RInsula + df$RMFG + df$dACC + df$SPL)/5)

df$MDN <- (df$Insula + df$Insula1 + df$Insula2 + df$Insula3 + df$Insula4 + df$dACC + df$RInsula + df$RInsula1 + df$SPL + df$SPL1 + df$SPL2 + df$RMFG + df$RMFG1 + df$RMFG2)/14

df$DMN <- (df$RTempPole + df$RTempPole1 + df$RPrec + df$RPrec1 + df$RPrec2 + df$MTG + df$MTG1 + df$ACing + df$ACing1 + df$LAG + df$LAG1 + df$LAG2 + df$LAG3 + df$LAG4 + df$RAG + df$RAG1 + df$RAG2)/17

df <- gather(df, "MDN", "DMN", key = "Network", value = "Estimate")
df$Network <- factor(df$Network, levels = c("MDN", "DMN")) # relevel
```

# Model for multiple-demand & default mode mean coefficients

``` r
# Prepare df for model
df <- mutate(df, NetworkSum = Network)
contrasts(df$NetworkSum) <- contr.sum(2)
df <- mutate(df, AgeSum = Age)
contrasts(df$AgeSum) <- contr.sum(2)
df <- mutate(df, ContrastSum = Contrast)
contrasts(df$ContrastSum) <- contr.sum(2)

# model
m_Estimate <- lmer(Estimate ~ NetworkSum * AgeSum * ContrastSum + (1|Subj), data = df) # main model with all interaction terms
summary(m_Estimate)
```

    ## Linear mixed model fit by REML ['lmerMod']
    ## Formula: Estimate ~ NetworkSum * AgeSum * ContrastSum + (1 | Subj)
    ##    Data: df
    ## 
    ## REML criterion at convergence: 669
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -2.71892 -0.57853  0.04692  0.68583  2.67362 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  Subj     (Intercept) 0.06008  0.2451  
    ##  Residual             0.90573  0.9517  
    ## Number of obs: 232, groups:  Subj, 30
    ## 
    ## Fixed effects:
    ##                                  Estimate Std. Error t value
    ## (Intercept)                      -0.38872    0.07707  -5.044
    ## NetworkSum1                       1.06883    0.06252  17.096
    ## AgeSum1                           0.33607    0.06274   5.356
    ## ContrastSum1                      0.49237    0.06252   7.876
    ## NetworkSum1:AgeSum1              -0.02445    0.06252  -0.391
    ## NetworkSum1:ContrastSum1          1.14020    0.06252  18.238
    ## AgeSum1:ContrastSum1             -0.01948    0.06252  -0.312
    ## NetworkSum1:AgeSum1:ContrastSum1 -0.27095    0.06252  -4.334
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) NtwrS1 AgeSm1 CntrS1 NtS1:AS1 NS1:CS AS1:CS
    ## NetworkSum1 0.000                                             
    ## AgeSum1     0.034  0.000                                      
    ## ContrastSm1 0.000  0.000  0.000                               
    ## NtwrkS1:AS1 0.000  0.034  0.000  0.000                        
    ## NtwrkS1:CS1 0.000  0.000  0.000  0.000  0.000                 
    ## AgSm1:CntS1 0.000  0.000  0.000  0.034  0.000    0.000        
    ## NS1:AS1:CS1 0.000  0.000  0.000  0.000  0.000    0.034  0.000

``` r
r2(m_Estimate)
```

    ## # R2 for Mixed Models
    ## 
    ##   Conditional R2: 0.766
    ##      Marginal R2: 0.751

``` r
# LRTs
drop1(m_Estimate, test = "Chisq", ddf = "lme4") # drop three-way interaction
```

    ## Single term deletions
    ## 
    ## Model:
    ## Estimate ~ NetworkSum * AgeSum * ContrastSum + (1 | Subj)
    ##                               npar    AIC    LRT   Pr(Chi)    
    ## <none>                             659.66                     
    ## NetworkSum:AgeSum:ContrastSum    1 676.24 18.576 1.633e-05 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
m_Estimate_twoWay <- lmer(Estimate ~ NetworkSum + AgeSum + ContrastSum + NetworkSum:AgeSum + NetworkSum:ContrastSum + AgeSum:ContrastSum + (1|Subj), data = df, REML = FALSE) # model with two-way interactions 
drop1(m_Estimate_twoWay, test = "Chisq", ddf = "lme4") # drop two-way interaction
```

    ## Single term deletions
    ## 
    ## Model:
    ## Estimate ~ NetworkSum + AgeSum + ContrastSum + NetworkSum:AgeSum + 
    ##     NetworkSum:ContrastSum + AgeSum:ContrastSum + (1 | Subj)
    ##                        npar    AIC     LRT Pr(Chi)    
    ## <none>                      676.24                    
    ## NetworkSum:AgeSum         1 674.39   0.144  0.7039    
    ## NetworkSum:ContrastSum    1 870.38 196.137  <2e-16 ***
    ## AgeSum:ContrastSum        1 674.33   0.092  0.7620    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
m_Estimate_mainEffects <- lmer(Estimate ~ NetworkSum + AgeSum + ContrastSum + (1|Subj), data = df, REML = FALSE) # model with main effects
drop1(m_Estimate_mainEffects, test = "Chisq", ddf = "lme4") # drop main effects
```

    ## Single term deletions
    ## 
    ## Model:
    ## Estimate ~ NetworkSum + AgeSum + ContrastSum + (1 | Subj)
    ##             npar    AIC    LRT   Pr(Chi)    
    ## <none>           866.47                     
    ## NetworkSum     1 957.21 92.734 < 2.2e-16 ***
    ## AgeSum         1 875.50 11.025 0.0008989 ***
    ## ContrastSum    1 887.51 23.038 1.588e-06 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
# post-hoc tests
emmp <- emmeans(m_Estimate, pairwise ~ AgeSum * NetworkSum | ContrastSum, adjust = "bonferroni") # resolve interaction

emmp_df <- emmp$contrasts %>% summary(infer = T) %>% as.data.frame()
fwrite(format(emmp_df, digits = 2, nsmall = 2), sep = '\t', row.names = F, file = "mc_m_beta_weights.csv")

# extract table
tab_model(m_Estimate, 
  dv.labels = c("Beta weight"),
  pred.labels = c("Intercept", "Network", "Age", "Condition", "Network * Age", "Network * Condition", "Age * Condition", "Network * Age * Condition"),
  string.pred = "Coefficient",
  string.ci = "Conf. Int (95%)",
  show.p = F,
  show.stat = T,
  file = "coefficients_model.doc")
```

<table style="border-collapse:collapse; border:none;">
<tr>
<th style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm;  text-align:left; ">
 
</th>
<th colspan="3" style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm; ">
Beta weight
</th>
</tr>
<tr>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  text-align:left; ">
Coefficient
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Estimates
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Conf. Int (95%)
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Statistic
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Intercept
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.39
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.54 – -0.24
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-5.04
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Network
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
1.07
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.95 – 1.19
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
17.10
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Age
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.34
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.21 – 0.46
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
5.36
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Condition
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.49
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.37 – 0.61
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
7.88
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Network \* Age
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.02
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.15 – 0.10
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.39
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Network \* Condition
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
1.14
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
1.02 – 1.26
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
18.24
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Age \* Condition
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.02
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.14 – 0.10
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.31
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Network \* Age \* Condition
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.27
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.39 – -0.15
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-4.33
</td>
</tr>
<tr>
<td colspan="4" style="font-weight:bold; text-align:left; padding-top:.8em;">
Random Effects
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
σ<sup>2</sup>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.91
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
τ<sub>00</sub> <sub>Subj</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.06
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
ICC
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.06
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
N <sub>Subj</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
30
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm; border-top:1px solid;">
Observations
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left; border-top:1px solid;" colspan="3">
232
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
Marginal R<sup>2</sup> / Conditional R<sup>2</sup>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.751 / 0.766
</td>
</tr>
</table>

# Plot data

``` r
networks_estimates_fMRI <- 
  ggplot(df, aes(x = Network, y = Estimate, color = Network)) +
  geom_boxplot(width = 0.35) +
  geom_jitter(height = 0, width = 0.1, alpha=0.8) +
  scale_colour_manual(values = c("#c93412", "#6d7f85","#c93412", "#6d7f85")) +
  ylab("Parameter estimate") +
  #xlab("Condition") +
  scale_y_continuous(limits = c(-5, 5)) +
  facet_wrap(~ Contrast + Age, ncol = 4) +
  apatheme +
  theme(legend.position=("none"), 
        legend.title=element_blank(), 
        axis.title.x = element_blank())
        #strip.text.x = element_blank(), # This removes labels from facets
        #axis.text.x = element_text(angle = 45, vjust = 0.5, hjust = 0.5))
networks_estimates_fMRI
```

![](regression_coefficients_univariate_fMRI_files/figure-markdown_github/Plot-1.png)

