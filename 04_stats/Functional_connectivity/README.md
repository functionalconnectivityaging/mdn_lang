- Scripts:
   - regression_coefficients_univariate_fMRI.md: Analysis of parameter estimates from univariate contrasts
   - PPI_connectivity_within_and_between_networks.md: Regression models for functional connectivity measures within and between networks
   - PPI_connectivity_behavioral_relevance.md: Analysis of interactions and correlations of functional connectivity measures with in- and out of scanner performance      
- Data/Cat_all_Count_all_transformed_AE_JE.txt  
Contains extracted beta weights for regions of interest (ROIs) within the domain-general systems multiple-demand network (MDN) and default mode network (DMN) for older and young adults (see Table 2 and Figure 3c in Paper)
- Data/PPI_Beta_weights  
Contains extracted beta weights for each seed-to-target combination of ROIs in MDN and DMN (see Table 2 and Figure 7 in Paper)
- RData  
RData structures containing data frames for use in Rproj analyses
