# Load packages

``` r
library(here)
library(psych)
library(stats)
library(reshape2)
library(plyr)
library(tidyverse)
library(wesanderson) 
library(data.table)
library(scales) 
library(lme4)
library(emmeans)
library(RColorBrewer)
library(ggeffects)
library(sjPlot)
library(ggpubr)
library(ppcor)
library(fitdistrplus)
source('https://osf.io/ws6xc/download') # source Meff function
```

# Load R data

``` r
load("RData/behav_df_edu.RData") # df with raw values for both conditions
load("RData/df_categories_04_2021.RData") # df with raw values for task "semantic fluency"
load("RData/NP_Conn_df.RData") # df with neuropsychological values and three connectivity measures per participant
```

# Create behavioral df for both tasks

``` r
network_df <- network_df %>% rename("Subj" = "Subject")
network_df <- spread(network_df, Network, Cats_Counting)

path = here::here("Behavioral/RData/all_subjects.RData")
load(path)

behav_df <- merge(network_df, all_subjects, by = c("Subj", "Age"))
behav_df$Difficulty <- factor(behav_df$Difficulty, levels = c("Easy", "Difficult", "Counting_forw", "Counting_backw"))
behav_df$DMN_z <- scale(behav_df$DMN, scale = F)
behav_df$MDN_z <- scale(behav_df$MDN, scale = F)
behav_df$MDN_DMN_z <- scale(behav_df$MDN_DMN, scale = F)
behav_df$Onset <- as.numeric(behav_df$Onset)
behav_df$Correct[is.na(behav_df$Correct)] <- 0
behav_df$Correct <- as.factor(behav_df$Correct)

behav_df <- mutate(behav_df, ConditionSum = Condition)
contrasts(behav_df$ConditionSum) <- contr.sum(2)/2
behav_df <- mutate(behav_df, AgeSum = Age)
contrasts(behav_df$AgeSum) <- contr.sum(2)/2

save(behav_df, file = "RData/behav_df_01_2021.RData")
```

# Prepare df for task semantic fluency

``` r
df_categories <- behav_df_edu[behav_df_edu$Condition == "Categories", ] # drop task Counting
df_categories = droplevels(df_categories) # drop unused factor levels
df_categories$MDN_DMN_z <- scale(df_categories$MDN_DMN, scale = F)
df_categories$MDN_z <- scale(df_categories$MDN, scale = F)
df_categories$DMN_z <- scale(df_categories$DMN, scale = F)
df_categories$Education <- scale(df_categories$Education)

df_categories$DifficultySum <- df_categories$Difficulty
contrasts(df_categories$DifficultySum) <- contr.sum(2)/2
contrasts(df_categories$AgeSum) <- contr.sum(2)/2

save(df_categories, file = "RData/df_categories_04_2021.RData")
```

# Prepare df for response time

``` r
# prepare df
df_categories.RT = df_categories[!is.na(df_categories$Onset),] # Remove NAs from Onset column and create df
df_categories.RT <- droplevels(df_categories.RT) # drop unused factor levels
df_categories.RT$MDN_DMN_z <- scale(df_categories.RT$MDN_DMN, scale = F) # re-center connectivity measures
df_categories.RT$MDN_z <- scale(df_categories.RT$MDN, scale = F)
df_categories.RT$DMN_z <- scale(df_categories.RT$DMN, scale = F)
contrasts(df_categories.RT$AgeSum) <- contr.sum(2)/2 # re-apply sum coding to factor

# Decide whether Gamma distribution or log-transformation is better for RT data
# fit.norm <- fitdist(df_categories.RT$Onset, "lnorm")
# fit.gamma <- fitdist(df_categories.RT$Onset, "gamma")
# fit.norm$aic # AIC: 135231.1 --> log transformation is better
# fit.gamma$aic # AIC: 136392.8
```

# Regression analyses

## GLMM - accuracy

``` r
# model
Correct_Categories <- glmer(Correct ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + Education + (1|Subj) + (1|Category), data = df_categories, family = binomial(link = "logit"), control=glmerControl(optimizer = 'bobyqa'))
summary(Correct_Categories)
```

    ## Generalized linear mixed model fit by maximum likelihood (Laplace
    ##   Approximation) [glmerMod]
    ##  Family: binomial  ( logit )
    ## Formula: Correct ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + Education +  
    ##     (1 | Subj) + (1 | Category)
    ##    Data: df_categories
    ## Control: glmerControl(optimizer = "bobyqa")
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   4827.0   4906.1  -2402.5   4805.0     9826 
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -12.6980   0.1077   0.2076   0.3496   0.6315 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  Subj     (Intercept) 0.208    0.4561  
    ##  Category (Intercept) 1.707    1.3067  
    ## Number of obs: 9837, groups:  Subj, 31; Category, 20
    ## 
    ## Fixed effects:
    ##                   Estimate Std. Error z value Pr(>|z|)    
    ## (Intercept)        3.08736    0.31216   9.890   <2e-16 ***
    ## MDN_z              0.21638    0.82750   0.261   0.7937    
    ## DMN_z             -0.81781    0.81870  -0.999   0.3178    
    ## MDN_DMN_z          0.55985    0.90966   0.615   0.5383    
    ## AgeSum1           -0.17424    0.11333  -1.537   0.1242    
    ## Education         -0.15289    0.07065  -2.164   0.0305 *  
    ## MDN_z:AgeSum1      1.15207    1.57177   0.733   0.4636    
    ## DMN_z:AgeSum1      2.06427    1.44752   1.426   0.1538    
    ## MDN_DMN_z:AgeSum1 -2.40747    1.53457  -1.569   0.1167    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) MDN_z  DMN_z  MDN_DMN_z AgeSm1 Eductn MDN_:A DMN_:A
    ## MDN_z       -0.006                                                    
    ## DMN_z       -0.006 -0.006                                             
    ## MDN_DMN_z    0.015 -0.566 -0.467                                      
    ## AgeSum1      0.006 -0.275  0.152  0.075                               
    ## Education   -0.005 -0.143  0.249 -0.179     0.674                     
    ## MDN_z:AgSm1 -0.025 -0.076  0.100  0.022     0.116  0.107              
    ## DMN_z:AgSm1 -0.016 -0.217 -0.222  0.211     0.025  0.004  0.274       
    ## MDN_DMN_:AS  0.030  0.096 -0.119  0.049    -0.061 -0.036 -0.647 -0.569

``` r
# LRTs
drop1(Correct_Categories, test = "Chisq") # drop two-way interactions
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + Education + 
    ##     (1 | Subj) + (1 | Category)
    ##                  npar    AIC    LRT Pr(Chi)  
    ## <none>                4827.0                 
    ## Education           1 4829.6 4.6152 0.03169 *
    ## MDN_z:AgeSum        1 4825.5 0.5246 0.46890  
    ## DMN_z:AgeSum        1 4827.0 2.0019 0.15710  
    ## MDN_DMN_z:AgeSum    1 4827.4 2.4305 0.11900  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
Correct_Categories_mainEffects <- glmer(Correct ~ MDN_z + DMN_z + MDN_DMN_z + AgeSum + Education + (1|Subj) + (1|Category), data = df_categories, family = binomial(link = "logit"), control=glmerControl(optimizer = 'bobyqa')) # model with main effects
drop1(Correct_Categories_mainEffects, test = "Chisq") # drop main effects
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ MDN_z + DMN_z + MDN_DMN_z + AgeSum + Education + (1 | 
    ##     Subj) + (1 | Category)
    ##           npar    AIC    LRT Pr(Chi)  
    ## <none>         4823.9                 
    ## MDN_z        1 4822.2 0.2867  0.5924  
    ## DMN_z        1 4823.0 1.0635  0.3024  
    ## MDN_DMN_z    1 4822.2 0.3464  0.5561  
    ## AgeSum       1 4824.5 2.5512  0.1102  
    ## Education    1 4826.7 4.8218  0.0281 *
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

## LMM - response time

``` r
# model
RT_Categories <- lmer(log(Onset) ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + Education + (1|Subj) + (1|Category), data = df_categories.RT, control = lmerControl(optimizer = "bobyqa"), REML = F) 
summary(RT_Categories)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: log(Onset) ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + Education +  
    ##     (1 | Subj) + (1 | Category)
    ##    Data: df_categories.RT
    ## Control: lmerControl(optimizer = "bobyqa")
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   7845.1   7931.2  -3910.6   7821.1     9663 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -9.2733 -0.6448 -0.1346  0.5039  4.9030 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  Subj     (Intercept) 0.013186 0.11483 
    ##  Category (Intercept) 0.004169 0.06457 
    ##  Residual             0.129235 0.35949 
    ## Number of obs: 9675, groups:  Subj, 31; Category, 20
    ## 
    ## Fixed effects:
    ##                    Estimate Std. Error t value
    ## (Intercept)        6.525401   0.025508 255.815
    ## MDN_z             -0.127005   0.079161  -1.604
    ## DMN_z             -0.353738   0.087981  -4.021
    ## MDN_DMN_z          0.534894   0.087441   6.117
    ## AgeSum1            0.057579   0.011137   5.170
    ## Education         -0.007191   0.006927  -1.038
    ## MDN_z:AgeSum1      0.939056   0.172515   5.443
    ## DMN_z:AgeSum1      0.904743   0.146856   6.161
    ## MDN_DMN_z:AgeSum1 -0.787088   0.160446  -4.906
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) MDN_z  DMN_z  MDN_DMN_z AgeSm1 Eductn MDN_:A DMN_:A
    ## MDN_z       -0.003                                                    
    ## DMN_z       -0.009 -0.157                                             
    ## MDN_DMN_z    0.013 -0.487 -0.388                                      
    ## AgeSum1      0.016 -0.364  0.125  0.125                               
    ## Education    0.004 -0.264  0.244 -0.107     0.696                     
    ## MDN_z:AgSm1 -0.041 -0.102  0.131 -0.021     0.140  0.208              
    ## DMN_z:AgSm1 -0.031 -0.083 -0.128  0.100     0.024 -0.010  0.330       
    ## MDN_DMN_:AS  0.053  0.047 -0.182  0.142    -0.036 -0.080 -0.679 -0.597

``` r
# LRTs
drop1(RT_Categories, test = "Chisq") # drop two-way interactions
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ (MDN_z + DMN_z + MDN_DMN_z) * AgeSum + Education + 
    ##     (1 | Subj) + (1 | Category)
    ##                  npar    AIC    LRT   Pr(Chi)    
    ## <none>                7845.1                     
    ## Education           1 7844.2  1.075    0.2997    
    ## MDN_z:AgeSum        1 7872.1 29.008 7.208e-08 ***
    ## DMN_z:AgeSum        1 7880.5 37.400 9.624e-10 ***
    ## MDN_DMN_z:AgeSum    1 7866.9 23.746 1.099e-06 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
RT_Categories_mainEffects <- lmer(log(Onset) ~ MDN_z + DMN_z + MDN_DMN_z + AgeSum + Education + (1|Subj) + (1|Category), data = df_categories.RT, control = lmerControl(optimizer = 'bobyqa'), REML = F)
drop1(RT_Categories_mainEffects, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ MDN_z + DMN_z + MDN_DMN_z + AgeSum + Education + 
    ##     (1 | Subj) + (1 | Category)
    ##           npar    AIC     LRT   Pr(Chi)    
    ## <none>         7889.8                      
    ## MDN_z        1 7888.3  0.4700    0.4930    
    ## DMN_z        1 7903.0 15.1619 9.867e-05 ***
    ## MDN_DMN_z    1 7919.3 31.4414 2.055e-08 ***
    ## AgeSum       1 7908.1 20.2651 6.742e-06 ***
    ## Education    1 7891.3  3.5183    0.0607 .  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
# post-hoc tests
t <- emtrends(RT_Categories, pairwise ~ AgeSum, var = "MDN_z", transform = "response", adjust = "bonferroni") # this gives simple slopes of MDN_z per factor level as well as pairwise comparisons
t
```

    ## $emtrends
    ##  AgeSum MDN_z.trend   SE  df asymp.LCL asymp.UCL
    ##  OA             241 77.7 Inf      88.1       393
    ##  YA            -395 81.1 Inf    -554.3      -236
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ##  contrast estimate  SE  df z.ratio p.value
    ##  OA - YA       636 118 Inf 5.398   <.0001 
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding

``` r
t$contrasts %>% confint()
```

    ##  contrast estimate  SE  df asymp.LCL asymp.UCL
    ##  OA - YA       636 118 Inf       405       867
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95

``` r
t1 <- emtrends(RT_Categories, pairwise ~ AgeSum, var = "DMN_z", transform = "response", adjust = "bonferroni") # this gives simple slopes of DMN_z per factor level as well as pairwise comparisons
t1
```

    ## $emtrends
    ##  AgeSum DMN_z.trend   SE  df asymp.LCL asymp.UCL
    ##  OA            69.3 75.3 Inf     -78.2       217
    ##  YA          -534.3 81.8 Inf    -694.6      -374
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ##  contrast estimate  SE  df z.ratio p.value
    ##  OA - YA       604 100 Inf 6.006   <.0001 
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding

``` r
t1$contrasts %>% confint()
```

    ##  contrast estimate  SE  df asymp.LCL asymp.UCL
    ##  OA - YA       604 100 Inf       407       800
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95

``` r
t2 <- emtrends(RT_Categories, pairwise ~ AgeSum, var = "MDN_DMN_z", transform = "response", adjust = "bonferroni") # this gives simple slopes of MDN_DMN_z per factor level as well as pairwise comparisons
t2
```

    ## $emtrends
    ##  AgeSum MDN_DMN_z.trend   SE  df asymp.LCL asymp.UCL
    ##  OA                99.3 89.2 Inf     -75.6       274
    ##  YA               615.6 73.8 Inf     470.9       760
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ##  contrast estimate  SE  df z.ratio p.value
    ##  OA - YA      -516 110 Inf -4.690  <.0001 
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding

``` r
t2$contrasts %>% confint()
```

    ##  contrast estimate  SE  df asymp.LCL asymp.UCL
    ##  OA - YA      -516 110 Inf      -732      -301
    ## 
    ## Degrees-of-freedom method: inherited from asymptotic when re-gridding 
    ## Confidence level used: 0.95

``` r
# plot
RT_Categories.MDN <- ggpredict(RT_Categories, terms = c("MDN_z[all]", "AgeSum"))
RT_Categories.plot.MDN <- plot(RT_Categories.MDN) +
  scale_fill_manual(values = c("#52257a", "#018571")) +
  scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
  coord_cartesian(ylim = c(500, 900)) +
  apatheme +
  theme(plot.title = element_blank())
RT_Categories.plot.MDN
```

![](PPI_connectivity_behavioral_relevance_covEducation_files/figure-markdown_github/unnamed-chunk-6-1.png)

``` r
RT_Categories.DMN <- ggpredict(RT_Categories, terms = c("DMN_z[all]", "AgeSum"))
RT_Categories.plot.DMN <- plot(RT_Categories.DMN) +
  scale_fill_manual(values = c("#52257a", "#018571")) +
  scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
  coord_cartesian(ylim = c(500, 900)) +
  apatheme +
  theme(plot.title = element_blank())
RT_Categories.plot.DMN
```

![](PPI_connectivity_behavioral_relevance_covEducation_files/figure-markdown_github/unnamed-chunk-6-2.png)

``` r
RT_Categories.MDN_DMN <- ggpredict(RT_Categories, terms = c("MDN_DMN_z[all]", "AgeSum"))
RT_Categories.plot.MDN_DMN <- plot(RT_Categories.MDN_DMN) +
  scale_fill_manual(values = c("#52257a", "#018571")) +
  scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
  coord_cartesian(ylim = c(500, 900)) +
  apatheme +
  theme(plot.title = element_blank())
RT_Categories.plot.MDN_DMN
```

![](PPI_connectivity_behavioral_relevance_covEducation_files/figure-markdown_github/unnamed-chunk-6-3.png)

## Extract table plot for both models

``` r
tab_model(Correct_Categories, RT_Categories,
  dv.labels = c("Accuracy", "Response time"),
  pred.labels = c("Intercept", "Within-MDN FC", "Within-DMN FC", "Between-network FC", "Age", "Education", "Within-MDN FC * Age", 
                  "Within-DMN FC * Age", "Between-network FC * Age"),
  string.pred = "Coefficient",
  string.ci = "Conf. Int (95%)", 
  transform = NULL,
  show.p = F,
  show.stat = T,
  file = "connectivity_models.doc")
```

<table style="border-collapse:collapse; border:none;">
<tr>
<th style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm;  text-align:left; ">
 
</th>
<th colspan="3" style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm; ">
Accuracy
</th>
<th colspan="3" style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm; ">
Response time
</th>
</tr>
<tr>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  text-align:left; ">
Coefficient
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Log-Odds
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Conf. Int (95%)
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Statistic
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Estimates
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Conf. Int (95%)
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  col7">
Statistic
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Intercept
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
3.09
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
2.48 – 3.70
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
9.89
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
6.53
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
6.48 – 6.58
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
255.82
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Within-MDN FC
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.22
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.41 – 1.84
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.26
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.13
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.28 – 0.03
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
-1.60
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Within-DMN FC
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.82
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-2.42 – 0.79
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.00
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.35
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.53 – -0.18
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
-4.02
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Between-network FC
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.56
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.22 – 2.34
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.62
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.53
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.36 – 0.71
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
6.12
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Age
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.17
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.40 – 0.05
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.54
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.06
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.04 – 0.08
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
5.17
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Education
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.15
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.29 – -0.01
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-2.16
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.01
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.02 – 0.01
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
-1.04
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Within-MDN FC \* Age
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
1.15
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.93 – 4.23
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.73
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.94
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.60 – 1.28
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
5.44
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Within-DMN FC \* Age
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
2.06
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.77 – 4.90
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
1.43
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.90
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.62 – 1.19
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
6.16
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Between-network FC \* Age
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-2.41
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-5.42 – 0.60
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.57
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.79
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.10 – -0.47
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
-4.91
</td>
</tr>
<tr>
<td colspan="7" style="font-weight:bold; text-align:left; padding-top:.8em;">
Random Effects
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
σ<sup>2</sup>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
3.29
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.13
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
τ<sub>00</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.21 <sub>Subj</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.01 <sub>Subj</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
1.71 <sub>Category</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.00 <sub>Category</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
ICC
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.37
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.12
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
N
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
31 <sub>Subj</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
31 <sub>Subj</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
20 <sub>Category</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
20 <sub>Category</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm; border-top:1px solid;">
Observations
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left; border-top:1px solid;" colspan="3">
9837
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left; border-top:1px solid;" colspan="3">
9675
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
Marginal R<sup>2</sup> / Conditional R<sup>2</sup>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.005 / 0.371
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.026 / 0.142
</td>
</tr>
</table>

# Correlation analyses

## Prepare df with NP

``` r
load("RData/network_df_01_2021.RData") # df with one value per connectivity measures for each participant
p <- here::here("Behavioral/RData/NP_all.RData") # df for neuropsychological data
load(p)

NP_all$Age <- 'OA' # create column for age groups
NP_all$Age[grepl('JE', NP_all$Participant)] = 'YA'
NP_all$Participant <- gsub("_.*", "", NP_all$Participant) # remove age group from subject column
NP_all$Participant <- as.factor(NP_all$Participant) # factorize column
NP_all <- rename(NP_all, "Subject" = "Participant") # rename column
NP_all <- NP_all[-c(2,9,10,12,13,20,21,22,23,24,26,27,28,29)] # remove unnecessary columns

NP_Conn_df <- merge(network_df, NP_all, by = c("Subject", "Age")) # merge dfs

NP_Conn_df$MDN_z <- scale(NP_Conn_df$MDN, scale = F) # center FC measure
NP_Conn_df$DMN_z <- scale(NP_Conn_df$DMN, scale = F) # center FC measure
NP_Conn_df$MDN_DMN_z <- scale(NP_Conn_df$MDN_DMN, scale = F) # center FC measure
NP_Conn_df$TMT_A_z <- scale(NP_Conn_df$TMT_A) # scale variables
NP_Conn_df$TMT_B_z <- scale(NP_Conn_df$TMT_B) # scale variables
NP_Conn_df$RWT_Vornamen_korrekt_z <- scale(NP_Conn_df$RWT_Vornamen_korrekt) # scale variables
NP_Conn_df$RWT_Hobbys_korrekt_z <- scale(NP_Conn_df$RWT_Hobbys_korrekt) # scale variables
NP_Conn_df$Reading_span_z <- scale(NP_Conn_df$Reading_span) # scale variables
NP_Conn_df$WST_z <- scale(NP_Conn_df$WST) # scale variables
NP_Conn_df$DSST_z <- scale(NP_Conn_df$DSST) # scale variables
NP_Conn_df$Bildung <- scale(NP_Conn_df$Bildung)

save(NP_Conn_df, file = "RData/NP_Conn_df.RData")
```

## Run factor analysis and then correlations

``` r
##################################### Factor analysis
NP_factors <- NP_Conn_df[,c(23,24,25,26,27,28,29)] # df only with behavioral tests
NP.fa <- factanal(NP_factors, factors = 2, scores = "regression", rotation = "varimax") # run factor analysis, assume two factors --> looks good, third factor has eigenvalue < 1

plot(NP.fa$loadings[,1], # plot factors
     NP.fa$loadings[,2],
     xlab = "Factor 1",
     ylab = "Factor 2",
     ylim = c(-1,1),
     xlim = c(-1,1),
     main = "Varimax rotation")
text(NP.fa$loadings[,1]-0.08, 
     NP.fa$loadings[,2]-0.08,
      colnames(NP_factors),
      col="blue")
abline(h = 0, v = 0)
```

![](PPI_connectivity_behavioral_relevance_covEducation_files/figure-markdown_github/unnamed-chunk-9-1.png)

``` r
NP_Conn_df <- cbind(NP_Conn_df, NP.fa$scores) # bind factor score columns with df for correlation analyses

NP_Conn_df$AgeBin <- 1 # create binarised column for age to run partial correlation controlling for age group
NP_Conn_df$AgeBin <- ifelse(NP_Conn_df$Age == "YA", 0, 1)
NP_Conn_df$AgeBin <- as.numeric(NP_Conn_df$AgeBin)


################## Partial correlations controlling for age
pcc <- with(NP_Conn_df, pcor.test(MDN, Factor1, AgeBin)) # r = 0.3602337, p = 0.005913755   
pcc
```

    ##    estimate     p.value statistic  n gp  Method
    ## 1 0.3602337 0.005913755  2.863837 58  1 pearson

``` r
p.adjust(pcc$p.value, method = "bonferroni", n = 3) # adjust for multiple comparison (n = 3)
```

    ## [1] 0.01774126

``` r
with(NP_Conn_df, pcor.test(DMN, Factor1, AgeBin)) # n.s.
```

    ##     estimate   p.value statistic  n gp  Method
    ## 1 0.07994527 0.5544205 0.5947938 58  1 pearson

``` r
with(NP_Conn_df, pcor.test(MDN_DMN, Factor1, AgeBin)) # n.s.
```

    ##   estimate   p.value statistic  n gp  Method
    ## 1 0.218667 0.1022235  1.661897 58  1 pearson

``` r
with(NP_Conn_df, pcor.test(MDN, Factor2, AgeBin)) # n.s.
```

    ##     estimate   p.value statistic  n gp  Method
    ## 1 0.05717251 0.6727142 0.4246973 58  1 pearson

``` r
with(NP_Conn_df, pcor.test(DMN, Factor2, AgeBin)) # n.s.
```

    ##      estimate   p.value  statistic  n gp  Method
    ## 1 -0.03808281 0.7785178 -0.2826347 58  1 pearson

``` r
with(NP_Conn_df, pcor.test(MDN_DMN, Factor2, AgeBin)) # n.s.
```

    ##    estimate   p.value statistic  n gp  Method
    ## 1 0.1874059 0.1627325  1.414908 58  1 pearson

``` r
y_resid <- resid(lm(MDN ~ AgeBin, NP_Conn_df)) # prepare resid for partial correlation plot for MDN and Factor 1
x_resid <- resid(lm(Factor1 ~ AgeBin, NP_Conn_df)) # prepare resid for partial correlation plot for MDN and Factor 1

pcc_plot <- ggplot(NP_Conn_df, aes(x = x_resid, y = y_resid)) + # plot partial correlation
  geom_smooth(method=lm) +
  geom_point() +
  labs(x="Executive functions (factor 1) | Age", y = "MDN | Age") +
  apatheme
pcc_plot
```

    ## `geom_smooth()` using formula 'y ~ x'

![](PPI_connectivity_behavioral_relevance_covEducation_files/figure-markdown_github/unnamed-chunk-9-2.png)

``` r
############# Only OA
NP_Conn_df_OA <- NP_Conn_df[NP_Conn_df$Age == "OA", ] # filter df for OA
NP_Conn_df_OA = droplevels(NP_Conn_df_OA) # drop unused factor levels

b <- with(NP_Conn_df_OA, pcor.test(Factor1, MDN, Bildung)) # ??

a <- with(NP_Conn_df_OA, cor.test(Factor1, MDN)) # r = 0.4574339, p = 0.01439
a
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor1 and MDN
    ## t = 2.623, df = 26, p-value = 0.01439
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  0.1017156 0.7094393
    ## sample estimates:
    ##       cor 
    ## 0.4574339

``` r
p.adjust(a$p.value, method = "bonferroni", n = 3)
```

    ## [1] 0.04316088

``` r
with(NP_Conn_df_OA, cor.test(Factor1, DMN)) # n.s.
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor1 and DMN
    ## t = 0.75754, df = 26, p-value = 0.4555
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.2392406  0.4930010
    ## sample estimates:
    ##       cor 
    ## 0.1469527

``` r
with(NP_Conn_df_OA, cor.test(Factor1, MDN_DMN)) # n.s.
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor1 and MDN_DMN
    ## t = 0.70559, df = 26, p-value = 0.4867
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.2487257  0.4853288
    ## sample estimates:
    ##       cor 
    ## 0.1370705

``` r
with(NP_Conn_df_OA, cor.test(Factor2, MDN)) # n.s.
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor2 and MDN
    ## t = -0.91211, df = 26, p-value = 0.3701
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.5153087  0.2108440
    ## sample estimates:
    ##        cor 
    ## -0.1760839

``` r
with(NP_Conn_df_OA, cor.test(Factor2, DMN)) # n.s.
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor2 and DMN
    ## t = -1.9514, df = 26, p-value = 0.06186
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.64455481  0.01805666
    ## sample estimates:
    ##        cor 
    ## -0.3574281

``` r
c <- with(NP_Conn_df_OA, cor.test(Factor2, MDN_DMN)) # n.s.
p.adjust(c$p.value, method = "bonferroni", n = 3) # adjust p value for multiple comparisons for plot
```

    ## [1] 1

``` r
cor_plot_OA <- ggscatter(NP_Conn_df, x = "MDN_z", y = "Factor1", # plot for significant correlation for factor 1 and MDN in OA
          color = "Age", palette = c("#52257a", "#018571"),
          add = "reg.line", conf.int = TRUE, cor.coef = F,
          xlab = "Within-MDN connectivity", ylab = "Executive functions") +
  stat_cor(aes(color = Age), method = "pearson") +
  apatheme
cor_plot_OA <- ggpar(cor_plot_OA, xlim = c(-0.2, 0.2), ylim = c(-3,3))
cor_plot_OA  
```

    ## `geom_smooth()` using formula 'y ~ x'

![](PPI_connectivity_behavioral_relevance_covEducation_files/figure-markdown_github/unnamed-chunk-9-3.png)

``` r
############# only YA
NP_Conn_df_YA <- NP_Conn_df[NP_Conn_df$Age == "YA", ] # filter df for YA
NP_Conn_df_YA = droplevels(NP_Conn_df_YA) # drop unused factor levels

d <- with(NP_Conn_df_YA, cor.test(Factor1, MDN)) # n.s.
d
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor1 and MDN
    ## t = 1.3792, df = 28, p-value = 0.1788
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.1188590  0.5614629
    ## sample estimates:
    ##       cor 
    ## 0.2522102

``` r
p.adjust(d$p.value, method = "bonferroni", n = 3) # adjust p value for multiple comparisons for plot
```

    ## [1] 0.5362932

``` r
with(NP_Conn_df_YA, cor.test(Factor1, DMN)) # n.s.
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor1 and DMN
    ## t = -0.094564, df = 28, p-value = 0.9253
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.3757186  0.3446197
    ## sample estimates:
    ##         cor 
    ## -0.01786798

``` r
with(NP_Conn_df_YA, cor.test(Factor1, MDN_DMN)) # n.s.
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor1 and MDN_DMN
    ## t = 1.6992, df = 28, p-value = 0.1004
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.06127745  0.59992895
    ## sample estimates:
    ##       cor 
    ## 0.3057414

``` r
with(NP_Conn_df_YA, cor.test(Factor2, MDN)) # n.s.
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor2 and MDN
    ## t = 1.5532, df = 28, p-value = 0.1316
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.08751035  0.58277572
    ## sample estimates:
    ##       cor 
    ## 0.2816382

``` r
with(NP_Conn_df_YA, cor.test(Factor2, DMN)) # n.s.
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor2 and DMN
    ## t = 1.9795, df = 28, p-value = 0.05767
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.01132387  0.63099408
    ## sample estimates:
    ##       cor 
    ## 0.3503748

``` r
b <- with(NP_Conn_df_YA, cor.test(Factor2, MDN_DMN)) # r = 0.4753153, p = 0.007943
p.adjust(b$p.value, method = "bonferroni", n = 3)
```

    ## [1] 0.02382931

``` r
cor_plot_YA <- ggscatter(NP_Conn_df, x = "MDN_DMN_z", y = "Factor2", # plot for significant correlation for factor 2 and MDN_DMN in YA
          color = "Age", palette = c("#52257a", "#018571"),
          add = "reg.line", conf.int = TRUE, 
          cor.coef = F, cor.method = "pearson",
          xlab = "Between MDN and DMN network connectivity", ylab = "Semantic memory") +
  stat_cor(aes(color = Age)) +
  apatheme
cor_plot_YA <- ggpar(cor_plot_YA, ylim = c(-3,3))
cor_plot_YA
```

    ## `geom_smooth()` using formula 'y ~ x'

![](PPI_connectivity_behavioral_relevance_covEducation_files/figure-markdown_github/unnamed-chunk-9-4.png)
