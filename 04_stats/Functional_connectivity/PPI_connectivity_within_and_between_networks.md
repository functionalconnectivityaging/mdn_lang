# Load packages

``` r
library(here)                         
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(magrittr)
library(scales)
library(lme4)
library(emmeans)
library(RColorBrewer)
library(ggeffects)
library(sjPlot)
library(stats)
library(ggplot2)
library(data.table)
library(ggpubr)
library(ppcor)
library(fitdistrplus)
source('https://osf.io/ws6xc/download') # source Meff function
```

# Load data

``` r
load("RData/all_subjects_01_2021.RData")
load("RData/results.Cats.counting.AE_01_2021.RData")
load("RData/results.Cats.Counting.JE_01_2021.RData")
load("RData/results.network_01_2021.RData")
load("RData/behav_df_01_2021.RData") # This is a big df with single values
load("RData/network_df_01_2021.RData") 

load("RData/alpha_meff_AE_01_2021.RData")
load("RData/alpha_meff_JE_01_2021.RData")
load("RData/alpha_meff_network.RData")
```

# Young adults

## Loop for Models for Semantic fluecy \> Counting

``` r
# prepare df
Cats.counting <- all_subjects[, -4] # delete column with values Cats>Rest
Cats.counting.JE <- subset(Cats.counting, Age == "YA")
Cats.counting.JE <- unite(Cats.counting.JE, "Seed_target", ROI_seed, ROI_target, sep = "_")
Cats.counting.JE <- spread(Cats.counting.JE, key = Seed_target, value = Cats_Counting)

# create data frame to store results
results.Cats.counting.JE <- data.frame()

# loop through the columns
for(var in names(Cats.counting.JE)[c(3:931,length(Cats.counting.JE))]){
        # dynamically generate formula
        fmla <- as.formula(paste0(var, "~ 1"))

        # fit glm model
        fit <- glm(fmla, data = Cats.counting.JE)

        ## capture summary stats
        intercept <- coef(summary(fit))[1]
        std.error <- coef(summary(fit))[2]
        t.value <- coef(summary(fit))[3]
        p.value <- coef(summary(fit))[4]
        AIC <- AIC(fit)
        Deviance <- deviance(fit)

        # get coefficents of fit
        cfit <- coef(summary(fit))

        # create temporary data frame
        df <- data.frame(var = var, intercept = cfit[1],
                         std.error = cfit[2], t.value = cfit[3], p.value = cfit[4],
                         AIC = AIC(fit), Deviance = deviance(fit), stringsAsFactors = F)

        # bind rows of temporary data frame to the results data frame
        results.Cats.counting.JE <- rbind(results.Cats.counting.JE, df)
}
```


## Heatmap thresholded with Meff correction - Semantic fluency \> Counting

``` r
Cat_count_full_th_JE <- ggplot(results.Cats.counting.JE, aes(Seed, Target)) +
  geom_raster(aes(fill = t.value)) +
  geom_tile(aes(color = p.value < alpha_meff_JE), fill = '#00000000', size = 1) + 
  scale_fill_gradientn(colours = palet, values=rescale(c(-10,10)), limits = c(-10,10), breaks = c(-10, -5, 0, 5, 10), oob = scales::squish, na.value = "white") +
  scale_color_manual(guide = "none", values = c(NA, "black")) +
  apatheme +
  theme(
    axis.text.x = element_text(angle = 90, vjust = 0.2, hjust = 0.95),
    axis.line = element_blank(),
    axis.ticks = element_blank())
Cat_count_full_th_JE
```

![](PPI_connectivity_within_and_between_networks_files/figure-markdown_github/This%20is%20thresholded%20plot,%20either%20Bonferroni-%20or%20Meff-corrected-1.png)


# Older adults	

## Loop for Models for Semantic fluency \> Counting

``` r
# prepare df
Cats.counting <- all_subjects[, -4] # delete column with values Cats>Rest
Cats.counting.AE <- subset(Cats.counting, Age == "OA")
Cats.counting.AE <- unite(Cats.counting.AE, "Seed_target", ROI_seed, ROI_target, sep = "_")
Cats.counting.AE <- Cats.counting.AE %>%
  group_by_at(vars(-Cats_Counting)) %>%  # group by everything other than the value column.
  mutate(row_id = 1:n()) %>% ungroup() %>%  # build group index
  spread(key = Seed_target, value = Cats_Counting) %>%    # spread
  select(-row_id)  # drop the index

save(Cats.counting.AE, file = "RData/Cats.counting.AE_01_2021.RData")

# create data frame to store results
results.Cats.counting.AE <- data.frame()

# loop through the columns
for(var in names(Cats.counting.AE)[c(3:931,length(Cats.counting.AE))]){
        # dynamically generate formula
        fmla <- as.formula(paste0(var, "~ 1"))

        # fit glm model
        fit <- glm(fmla, data = Cats.counting.AE)

        ## capture summary stats
        intercept <- coef(summary(fit))[1]
        std.error <- coef(summary(fit))[2]
        t.value <- coef(summary(fit))[3]
        p.value <- coef(summary(fit))[4]
        AIC <- AIC(fit)
        Deviance <- deviance(fit)

        # get coefficents of fit
        cfit <- coef(summary(fit))

        # create temporary data frame
        df <- data.frame(var = var, intercept = cfit[1],
                         std.error = cfit[2], t.value = cfit[3], p.value = cfit[4],
                         AIC = AIC(fit), Deviance = deviance(fit), stringsAsFactors = F)

        # bind rows of temporary data frame to the results data frame
        results.Cats.counting.AE <- rbind(results.Cats.counting.AE, df)
}
```


## Heatmap thresholded with Meff correction -Semantic fluency \> Counting

``` r
Cat_count_full_th_AE <- ggplot(results.Cats.counting.AE, aes(Seed, Target)) +
  geom_raster(aes(fill = t.value)) +
  geom_tile(aes(color = p.value < alpha_meff_AE), fill = '#00000000', size = 1) + 
  scale_fill_gradientn(colours = palet, values=rescale(c(-10,10)), limits = c(-10,10), breaks = c(-10, -5, 0, 5, 10), oob = scales::squish, na.value = "white") +
  scale_color_manual(guide = "none", values = c(NA, "black")) +
  apatheme +
  theme(
    axis.text.x = element_text(angle = 90, vjust = 0.2, hjust = 0.95),
    axis.line = element_blank(),
    axis.ticks = element_blank())
Cat_count_full_th_AE
```

![](PPI_connectivity_within_and_between_networks_files/figure-markdown_github/This%20is%20thresholded%20plot,%20either%20Bonferroni-%20or%20Meff-corrected%20OA-1.png)


# Group comparison GLMs on networks with IV Age

``` r
MDN_seed = c('Insula', 'Insula1', 'Insula2', 'Insula3', 'Insula4', 'dACC', 'RInsula', 'RInsula1', 'SPL', 'SPL1', 'SPL2', 'RMFG', 'RMFG1', 'RMFG2');
DMN_seed = c('RTempPole', 'RTempPole1', 'RPrec', 'RPrec1', 'RPrec2', 'MTG', 'MTG1', 'ACing', 'ACing1', 'LAG', 'LAG1', 'LAG2', 'LAG3', 'LAG4', 'RAG', 'RAG1', 'RAG2')

all_subjects$Network <- ifelse ((all_subjects$ROI_seed %in% MDN_seed) & (all_subjects$ROI_target %in% MDN_seed), "MDN",
                                ifelse((all_subjects$ROI_seed %in% DMN_seed) & (all_subjects$ROI_target %in% DMN_seed), "DMN", "MDN_DMN"))

network_df <- all_subjects %>% group_by(Age, Subject, Network) %>% summarise(mean(Cats_Counting))
network_df <- network_df %>% rename("Cats_Counting" = "mean(Cats_Counting)")

network_df <- spread(network_df, key = Network, value = Cats_Counting)
network_df <- mutate(network_df, AgeSum = Age)
contrasts(network_df$AgeSum) <- contr.sum(2)/2
network_df <- network_df[,c(6,1,2,3,4,5)]

save(network_df, file = "RData/network_df_01_2021.RData")

# Meff value calculation
meff_df <- (network_df[4:6])
meff_df[is.na(meff_df)] <- 0
meff(meff_df)
alpha_meff_network <- 0.05/meff(meff_df)
alpha_bonferroni_network <- 0.05/((3*2)/2)
save(alpha_bonferroni_network, file = "RData/alpha_bonferroni_network.RData")
save(alpha_meff_network, file = "RData/alpha_meff_network.RData")

# create data frame to store results
 results.network <- data.frame()

# loop through the columns
for(var in names(network_df)[c(4:5, length(network_df))]){
         # dynamically generate formula
         fmla <- as.formula(paste0(var, "~ Age"))

         # fit glm model
         fit <- glm(fmla, data = network_df)

         tab_model(fit) # optional idea for different output format

         ## capture summary stats
         intercept <- coef(summary(fit))[1]
         slope <- coef(summary(fit))[2]
         std.error.intercept <- coef(summary(fit))[3]
         std.error.slope <- coef(summary(fit))[4]
         t.value.intercept <- coef(summary(fit))[5]
         t.value.slope <- coef(summary(fit))[6]
         p.value.intercept <- coef(summary(fit))[7]
         p.value.slope <- coef(summary(fit))[8]
         AIC <- AIC(fit)
         Deviance <- deviance(fit)

         # get coefficents of fit
         cfit <- coef(summary(fit))

         # create temporary data frame
         df <- data.frame(var = var, intercept = cfit[1], slope = cfit[2],
                          std.error.intercept = cfit[3], std.error.slope = cfit[4], t.value.intercept = cfit[5], t.value.slope = cfit[6], p.value.intercept = cfit[7], p.value.slope = cfit[8], AIC = AIC(fit), Deviance = deviance(fit), stringsAsFactors = F)

         # bind rows of temporary data frame to the results data frame
         results.network <- rbind(results.network, df)
 }

results.network <- separate(results.network, var, sep = "_", fill = "left", into = c("Network", "Intercept"))
results.network[1, 1] <- "DMN"
results.network[2, 1] <- "MDN"
results.network$Network <- as.factor(results.network$Network)
results.network$Intercept <- as.factor(results.network$Intercept)
results.network$Network <- factor(results.network$Network, levels = c("DMN", "MDN"))
results.network$Intercept <- factor(results.network$Intercept, levels = c("DMN", "MDN"))

save(results.network, file = "RData/results.network_01_2021.RData")
```

# Heatmaps for group comparisons

``` r
# This is the heatmap for between and within networks (Intercepts)
Networks_between_within <- ggplot(results.network, aes(Intercept, Network)) +
  geom_tile(aes(fill = t.value.intercept, color = p.value.intercept < alpha_meff_network), size = 1) +
  scale_fill_gradientn(colours = palet.positive, values=rescale(c(0,6)), limits = c(0, 6), breaks = seq(0, 6, by = 3), oob = scales::squish, na.value = "white") +
  scale_color_manual(guide = "none", values = c(NA, "black")) +
  apatheme +
  theme(
    #axis.text.x = element_text(angle = 90, vjust = 0.2, hjust = 0.95),
    axis.line = element_blank(),
    axis.ticks = element_blank())
Networks_between_within
```

![](PPI_connectivity_within_and_between_networks_files/figure-markdown_github/unnamed-chunk-4-1.png)

``` r

# This is the heatmap for IV Age (Slope)
Networks_age <- ggplot(results.network, aes(Intercept, Network)) +
  geom_tile(aes(fill = t.value.slope, color = p.value.slope < alpha_meff_network), size = 1) +
  scale_fill_gradientn(colours = palet.positive, values=rescale(c(0,6)), limits = c(0, 6), breaks = seq(0, 6, by = 3), oob = scales::squish, na.value = "white") +
  scale_color_manual(guide = "none", values = c(NA, "black")) +
  apatheme +
  theme(
    #axis.text.x = element_text(angle = 90, vjust = 0.2, hjust = 0.95),
    axis.line = element_blank(),
    axis.ticks = element_blank())
Networks_age
```

![](PPI_connectivity_within_and_between_networks_files/figure-markdown_github/unnamed-chunk-4-2.png)
