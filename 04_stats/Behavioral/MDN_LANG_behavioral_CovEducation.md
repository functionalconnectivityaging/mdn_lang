# Load packages

``` r
library(here)
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(berryFunctions)
library(data.table)
library(ggridges)
library(extrafont)
library(scales)
library(lme4)
library(emmeans)
library(sjPlot)
library(cowplot)
library(fitdistrplus) 
```

# Load data

``` r
load('RData/all_subjects_edu.RData') # This is the df with NP data
load('RData/df_cat_count.RData') # This is the df with percentages for individuals for the plots
load('RData/df_onsets.RData') # This if the df for onset calculations and plots since NAs are excluded
load("RData/RT.RData") # df with mean of RTs for each participant, condition, difficulty level
load('RData/df2.RData') # This is a df with percentages for conditions for correlation with NPs
load('RData/df_perc.RData') # This is a df with percentages per age group and SEMs for conditions for bar plots
load('RData/df3.RData') # This is a df with percentages for difficulty levels within categories
load('RData/df_perc_diff.RData') # This is a df with percentages per age group and SEMs for difficulty for bar plots
```

# Linear mixed-effects models for accuracy and response time data

## GLMM (binomial mixed regression) for DV “Correct”

``` r
# Prepare df for model
all_subjects <- all_subjects_edu
all_subjects[is.na(all_subjects)] <- 0
all_subjects$Education_z <- scale(all_subjects$Education)
all_subjects$Difficulty <- factor(all_subjects$Difficulty, levels = c("Easy", "Difficult", "Counting_forw", "Counting_backw"))
all_subjects$Age <- as.factor(all_subjects$Age)
all_subjects$Subj <- as.factor(all_subjects$Subj)
all_subjects$Difficulty[all_subjects$Category == "Aufwaerts zaehlen von 1"] <- "Easy"
all_subjects$Difficulty[all_subjects$Category == "Abwaerts zaehlen von 9"] <- "Difficult"
all_subjects$Difficulty <- as.factor(all_subjects$Difficulty)
all_subjects <- droplevels(all_subjects)

# sum coding for categorical predictors
all_subjects <- mutate(all_subjects, AgeSum = Age)
contrasts(all_subjects$AgeSum) <- contr.sum(2)/2 
all_subjects <- mutate(all_subjects, ConditionSum = Condition)
contrasts(all_subjects$ConditionSum) <- contr.sum(2)/2
all_subjects <- mutate(all_subjects, DifficultySum = Difficulty)
contrasts(all_subjects$DifficultySum) <- contr.sum(2)/2

# model
m_acc <- glmer(Correct ~ AgeSum * ConditionSum * DifficultySum + Education_z + (1|Subj) + (1|Category), family = binomial(link = "logit"), data = all_subjects, glmerControl(optimizer = "bobyqa")) # GLMM for age by condition by difficulty
summary(m_acc)
```

    ## Generalized linear mixed model fit by maximum likelihood (Laplace
    ##   Approximation) [glmerMod]
    ##  Family: binomial  ( logit )
    ## Formula: Correct ~ AgeSum * ConditionSum * DifficultySum + Education_z +  
    ##     (1 | Subj) + (1 | Category)
    ##    Data: all_subjects
    ## Control: glmerControl(optimizer = "bobyqa")
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   5149.7   5236.5  -2563.9   5127.7    19699 
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -24.9319   0.0473   0.0726   0.1914   0.6273 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  Subj     (Intercept) 0.1864   0.4317  
    ##  Category (Intercept) 0.2232   0.4725  
    ## Number of obs: 19710, groups:  Subj, 31; Category, 22
    ## 
    ## Fixed effects:
    ##                                      Estimate Std. Error z value
    ## (Intercept)                           6.27617    0.54489  11.518
    ## AgeSum1                              -3.94028    1.03262  -3.816
    ## ConditionSum1                        -6.47275    1.07616  -6.015
    ## DifficultySum1                        4.63215    1.07208   4.321
    ## Education_z                          -0.11150    0.06351  -1.756
    ## AgeSum1:ConditionSum1                 7.26020    2.05503   3.533
    ## AgeSum1:DifficultySum1               -7.99332    2.02065  -3.956
    ## ConditionSum1:DifficultySum1         -5.02743    2.13818  -2.351
    ## AgeSum1:ConditionSum1:DifficultySum1 14.94549    4.02857   3.710
    ##                                                  Pr(>|z|)    
    ## (Intercept)                          < 0.0000000000000002 ***
    ## AgeSum1                                          0.000136 ***
    ## ConditionSum1                                0.0000000018 ***
    ## DifficultySum1                               0.0000155527 ***
    ## Education_z                                      0.079154 .  
    ## AgeSum1:ConditionSum1                            0.000411 ***
    ## AgeSum1:DifficultySum1                       0.0000762766 ***
    ## ConditionSum1:DifficultySum1                     0.018710 *  
    ## AgeSum1:ConditionSum1:DifficultySum1             0.000207 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) AgeSm1 CndtS1 DffcS1 Edctn_ AgS1:CS1 AS1:DS CS1:DS
    ## AgeSum1     -0.902                                                   
    ## ConditinSm1 -0.963  0.912                                            
    ## DiffcltySm1  0.855 -0.918 -0.859                                     
    ## Education_z -0.001  0.069  0.001  0.000                              
    ## AgSm1:CndS1  0.904 -0.992 -0.915  0.920  0.000                       
    ## AgSm1:DffS1 -0.911  0.965  0.920 -0.914  0.001 -0.963                
    ## CndtnS1:DS1 -0.849  0.917  0.864 -0.974  0.000 -0.920    0.913       
    ## AS1:CS1:DS1  0.910 -0.961 -0.921  0.913 -0.001  0.967   -0.994 -0.914

``` r
# LRTs:
drop1(m_acc, test = "Chisq") # drop three-way interaction
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ AgeSum * ConditionSum * DifficultySum + Education_z + 
    ##     (1 | Subj) + (1 | Category)
    ##                                   npar    AIC    LRT  Pr(Chi)   
    ## <none>                                 5149.7                   
    ## Education_z                          1 5150.8 3.0314 0.081669 . 
    ## AgeSum:ConditionSum:DifficultySum    1 5157.0 9.2840 0.002312 **
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
m_acc_twoWay <- glmer(Correct ~ AgeSum + ConditionSum + DifficultySum + Education_z + AgeSum:ConditionSum + AgeSum:DifficultySum + ConditionSum:DifficultySum + (1|Subj) + (1|Category), family = binomial(link = "logit"), data = all_subjects, glmerControl(optimizer = "bobyqa")) # model with two-way interactions 
drop1(m_acc_twoWay, test = "Chisq") # drop two-way interactions
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ AgeSum + ConditionSum + DifficultySum + Education_z + 
    ##     AgeSum:ConditionSum + AgeSum:DifficultySum + ConditionSum:DifficultySum + 
    ##     (1 | Subj) + (1 | Category)
    ##                            npar    AIC    LRT  Pr(Chi)   
    ## <none>                          5157.0                   
    ## Education_z                   1 5158.1 3.0261 0.081936 . 
    ## AgeSum:ConditionSum           1 5157.0 1.9514 0.162434   
    ## AgeSum:DifficultySum          1 5164.8 9.7606 0.001783 **
    ## ConditionSum:DifficultySum    1 5158.9 3.9027 0.048208 * 
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
m_acc_mainEffects <- glmer(Correct ~ AgeSum + ConditionSum + DifficultySum + Education_z + (1|Subj) + (1|Category), family = binomial(link = "logit"), data = all_subjects, glmerControl(optimizer = "bobyqa")) # model with main effects
drop1(m_acc_mainEffects, test = "Chisq") # drop main effects
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ AgeSum + ConditionSum + DifficultySum + Education_z + 
    ##     (1 | Subj) + (1 | Category)
    ##               npar    AIC     LRT      Pr(Chi)    
    ## <none>             5168.4                         
    ## AgeSum           1 5168.6  2.2248      0.13581    
    ## ConditionSum     1 5188.0 21.5914 0.0000033736 ***
    ## DifficultySum    1 5193.9 27.4745 0.0000001592 ***
    ## Education_z      1 5169.4  3.0173      0.08238 .  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
# post-hoc tests
emmp <- emmeans(m_acc, pairwise ~ AgeSum + DifficultySum | ConditionSum, adjust = "bonferroni", type = "response") # post-hoc comparisons for age effects for difficulty by condition | three-way interaction
emmp
```

    ## $emmeans
    ## ConditionSum = Categories:
    ##  AgeSum DifficultySum      prob          SE  df asymp.LCL asymp.UCL
    ##  OA     Easy          0.9784116 0.004760320 Inf 0.9668198 0.9860122
    ##  YA     Easy          0.9876813 0.002899955 Inf 0.9804893 0.9922432
    ##  OA     Difficult     0.8760594 0.020040525 Inf 0.8311613 0.9103064
    ##  YA     Difficult     0.8813762 0.019057326 Inf 0.8386576 0.9139448
    ## 
    ## ConditionSum = Counting:
    ##  AgeSum DifficultySum      prob          SE  df asymp.LCL asymp.UCL
    ##  OA     Easy          0.9956524 0.002443722 Inf 0.9869689 0.9985579
    ##  YA     Easy          1.0000000 0.000000004 Inf 0.9999973 1.0000000
    ##  OA     Difficult     0.9975785 0.001509830 Inf 0.9918043 0.9992875
    ##  YA     Difficult     0.9971520 0.001650080 Inf 0.9911590 0.9990863
    ## 
    ## Confidence level used: 0.95 
    ## Intervals are back-transformed from the logit scale 
    ## 
    ## $contrasts
    ## ConditionSum = Categories:
    ##  contrast                    odds.ratio       SE  df z.ratio p.value
    ##  OA Easy / YA Easy                  0.6        0 Inf -2.713  0.0400 
    ##  OA Easy / OA Difficult             6.4        2 Inf  7.076  <.0001 
    ##  OA Easy / YA Difficult             6.1        2 Inf  6.666  <.0001 
    ##  YA Easy / OA Difficult            11.3        3 Inf  8.563  <.0001 
    ##  YA Easy / YA Difficult            10.8        3 Inf  8.714  <.0001 
    ##  OA Difficult / YA Difficult        1.0        0 Inf -0.447  1.0000 
    ## 
    ## ConditionSum = Counting:
    ##  contrast                    odds.ratio       SE  df z.ratio p.value
    ##  OA Easy / YA Easy                  0.0        0 Inf -3.791  0.0009 
    ##  OA Easy / OA Difficult             0.6        0 Inf -0.706  1.0000 
    ##  OA Easy / YA Difficult             0.7        1 Inf -0.528  1.0000 
    ##  YA Easy / OA Difficult       2461403.5 10105894 Inf  3.584  0.0020 
    ##  YA Easy / YA Difficult       2896157.6 11755020 Inf  3.666  0.0015 
    ##  OA Difficult / YA Difficult        1.2        1 Inf  0.311  1.0000 
    ## 
    ## P value adjustment: bonferroni method for 6 tests 
    ## Tests are performed on the log odds ratio scale

``` r
# write post-hoc results to table
emmp_df <- emmp$contrasts %>% summary(infer = T) %>% as.data.frame()
fwrite(format(emmp_df, digits = 2, nsmall = 2), sep = '\t', row.names = F, file = paste0("mc_m_accuracy", today, ".csv"))
```

## LMM for reaction times

``` r
all_subjects$Onset <- as.numeric(all_subjects$Onset)
df_onsets <- df_onsets[!is.na(df_onsets$Onset),] # exclude NAs
df_onsets <- all_subjects[all_subjects$Onset > 0, ] # exclude rows with 0 because these should be NAs
df_onsets <- df_onsets %>% filter(Onset != 0) # exclude rows with 0 because these should be NAs
df_onsets <- droplevels(df_onsets)
save(df_onsets, file = 'RData/df_onsets.RData')

# Decide whether Gamma distribution or log-transformation is better for RT data
# fit.norm <- fitdist(df_onsets$Onset, "lnorm")
# fit.gamma <- fitdist(df_onsets$Onset, "gamma")
# fit.norm$aic # AIC: 264804.8 --> log transformation is better
# fit.gamma$aic # AIC: 267212.6

# Prepare df for model
df_onsets$Age <- as.factor(df_onsets$Age)
df_onsets$Subj <- as.factor(df_onsets$Subj)
df_onsets$Difficulty[df_onsets$Category == "Aufwaerts zaehlen von 1"] <- "Easy"
df_onsets$Difficulty[df_onsets$Category == "Abwaerts zaehlen von 9"] <- "Difficult"
df_onsets <- droplevels(df_onsets)
df_onsets$Education_z <- scale(df_onsets$Education)

# sum coding for categorical predictors
df_onsets <- mutate(df_onsets, AgeSum = Age)
contrasts(df_onsets$AgeSum) <- contr.sum(2)/2
df_onsets <- mutate(df_onsets, ConditionSum = Condition)
contrasts(df_onsets$ConditionSum) <- contr.sum(2)/2
df_onsets <- mutate(df_onsets, DifficultySum = Difficulty)
contrasts(df_onsets$DifficultySum) <- contr.sum(2)/2

# model
m_RT <- lmer(log(Onset) ~ AgeSum * ConditionSum * DifficultySum + Education_z + (1|Subj) + (1|Category), data = df_onsets, lmerControl(optimizer = "bobyqa"), REML = F)
summary(m_RT)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: log(Onset) ~ AgeSum * ConditionSum * DifficultySum + Education_z +  
    ##     (1 | Subj) + (1 | Category)
    ##    Data: df_onsets
    ## Control: lmerControl(optimizer = "bobyqa")
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##  10373.8  10468.3  -5174.9  10349.8    19479 
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -10.5065  -0.6472  -0.0998   0.5273   5.5589 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  Subj     (Intercept) 0.007932 0.08906 
    ##  Category (Intercept) 0.001065 0.03264 
    ##  Residual             0.098741 0.31423 
    ## Number of obs: 19491, groups:  Subj, 31; Category, 22
    ## 
    ## Fixed effects:
    ##                                       Estimate Std. Error t value
    ## (Intercept)                           6.445437   0.020200 319.080
    ## AgeSum1                               0.011718   0.006322   1.854
    ## ConditionSum1                         0.173363   0.024623   7.041
    ## DifficultySum1                       -0.058680   0.024623  -2.383
    ## Education_z                          -0.007350   0.003876  -1.896
    ## AgeSum1:ConditionSum1                 0.075173   0.009022   8.332
    ## AgeSum1:DifficultySum1                0.003539   0.009021   0.392
    ## ConditionSum1:DifficultySum1         -0.098525   0.049246  -2.001
    ## AgeSum1:ConditionSum1:DifficultySum1  0.030446   0.018042   1.688
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) AgeSm1 CndtS1 DffcS1 Edctn_ AgS1:CS1 AS1:DS CS1:DS
    ## AgeSum1      0.015                                                   
    ## ConditinSm1 -0.482  0.001                                            
    ## DiffcltySm1  0.000  0.000  0.000                                     
    ## Education_z  0.009  0.672  0.000  0.000                              
    ## AgSm1:CndS1  0.001  0.006  0.011  0.000  0.000                       
    ## AgSm1:DffS1 -0.001 -0.003  0.000  0.011  0.003 -0.002                
    ## CndtnS1:DS1  0.000  0.000  0.000 -0.790  0.000 -0.001    0.001       
    ## AS1:CS1:DS1  0.000 -0.002 -0.001  0.001  0.001 -0.006    0.008  0.011

``` r
# LRTs
drop1(m_RT, test = "Chisq") # drop three-way interaction
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ AgeSum * ConditionSum * DifficultySum + Education_z + 
    ##     (1 | Subj) + (1 | Category)
    ##                                   npar   AIC    LRT Pr(Chi)  
    ## <none>                                 10374                 
    ## Education_z                          1 10375 3.5954 0.05794 .
    ## AgeSum:ConditionSum:DifficultySum    1 10375 2.8474 0.09152 .
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
m_RT_twoWay <- lmer(log(Onset) ~ AgeSum + ConditionSum + DifficultySum + Education_z + AgeSum:ConditionSum + AgeSum:DifficultySum + ConditionSum:DifficultySum + (1|Subj) + (1|Category), data = df_onsets, REML = F, lmerControl(optimizer = "bobyqa")) # model with two-way interactions 
drop1(m_RT_twoWay, test = "Chisq") # drop two-way interactions
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ AgeSum + ConditionSum + DifficultySum + Education_z + 
    ##     AgeSum:ConditionSum + AgeSum:DifficultySum + ConditionSum:DifficultySum + 
    ##     (1 | Subj) + (1 | Category)
    ##                            npar   AIC    LRT              Pr(Chi)    
    ## <none>                          10375                                
    ## Education_z                   1 10376  3.600              0.05780 .  
    ## AgeSum:ConditionSum           1 10442 69.464 < 0.0000000000000002 ***
    ## AgeSum:DifficultySum          1 10373  0.143              0.70512    
    ## ConditionSum:DifficultySum    1 10376  3.632              0.05667 .  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
m_RT_mainEffects <- lmer(log(Onset) ~ AgeSum + ConditionSum + DifficultySum + Education_z + (1|Subj) + (1|Category), data = df_onsets, REML = F, lmerControl(optimizer = "bobyqa")) # model with main effects
drop1(m_RT_mainEffects, test = "Chisq") # drop main effects
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ AgeSum + ConditionSum + DifficultySum + Education_z + 
    ##     (1 | Subj) + (1 | Category)
    ##               npar   AIC     LRT     Pr(Chi)    
    ## <none>             10442                        
    ## AgeSum           1 10443  3.2447     0.07166 .  
    ## ConditionSum     1 10461 21.3710 0.000003784 ***
    ## DifficultySum    1 10461 20.9854 0.000004628 ***
    ## Education_z      1 10444  3.6072     0.05753 .  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
# post-hoc tests
emmp <- emmeans(m_RT, pairwise ~ AgeSum * DifficultySum | ConditionSum, adjust = "bonferroni", type = "response")
emmp
```

    ## $emmeans
    ## ConditionSum = Categories:
    ##  AgeSum DifficultySum response   SE  df asymp.LCL asymp.UCL
    ##  OA     Easy               670 13.6 Inf       644       697
    ##  YA     Easy               632 12.7 Inf       608       657
    ##  OA     Difficult          740 15.0 Inf       711       770
    ##  YA     Difficult          711 14.3 Inf       683       739
    ## 
    ## ConditionSum = Counting:
    ##  AgeSum DifficultySum response   SE  df asymp.LCL asymp.UCL
    ##  OA     Easy               566 20.9 Inf       526       608
    ##  YA     Easy               584 21.6 Inf       543       628
    ##  OA     Difficult          574 21.3 Inf       534       618
    ##  YA     Difficult          586 21.6 Inf       545       630
    ## 
    ## Degrees-of-freedom method: asymptotic 
    ## Confidence level used: 0.95 
    ## Intervals are back-transformed from the log scale 
    ## 
    ## $contrasts
    ## ConditionSum = Categories:
    ##  contrast                    ratio      SE  df z.ratio p.value
    ##  OA Easy / YA Easy           1.060 0.01066 Inf  5.836  <.0001 
    ##  OA Easy / OA Difficult      0.906 0.01571 Inf -5.687  <.0001 
    ##  OA Easy / YA Difficult      0.943 0.01672 Inf -3.308  0.0056 
    ##  YA Easy / OA Difficult      0.854 0.01517 Inf -8.855  <.0001 
    ##  YA Easy / YA Difficult      0.889 0.01514 Inf -6.891  <.0001 
    ##  OA Difficult / YA Difficult 1.041 0.01052 Inf  3.948  0.0005 
    ## 
    ## ConditionSum = Counting:
    ##  contrast                    ratio      SE  df z.ratio p.value
    ##  OA Easy / YA Easy           0.969 0.00969 Inf -3.169  0.0092 
    ##  OA Easy / OA Difficult      0.985 0.04636 Inf -0.324  1.0000 
    ##  OA Easy / YA Difficult      0.965 0.04560 Inf -0.747  1.0000 
    ##  YA Easy / OA Difficult      1.017 0.04802 Inf  0.348  1.0000 
    ##  YA Easy / YA Difficult      0.996 0.04681 Inf -0.076  1.0000 
    ##  OA Difficult / YA Difficult 0.980 0.00983 Inf -1.997  0.2747 
    ## 
    ## Degrees-of-freedom method: asymptotic 
    ## P value adjustment: bonferroni method for 6 tests 
    ## Tests are performed on the log scale

``` r
# save post-hoc tests to table
emmp_df <- emmp$contrasts %>% summary(infer = T) %>% as.data.frame()
fwrite(format(emmp_df, digits = 2, nsmall = 2), sep = '\t', row.names = F, file = paste0("mc_m_RT_", today, ".csv"))
confint(emmp)
```

    ## $emmeans
    ## ConditionSum = Categories:
    ##  AgeSum DifficultySum response   SE  df asymp.LCL asymp.UCL
    ##  OA     Easy               670 13.6 Inf       644       697
    ##  YA     Easy               632 12.7 Inf       608       657
    ##  OA     Difficult          740 15.0 Inf       711       770
    ##  YA     Difficult          711 14.3 Inf       683       739
    ## 
    ## ConditionSum = Counting:
    ##  AgeSum DifficultySum response   SE  df asymp.LCL asymp.UCL
    ##  OA     Easy               566 20.9 Inf       526       608
    ##  YA     Easy               584 21.6 Inf       543       628
    ##  OA     Difficult          574 21.3 Inf       534       618
    ##  YA     Difficult          586 21.6 Inf       545       630
    ## 
    ## Degrees-of-freedom method: asymptotic 
    ## Confidence level used: 0.95 
    ## Intervals are back-transformed from the log scale 
    ## 
    ## $contrasts
    ## ConditionSum = Categories:
    ##  contrast                    ratio      SE  df asymp.LCL asymp.UCL
    ##  OA Easy / YA Easy           1.060 0.01066 Inf     1.033     1.089
    ##  OA Easy / OA Difficult      0.906 0.01571 Inf     0.866     0.949
    ##  OA Easy / YA Difficult      0.943 0.01672 Inf     0.900     0.988
    ##  YA Easy / OA Difficult      0.854 0.01517 Inf     0.815     0.895
    ##  YA Easy / YA Difficult      0.889 0.01514 Inf     0.850     0.930
    ##  OA Difficult / YA Difficult 1.041 0.01052 Inf     1.013     1.069
    ## 
    ## ConditionSum = Counting:
    ##  contrast                    ratio      SE  df asymp.LCL asymp.UCL
    ##  OA Easy / YA Easy           0.969 0.00969 Inf     0.944     0.995
    ##  OA Easy / OA Difficult      0.985 0.04636 Inf     0.870     1.115
    ##  OA Easy / YA Difficult      0.965 0.04560 Inf     0.852     1.093
    ##  YA Easy / OA Difficult      1.017 0.04802 Inf     0.897     1.152
    ##  YA Easy / YA Difficult      0.996 0.04681 Inf     0.880     1.128
    ##  OA Difficult / YA Difficult 0.980 0.00983 Inf     0.955     1.006
    ## 
    ## Degrees-of-freedom method: asymptotic 
    ## Confidence level used: 0.95 
    ## Conf-level adjustment: bonferroni method for 6 estimates 
    ## Intervals are back-transformed from the log scale

# Extract models table

``` r
tab_model(m_acc, m_RT,
  dv.labels = c("Accuracy", "Response time"),
  pred.labels = c("Intercept", "Age", "Condition", "Difficulty", "Education", "Age * Condition", "Age * Difficulty", "Condition * Difficulty", "Age * Condition * Difficulty"),
  string.pred = "Coefficient",
  string.ci = "Conf. Int (95%)", 
  transform = NULL,
  show.p = F,
  show.stat = T,
  file = paste0("behav_models_", today, ".doc"))
```

<table style="border-collapse:collapse; border:none;">
<tr>
<th style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm;  text-align:left; ">
 
</th>
<th colspan="3" style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm; ">
Accuracy
</th>
<th colspan="3" style="border-top: double; text-align:center; font-style:normal; font-weight:bold; padding:0.2cm; ">
Response time
</th>
</tr>
<tr>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  text-align:left; ">
Coefficient
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Log-Odds
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Conf. Int (95%)
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Statistic
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Estimates
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  ">
Conf. Int (95%)
</td>
<td style=" text-align:center; border-bottom:1px solid; font-style:italic; font-weight:normal;  col7">
Statistic
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Intercept
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
6.28
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
5.21 – 7.34
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
11.52
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
6.45
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
6.41 – 6.49
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
319.08
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Age
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-3.94
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-5.96 – -1.92
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-3.82
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.01
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.00 – 0.02
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
1.85
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Condition
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-6.47
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-8.58 – -4.36
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-6.01
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.17
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.13 – 0.22
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
7.04
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Difficulty
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
4.63
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
2.53 – 6.73
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
4.32
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.06
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.11 – -0.01
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
-2.38
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Education
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.11
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.24 – 0.01
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-1.76
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.01
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.01 – 0.00
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
-1.90
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Age \* Condition
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
7.26
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
3.23 – 11.29
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
3.53
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.08
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.06 – 0.09
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
8.33
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Age \* Difficulty
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-7.99
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-11.95 – -4.03
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-3.96
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.00
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.01 – 0.02
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
0.39
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Condition \* Difficulty
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-5.03
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-9.22 – -0.84
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-2.35
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.10
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.20 – -0.00
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
-2.00
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; ">
Age \* Condition \* Difficulty
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
14.95
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
7.05 – 22.84
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
3.71
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
0.03
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  ">
-0.00 – 0.07
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:center;  col7">
1.69
</td>
</tr>
<tr>
<td colspan="7" style="font-weight:bold; text-align:left; padding-top:.8em;">
Random Effects
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
σ<sup>2</sup>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
3.29
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.10
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
τ<sub>00</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.19 <sub>Subj</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.01 <sub>Subj</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.22 <sub>Category</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.00 <sub>Category</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
ICC
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.11
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.08
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
N
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
31 <sub>Subj</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
31 <sub>Subj</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
22 <sub>Category</sub>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
22 <sub>Category</sub>
</td>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm; border-top:1px solid;">
Observations
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left; border-top:1px solid;" colspan="3">
19710
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left; border-top:1px solid;" colspan="3">
19491
</td>
</tr>
<tr>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; text-align:left; padding-top:0.1cm; padding-bottom:0.1cm;">
Marginal R<sup>2</sup> / Conditional R<sup>2</sup>
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.900 / 0.911
</td>
<td style=" padding:0.2cm; text-align:left; vertical-align:top; padding-top:0.1cm; padding-bottom:0.1cm; text-align:left;" colspan="3">
0.079 / 0.156
</td>
</tr>
</table>

# Plots for Accuracy - bar plots with individual data points on top

``` r
# Plot for accuracy in tasks in both age groups
accuracy_tasks <- 
  ggplot(data = df_perc, aes(x = Condition, y = Perc)) +
  geom_bar(aes(fill = Condition), stat = "identity") +
  geom_point(data = df2, shape = 21, fill = "gray75", color = "black", position = position_jitter(width = 0.2), 
             size = 2) +
  geom_errorbar(aes(ymin = lowerE, ymax = upperE, width = 0.2)) +
  scale_fill_manual(values = c('#195E8E','#FFB838')) +
  labs(y = "% correct") +
  facet_wrap(~ Age, ncol = 2) +
  apatheme +
  theme(legend.position=("none"), 
        legend.title=element_blank(), 
        axis.title.x = element_blank(),
        strip.text.x = element_blank(), # This removes labels from facets
        axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1))
accuracy_tasks
```

![](MDN_LANG_behavioral_CovEducation_files/figure-markdown_github/Plot%20for%20Categories%20vs%20Counting%20%3C-%20in%20percentage-1.png)

``` r
# Plot for accuracy in difficulty levels of semantic fluency in both age groups
accuracy_difficulty <- 
  ggplot(data = df_perc_diff, aes(x = Difficulty, y = Perc)) +
  geom_bar(aes(fill = Difficulty), stat = "identity") +
  geom_point(data = df3, shape = 21, fill = "gray75", color = "black", position = position_jitter(width = 0.2), 
             size = 2) +
  geom_errorbar(aes(ymin = lowerE, ymax = upperE, width = 0.2)) +
  scale_fill_manual(values = c('#85a4be', '#b9cad9')) +
  labs(y = "% correct") +
  facet_wrap(~ Age, ncol = 2) +
  apatheme +
  theme(legend.position=("none"), 
        legend.title=element_blank(), 
        axis.title.x = element_blank(),
        strip.text.x = element_blank(), # This removes labels from facets
        axis.text.x = element_text(angle = 45, vjust = 1, hjust=1))
accuracy_difficulty
```

![](MDN_LANG_behavioral_CovEducation_files/figure-markdown_github/Plot%20for%20Categories%20vs%20Counting%20%3C-%20in%20percentage-2.png)

# Plots for Response time - violin plots with boxplots

``` r
# Plot for RT in both tasks in both age groups
rt_tasks <- ggplot() +
  geom_violin(data = RT, aes(x = Condition, y = Mean_onset, fill = Condition), width=1) +
  geom_boxplot(data = RT, aes(x = Condition, y = Mean_onset, fill = Condition), width=0.3, color="grey55", alpha=0.1) +
  facet_wrap(~ Age) +
  ylab("ms") +
  scale_y_continuous(limits = c(400, 1200)) +
  scale_fill_manual(values = c('#195E8E','#FFB838')) +
  apatheme +
  theme(legend.position=("none"),
        legend.title=element_blank(), 
        strip.text.x = element_blank(), # This removes labels from facets
        axis.title.x = element_blank(),
        #axis.text.x = element_blank())
        axis.text.x = element_text(angle = 45, vjust = 1, hjust=1))
        #panel.spacing = unit(0.2, "lines"))
rt_tasks
```

![](MDN_LANG_behavioral_CovEducation_files/figure-markdown_github/Response%20time-1.png)

``` r
# Plot for RT in difficulty levels of semantic fluency in both age groups
rt_difficulty <- ggplot() +
  geom_violin(data = RT, aes(x = Difficulty, y = Mean_onset, fill = Difficulty), width=1, alpha = 0.6) +
  geom_boxplot(data = RT, aes(x = Difficulty, y = Mean_onset, fill = Difficulty), width=0.3, color="grey55", alpha=0.1) +
  facet_wrap(~ Age) +
  ylab("ms") +
  scale_y_continuous(limits = c(400, 1200)) +
  scale_fill_manual(values = c('#195E8E', '#85A4BE')) +
  apatheme +
  theme(legend.position=("none"),
        legend.title=element_blank(), 
        strip.text.x = element_blank(), # This removes labels from facets
        axis.title.x = element_blank(),
        #axis.text.x = element_blank())
        axis.text.x = element_text(angle = 45, vjust = 1, hjust=1))
        #panel.spacing = unit(0.2, "lines"))
rt_difficulty
```

![](MDN_LANG_behavioral_CovEducation_files/figure-markdown_github/Response%20time-2.png)
