- Scripts:
    - MDN_LANG_behavioral.md: Analysis of behavioral data from in-scanner task
    - MDN_LANG_NP.md: Analysis of neuropsychological test out of scanner
- NP_data  
Contains tables with neuropsychological data and demographic information of participants
- RData  
RData structures containing data frames for use in Rproj analyses
- fMRI_behavioral/Data  
Contains raw data for accuracy and response times for in-scanner paradigm (semantic fluency task and counting task)
