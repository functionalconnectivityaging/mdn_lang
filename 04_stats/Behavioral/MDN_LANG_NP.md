# Load data

``` r
load('RData/NP_all.RData')
```

# MMSE

``` r
tapply(NP_all$MMSE.2, NP_all$Group, mean, na.rm = T)
```

    ##       OA       YA 
    ## 28.35714      NaN

``` r
tapply(NP_all$MMSE.2, NP_all$Group, sd, na.rm = T)
```

    ##       OA       YA 
    ## 1.193013       NA

# Sex

``` r
NP_all$Sex <- droplevels(NP_all$Sex)
tbl = table(NP_all$Sex, NP_all$Group)
chisq.test(tbl) # no sig difference in sex occurence between age groups <- groups are matched for sex but not for years of ed
```

    ## 
    ##  Pearson's Chi-squared test with Yates' continuity correction
    ## 
    ## data:  tbl
    ## X-squared = 0, df = 1, p-value = 1

# Age

``` r
tapply(NP_all$Age, NP_all$Group, mean, na.rm = T)
```

    ##       OA       YA 
    ## 65.17857 27.56667

``` r
tapply(NP_all$Age, NP_all$Group, sd, na.rm = T)
```

    ##       OA       YA 
    ## 2.789578 4.360349

# Education

``` r
tapply(NP_all$Bildung, NP_all$Group, mean, na.rm = T) # means for years of education for both age groups
```

    ##       OA       YA 
    ## 15.23214 18.73333

``` r
tapply(NP_all$Bildung, NP_all$Group, sd, na.rm = T) 
```

    ##       OA       YA 
    ## 2.533049 2.585548

``` r
boxplot(Bildung ~ Group, data = NP_all)
```

![](MDN_LANG_NP_files/figure-markdown_github/Years%20of%20education-1.png)

``` r
t.test(NP_all$Bildung[NP_all$Group == "YA"], NP_all$Bildung[NP_all$Group == "OA"]) # YA have more years of education
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  NP_all$Bildung[NP_all$Group == "YA"] and NP_all$Bildung[NP_all$Group == "OA"]
    ## t = 5.2078, df = 55.862, p-value = 2.847e-06
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##  2.154334 4.848047
    ## sample estimates:
    ## mean of x mean of y 
    ##  18.73333  15.23214

# RWT (semantic fluency test)

``` r
tapply(NP_all$RWT_Vornamen_korrekt, NP_all$Group, mean, na.rm = T) # means for number of correct RWT Vornamen for both age groups
```

    ##       OA       YA 
    ## 24.67857 32.10000

``` r
tapply(NP_all$RWT_Hobbys_korrekt, NP_all$Group, mean, na.rm = T) # means for number of correct RWT Hobbys for both age groups
```

    ##       OA       YA 
    ## 16.03571 19.13333

``` r
boxplot(RWT_Vornamen_korrekt ~ Group, data = NP_all)
```

![](MDN_LANG_NP_files/figure-markdown_github/RWT:%20analyses-1.png)

``` r
boxplot(RWT_Hobbys_korrekt ~ Group, data = NP_all)
```

![](MDN_LANG_NP_files/figure-markdown_github/RWT:%20analyses-2.png)

``` r
t.test(NP_all$RWT_Vornamen_korrekt[NP_all$Group == "OA"], NP_all$RWT_Vornamen_korrekt[NP_all$Group == "YA"]) # YA produced significantly more Vornamen
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  NP_all$RWT_Vornamen_korrekt[NP_all$Group == "OA"] and NP_all$RWT_Vornamen_korrekt[NP_all$Group == "YA"]
    ## t = -6.0087, df = 55.848, p-value = 1.484e-07
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##  -9.895806 -4.947052
    ## sample estimates:
    ## mean of x mean of y 
    ##  24.67857  32.10000

``` r
t.test(NP_all$RWT_Hobbys_korrekt[NP_all$Group == "OA"], NP_all$RWT_Hobbys_korrekt[NP_all$Group == "YA"]) # YA produced significantly more Hobbys
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  NP_all$RWT_Hobbys_korrekt[NP_all$Group == "OA"] and NP_all$RWT_Hobbys_korrekt[NP_all$Group == "YA"]
    ## t = -2.6685, df = 50.953, p-value = 0.01019
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##  -5.428125 -0.767113
    ## sample estimates:
    ## mean of x mean of y 
    ##  16.03571  19.13333

``` r
NP_all$fluency <- rowSums(NP_all[c('RWT_Vornamen_korrekt', 'RWT_Hobbys_korrekt')], na.rm=TRUE) # summed fluency score accross categories
tapply(NP_all$fluency, NP_all$Group, mean, na.rm = T) # means for number of correct RWT Vornamen for both age groups
```

    ##       OA       YA 
    ## 40.71429 51.23333

``` r
tapply(NP_all$fluency, NP_all$Group, sd, na.rm = T) # means for number of correct RWT Hobbys for both age groups
```

    ##       OA       YA 
    ## 6.649183 8.410516

``` r
t.test(NP_all$fluency[NP_all$Group == "OA"], NP_all$fluency[NP_all$Group == "YA"]) # YA produced significantly more words
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  NP_all$fluency[NP_all$Group == "OA"] and NP_all$fluency[NP_all$Group == "YA"]
    ## t = -5.3015, df = 54.564, p-value = 2.134e-06
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##  -14.496102  -6.541993
    ## sample estimates:
    ## mean of x mean of y 
    ##  40.71429  51.23333

# TMT

``` r
tapply(NP_all$TMT_A, NP_all$Group, mean, na.rm = T) # means for TMT_A for both age groups
```

    ##       OA       YA 
    ## 25.43679 17.28967

``` r
tapply(NP_all$TMT_A, NP_all$Group, sd, na.rm = T) # sd for TMT_A for both age groups
```

    ##       OA       YA 
    ## 6.425010 5.836511

``` r
tapply(NP_all$TMT_B, NP_all$Group, mean, na.rm = T) # means for TMT_B for both age groups
```

    ##       OA       YA 
    ## 61.83857 36.10333

``` r
tapply(NP_all$TMT_B, NP_all$Group, sd, na.rm = T) # sd for TMT_B for both age groups
```

    ##       OA       YA 
    ## 29.35744 11.94471

``` r
boxplot(TMT_A ~ Group, data = NP_all)
```

![](MDN_LANG_NP_files/figure-markdown_github/TMT%20analyses-1.png)

``` r
boxplot(TMT_B ~ Group, data = NP_all)
```

![](MDN_LANG_NP_files/figure-markdown_github/TMT%20analyses-2.png)

``` r
t.test(NP_all$TMT_A[NP_all$Group == "OA"], NP_all$TMT_A[NP_all$Group == "YA"]) # YA are sig faster
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  NP_all$TMT_A[NP_all$Group == "OA"] and NP_all$TMT_A[NP_all$Group == "YA"]
    ## t = 5.0431, df = 54.504, p-value = 5.403e-06
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##   4.908943 11.385295
    ## sample estimates:
    ## mean of x mean of y 
    ##  25.43679  17.28967

``` r
t.test(NP_all$TMT_B[NP_all$Group == "OA"], NP_all$TMT_B[NP_all$Group == "YA"]) # YA are sig faster
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  NP_all$TMT_B[NP_all$Group == "OA"] and NP_all$TMT_B[NP_all$Group == "YA"]
    ## t = 4.3171, df = 35.206, p-value = 0.0001225
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##  13.63577 37.83471
    ## sample estimates:
    ## mean of x mean of y 
    ##  61.83857  36.10333

# Reading span, WST (spot-the-word test) & DSST

``` r
tapply(NP_all$Reading_span, NP_all$Group, mean, na.rm = T) # means for Reading span for both age groups
```

    ##       OA       YA 
    ## 2.928571 3.466667

``` r
tapply(NP_all$Reading_span, NP_all$Group, sd, na.rm = T) # sd for Reading span for both age groups
```

    ##        OA        YA 
    ## 0.6900656 0.9994251

``` r
tapply(NP_all$WST, NP_all$Group, mean, na.rm = T) # means for WST for both age groups
```

    ##       OA       YA 
    ## 31.46429 29.13333

``` r
tapply(NP_all$WST, NP_all$Group, sd, na.rm = T) # sd for WST for both age groups
```

    ##       OA       YA 
    ## 2.471799 3.159368

``` r
tapply(NP_all$DSST, NP_all$Group, mean, na.rm = T) # means for DSST for both age groups
```

    ##       OA       YA 
    ## 50.21429 72.10000

``` r
tapply(NP_all$DSST, NP_all$Group, sd, na.rm = T) # sd for DSST for both age groups
```

    ##       OA       YA 
    ## 10.40426 11.37556

``` r
boxplot(Reading_span ~ Group, data = NP_all)
```

![](MDN_LANG_NP_files/figure-markdown_github/Reading%20span,%20WST%20&%20DSST-1.png)

``` r
boxplot(WST ~ Group, data = NP_all)
```

![](MDN_LANG_NP_files/figure-markdown_github/Reading%20span,%20WST%20&%20DSST-2.png)

``` r
boxplot(DSST ~ Group, data = NP_all)
```

![](MDN_LANG_NP_files/figure-markdown_github/Reading%20span,%20WST%20&%20DSST-3.png)

``` r
t.test(NP_all$Reading_span[NP_all$Group == "OA"], NP_all$Reading_span[NP_all$Group == "YA"]) # YA have sig bigger RS
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  NP_all$Reading_span[NP_all$Group == "OA"] and NP_all$Reading_span[NP_all$Group == "YA"]
    ## t = -2.3992, df = 51.703, p-value = 0.02007
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.98820884 -0.08798163
    ## sample estimates:
    ## mean of x mean of y 
    ##  2.928571  3.466667

``` r
t.test(NP_all$WST[NP_all$Group == "OA"], NP_all$WST[NP_all$Group == "YA"]) # OA have sig bigger verbal knowledge
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  NP_all$WST[NP_all$Group == "OA"] and NP_all$WST[NP_all$Group == "YA"]
    ## t = 3.1404, df = 54.386, p-value = 0.002727
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##  0.8430836 3.8188211
    ## sample estimates:
    ## mean of x mean of y 
    ##  31.46429  29.13333

``` r
t.test(NP_all$DSST[NP_all$Group == "OA"], NP_all$DSST[NP_all$Group == "YA"]) # YA are sig better in DSST
```

    ## 
    ##  Welch Two Sample t-test
    ## 
    ## data:  NP_all$DSST[NP_all$Group == "OA"] and NP_all$DSST[NP_all$Group == "YA"]
    ## t = -7.6524, df = 55.98, p-value = 2.898e-10
    ## alternative hypothesis: true difference in means is not equal to 0
    ## 95 percent confidence interval:
    ##  -27.61498 -16.15644
    ## sample estimates:
    ## mean of x mean of y 
    ##  50.21429  72.10000

# Composite scores

``` r
# can TMT_A and TMT_B be combined? Cronbach's Alpha should be ~0-7
psych::alpha(NP_all[c("TMT_A","TMT_B")],check.keys = TRUE)
```

    ## Number of categories should be increased  in order to count frequencies.

    ## 
    ## Reliability analysis   
    ## Call: psych::alpha(x = NP_all[c("TMT_A", "TMT_B")], check.keys = TRUE)
    ## 
    ##   raw_alpha std.alpha G6(smc) average_r S/N   ase mean sd median_r
    ##       0.48      0.75     0.6       0.6   3 0.061   35 15      0.6
    ## 
    ##  lower alpha upper     95% confidence boundaries
    ## 0.36 0.48 0.6 
    ## 
    ##  Reliability if an item is dropped:
    ##       raw_alpha std.alpha G6(smc) average_r S/N alpha se var.r med.r
    ## TMT_A      0.17       0.6    0.36       0.6 1.5       NA     0   0.6
    ## TMT_B      2.07       0.6    0.36       0.6 1.5       NA     0   0.6
    ## 
    ##  Item statistics 
    ##        n raw.r std.r r.cor r.drop mean   sd
    ## TMT_A 58  0.74  0.89  0.69    0.6   21  7.3
    ## TMT_B 58  0.98  0.89  0.69    0.6   49 25.5

``` r
## make composite scores
NP_all$TMT_A_z <- scale(NP_all$TMT_A)
NP_all$TMT_B_z <- scale(NP_all$TMT_B)
NP_all$TMT_comp <-rowMeans(NP_all[c('TMT_A_z', 'TMT_B_z')], na.rm=TRUE)

## can both RWTs be combined? Cronbach's Alpha should be ~0-7
psych::alpha(NP_all[c("RWT_Vornamen_korrekt","RWT_Hobbys_korrekt")],check.keys = TRUE)
```

    ## Number of categories should be increased  in order to count frequencies.

    ## 
    ## Reliability analysis   
    ## Call: psych::alpha(x = NP_all[c("RWT_Vornamen_korrekt", "RWT_Hobbys_korrekt")], 
    ##     check.keys = TRUE)
    ## 
    ##   raw_alpha std.alpha G6(smc) average_r S/N   ase mean  sd median_r
    ##       0.64      0.65    0.48      0.48 1.9 0.091   23 4.6     0.48
    ## 
    ##  lower alpha upper     95% confidence boundaries
    ## 0.46 0.64 0.82 
    ## 
    ##  Reliability if an item is dropped:
    ##                      raw_alpha std.alpha G6(smc) average_r  S/N alpha se var.r
    ## RWT_Vornamen_korrekt      0.61      0.48    0.23      0.48 0.94       NA     0
    ## RWT_Hobbys_korrekt        0.38      0.48    0.23      0.48 0.94       NA     0
    ##                      med.r
    ## RWT_Vornamen_korrekt  0.48
    ## RWT_Hobbys_korrekt    0.48
    ## 
    ##  Item statistics 
    ##                       n raw.r std.r r.cor r.drop mean  sd
    ## RWT_Vornamen_korrekt 58  0.89  0.86   0.6   0.48   29 6.0
    ## RWT_Hobbys_korrekt   58  0.82  0.86   0.6   0.48   18 4.7

``` r
## make composite score for RWT
NP_all$RWT_comp <-rowMeans(NP_all[c('RWT_Vornamen_korrekt', 'RWT_Hobbys_korrekt')], na.rm=TRUE)
NP_all$RWT_Vornamen_korrekt_z <- scale(NP_all$RWT_Vornamen_korrekt)
NP_all$RWT_Hobbys_korrekt_z <- scale(NP_all$RWT_Hobbys_korrekt)
NP_all$RWT_comp_z <-rowMeans(NP_all[c('RWT_Vornamen_korrekt_z', 'RWT_Hobbys_korrekt_z')], na.rm=TRUE)
```

# Scale test values

``` r
NP_all$Reading_span_z <- scale(NP_all$Reading_span)
NP_all$WST_z <- scale(NP_all$WST)
NP_all$DSST_z <- scale(NP_all$DSST)
```

# Spider plot

``` r
sub_df <- subset(NP_all, select= c("Group", "Reading_span_z", "WST_z", "DSST_z", "TMT_A_z", "TMT_B_z", "RWT_comp_z"))

sub_df <- sub_df %>%
  group_by(Group) %>%
  dplyr::summarise_at(.vars = c("Reading_span_z", "WST_z", "DSST_z", "TMT_A_z", "TMT_B_z", "RWT_comp_z"), .funs = mean) #%>%
 # dplyr:: mutate_each(funs(rescale), -group)
sub_df$Subject <- NULL
spider_df <- reshape2::melt(sub_df, id.vars = "Group")
colnames(spider_df) <- c("Group","Test","Mean")

spider <- ggplot(spider_df, aes(x = Test, y = Mean)) +
  geom_point(aes(group = Group, color = Group),size=3)+
  geom_polygon(aes(group = Group, color = Group), fill = NA, size = 2) +
  scale_y_continuous(expand = c(0,0), breaks = c(-1,-0.5,0,0.5,1), limits = c(-1,1))+
  scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c('#52257a','#018571'))+     #'#297AB1'; '#307FE2',gp3[5]; 002E9D
  scale_x_discrete(labels  = c("Reading span", "STW", "DSST", "TMT A", "TMT B", "Verbal fluency"))+
  coord_polar(start = pi+0.63, clip = "off")+
  theme_minimal() +
  theme(panel.grid.minor = element_blank(),
        panel.grid.major.y = element_line(size = 1),
        panel.grid.major.x = element_line(size = 1),
        text=element_text(family = 'sans', size = 18),
       # strip.text.x = element_text(),
        axis.text.x = element_text(size = 18, colour = "black", vjust=-1),
        axis.text.y = element_blank(),
       #axis.line.x = element_blank(),
        legend.position = c(0.1,-0.02),
        legend.justification = c(-0.3,1),
        legend.direction = "horizontal",
       plot.margin = margin(0,0,20,0)) +
  labs(x = "", y = "", colour = "Age group") 
spider
```

![](MDN_LANG_NP_files/figure-markdown_github/unnamed-chunk-7-1.png)

