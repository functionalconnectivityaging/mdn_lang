#!/bin/bash
set -x

# This script copies contrast images from individual participant folders to second level group folders

for x in {001..030}; do
for z in {0001..0014};do
mkdir -p /data/pt_02004/MDN_LANG/Derivatives/group_statistics/fmriprep_fd/{Categories_Counting,Difficult_Easy,CountingForw_CountingBackw,Difficult_All,Easy_All,CountingForw_All,CountingBackw_All,Counting_All,Speech_All,Categories_All}
mv /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_"$z".nii /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_"$z"_"$x".nii
cp /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_0001_"$x".nii /data/pt_02004/MDN_LANG/Derivatives/group_statistics/fmriprep_fd/Speech_All
cp /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_0002_"$x".nii /data/pt_02004/MDN_LANG/Derivatives/group_statistics/fmriprep_fd/Categories_Counting
cp /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_0003_"$x".nii /data/pt_02004/MDN_LANG/Derivatives/group_statistics/fmriprep_fd/Difficult_Easy
cp /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_0006_"$x".nii /data/pt_02004/MDN_LANG/Derivatives/group_statistics/fmriprep_fd/CountingForw_CountingBackw
cp /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_0009_"$x".nii /data/pt_02004/MDN_LANG/Derivatives/group_statistics/fmriprep_fd/Difficult_All
cp /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_0010_"$x".nii /data/pt_02004/MDN_LANG/Derivatives/group_statistics/fmriprep_fd/Easy_All
cp /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_0011_"$x".nii /data/pt_02004/MDN_LANG/Derivatives/group_statistics/fmriprep_fd/CountingForw_All
cp /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_0012_"$x".nii /data/pt_02004/MDN_LANG/Derivatives/group_statistics/fmriprep_fd/CountingBackw_All
cp /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_0013_"$x".nii /data/pt_02004/MDN_LANG/Derivatives/group_statistics/fmriprep_fd/Categories_All
cp /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/fmriprep/1st_level_fd/con_0014_"$x".nii /data/pt_02004/MDN_LANG/Derivatives/group_statistics/fmriprep_fd/Counting_All
done
done
