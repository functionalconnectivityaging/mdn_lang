%% Batch for 2nd level analysis %%

data_path = '/data/pt_02004/MDN_LANG/Derivatives/group_statistics/SST_YA/';
spm_path = '/data/pt_02004/spm12';

GM_mask = '/data/pt_02004/Masks&ROIs/GM_masks/GM_spm_0.3.nii';

RT = '/data/pt_02004/MDN_LANG/Derivatives/group_statistics/RT_JE.txt';

% contrasts = {'Categories_Counting','Categories_All','Counting_Categories','Counting_All', ...
%     'CountingBackw_CountingForw','CountingBackw_All','CountingForw_CountingBackw', ...
%     'CountingForw_All','Dificult_Easy','Difficult_All','Easy_Difficult', ...
%     'Easy_All','Speech_All'};
      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');
  
%% Categories_Counting
con_path = [data_path 'Categories_Counting_masked_covRT'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {RT};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Categories>Counting';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Counting>Categories';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
%% Categories_All
con_path = [data_path 'Categories_All_masked_covRT'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {RT};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Categories>All';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'All>Categories';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
%% Counting_All
con_path = [data_path 'Counting_All_masked_covRT'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {RT};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Counting>All';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'All>Counting';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
%% Speech_All
con_path = [data_path 'Speech_All_masked_covRT'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {RT};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Speech>All';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'All>Speech';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    

%% Difficult_Easy
con_path = [data_path 'Difficult_Easy_masked_covRT'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {RT};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Difficult>Easy';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Easy>Difficult';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)