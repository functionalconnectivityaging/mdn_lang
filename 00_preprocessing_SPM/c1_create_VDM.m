function create_fieldmap(myfieldmap, myfieldmap_mag,myepis, myt1)
% create_fieldmap uses the SPM fieldmap toolbox to create a vdm from a given fieldmap (probably created with FSL from the AP/PA scans).

% Make sure you have a pm_defaults.m file in the matlab searchpath to set
% globals pm_def
% pm_def.TOTAL_EPI_READOUT_TIME = 23.8952 ms;

% specify paths
% SPM path
addpath /data/pt_02004/spm12
% Data folder
script_path =  '/data/pt_02004';
data_path = '/data/pt_02004/MDN_LANG/';

% Subject_folders

% sub = {'002','003','004','005','006','007','008','009','010', '011', ...
%         '012','013','014','015','016','017','018','019', ...
%          '021','022','023','024','025','026','027','028','029','030'};
 sub = {'020'};
run = {'2'};
     
for i = 1:numel(sub)
for x = 1:numel(run)
    
    myfieldmap = [data_path 'Derivatives/subjects/' sub{i} '_JE/fmaps/run-' run{x} '/fpm_fieldmap_sub-control' sub{i} '_run-' run{x} '.img'];
    myfieldmap_mag = [data_path 'Derivatives/subjects/' sub{i} '_JE/fmaps/run-' run{x} '/fpm_mag_sub-control' sub{i} '_run-' run{x} '.img'];
    myepis = [data_path 'Derivatives/subjects/' sub{i} '_JE/preprocessing_Echo2_SPM/func/run-' run{x} '/rasub-control' sub{i} '_task-SWG_run-' run{x} '_echo-2_bold_trimmed.nii'];
    myt1 = [data_path 'Derivatives/subjects/' sub{i} '_JE/preprocessing_Echo2_SPM/anat/run-' run{x} '/sub-control' sub{i} '_T1w.nii'];
    X = ['This is participant ' sub{i} ' and run ' run{x}];
    disp(X);
    
% Set up default parameters and structures
spm('defaults','FMRI');
IP = FieldMap('Initialise','/data/pt_02004/Scripts/Matlab/Preprocessing/pm_defaults.m'); % Gets default params from pm_defaults
global pm_def
disp(pm_def.TOTAL_EPI_READOUT_TIME);

% aIP = FieldMap('Initialise'); % Gets default params from pm_defaults

%----------------------------------------------------------------------
% Or you may want to load a precalculated Hz phase map instead...
%----------------------------------------------------------------------

% [IP.fm, IP.pP] = FieldMap('LoadFieldMap');
% aIP.pP = spm_vol(spm_select(1,'^fpm.*\.img$','Select field map'));
IP.pP = spm_vol(myfieldmap);

IP.fm.fpm = spm_read_vols(IP.pP);
IP.fm.jac = pm_diff(IP.fm.fpm,2);
if isfield(IP,'P') && ~isempty(IP.P{1})
    IP.P = cell(1,4);
end

%----------------------------------------------------------------------
% Create field map (in Hz) - this routine calls the unwrapping
%----------------------------------------------------------------------

% IP.fm = FieldMap('CreateFieldMap',IP);

%----------------------------------------------------------------------
% Write out field map
% Outputs -> fpm_NAME-OF-FIRST-INPUT-IMAGE.img
%----------------------------------------------------------------------

% FieldMap('Write',IP.P{1},IP.fm.fpm,'fpm_',64,'Smoothed phase map');

%----------------------------------------------------------------------
% Convert Hz to voxels and write voxel displacement map
% Outputs -> vdm_NAME-OF-FIRST-INPUT-IMAGE.img
%----------------------------------------------------------------------

[IP.vdm, IP.vdmP]=FieldMap('FM2VDM',IP);

%----------------------------------------------------------------------
% Select an EPI to unwarp
%----------------------------------------------------------------------

% IP.epiP = FieldMap('LoadEPI');
temp = spm_vol(myepis);
IP.epiP = temp(1);

%----------------------------------------------------------------------
% Match voxel displacement map to image
% Outputs -> mag_NAME-OF-FIRST-INPUT-IMAGE.img
%----------------------------------------------------------------------

% IP.vdmP = FieldMap('MatchVDM',IP);

IP.fmagP = spm_vol(myfieldmap_mag);

% Now we have field map magnitude image, we want to coregister it to the
% EPI to be unwarped.
% If using an EPI field map:
% 1) Coregister magnitude image to EPI.
% 2) Apply resulting transformation matrix to voxel shift map
% If using a non-EPI field map:
% 1) Forward warp magnitude image
% 2) Coregister warped magnitude image to EPI.
% 3) Apply resulting transformation matrix to voxel shift map IP.vdmP;


% Need to sample magnitude image in space of EPI to be unwarped...
[x,y,z] = ndgrid(1:IP.epiP.dim(1),1:IP.epiP.dim(2),1:IP.epiP.dim(3));
xyz = [x(:) y(:) z(:)];

% Space of EPI is IP.epiP{1}.mat and space of fmagP is IP.fmagP.mat
tM = inv(IP.epiP.mat\IP.fmagP.mat);
x2 = tM(1,1)*x + tM(1,2)*y + tM(1,3)*z + tM(1,4);
y2 = tM(2,1)*x + tM(2,2)*y + tM(2,3)*z + tM(2,4);
z2 = tM(3,1)*x + tM(3,2)*y + tM(3,3)*z + tM(3,4);
xyz2 = [x2(:) y2(:) z2(:)];
wfmag = reshape(spm_sample_vol(IP.fmagP,xyz2(:,1),...
    xyz2(:,2),xyz2(:,3),1),IP.epiP.dim(1:3));

% Need to sample voxel shift map in space of EPI to be unwarped
tvdm = reshape(spm_sample_vol(IP.vdm.vdm,xyz2(:,1),...
    xyz2(:,2),xyz2(:,3),0),IP.epiP.dim(1:3));

% Now apply warps to resampled forward warped magnitude image...
wfmag = reshape(spm_sample_vol(wfmag,xyz(:,1),xyz(:,2)-tvdm(:),...
    xyz(:,3),1),IP.epiP.dim(1:3));

% Write out forward warped magnitude image
IP.wfmagP = struct('dim',  IP.epiP.dim,...
    'dt',[64 spm_platform('bigend')],...
    'pinfo',   IP.epiP.pinfo,...
    'mat',     IP.epiP.mat);
IP.wfmagP = FieldMap('Write',IP.epiP,wfmag,'wfmag_',4,'Voxel shift map');

% Now coregister warped magnitude field map to EPI
[mi,M] = FieldMap('Coregister',IP.epiP,IP.wfmagP);

% Update the .mat file of the forward warped mag image
spm_get_space(deblank(IP.wfmagP.fname),M*IP.wfmagP.mat);

% Get the original space of the fmap magnitude
MM = IP.fmagP.mat;

% Update .mat file for voxel displacement map
IP.vdmP.mat=M*MM;
spm_get_space(deblank(IP.vdmP.fname),M*MM);

%----------------------------------------------------------------------
% Unwarp EPI
%----------------------------------------------------------------------

% IP.uepiP = FieldMap('UnwarpEPI',IP);

%----------------------------------------------------------------------
% Write unwarped EPI
% Outputs -> uNAME-OF-EPI.img
%----------------------------------------------------------------------

% IP.uepiP = FieldMap('Write',IP.epiP,IP.uepiP.dat,'u',IP.epiP.dt(1),'Unwarped image');

%----------------------------------------------------------------------
% Load a structural image
%----------------------------------------------------------------------

% IP.nwarp=FieldMap('LoadStructural');
  IP.nwarp = spm_vol(myt1);
%----------------------------------------------------------------------
% Coregister structural with the unwarped image
%----------------------------------------------------------------------

% FieldMap('MatchStructural',IP);
end
end
end

