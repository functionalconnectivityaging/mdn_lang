#!/bin/bash
#SCRIPT_PATH="./combineMultiEchoData.sh"
set -x
for x in {001..031}; do
for z in 1 2; do
./combineMultiEchoData.sh /data/pt_02004/MDN_LANG/Dicom/"$x"/func/run-"$z" /data/pt_02004/MDN_LANG/Derivatives/subjects/"$x"/Preprocessing_29_prepVols_w.o_FieldMap 5 --nPrepVols 29 --info
echo "This was participant "$x"."
done
