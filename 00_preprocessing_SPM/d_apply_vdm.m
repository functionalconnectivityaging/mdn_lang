%% Apply VDM script
% This script applies the previously created voxel displacement map (VDM) for
% each run to all EPIs

%% Specify paths & folders
%  Data folder
script_path =  '/data/pt_02004/Scripts/Matlab/Preprocessing';
data_path = '/data/pt_02004/MDN_LANG/Derivatives/subjects/';
addpath /data/pt_02004/spm12;

% Subject_folders
 sub = {'002','003','004','005','006','007','008','009','010', '011', ...
        '012','013','014'};
%         '015','016','017','018','019', ...
%          '021','022','023', '024','025','026','027','028','029','030'};
% sub = {'020'};
run = {'1','2'};
      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');

% Loop through subject folders
for i = 1:numel(sub) 
for x = 1:numel(run)

    % Display which participant and which run is currently processed
    X = ['This is participant ' sub{i} ' and run ' run{x}];
        disp(X);

%     %% Get functional scans of current subject & current session
%     % because spm_vol first needs to count number of volumes from 4D scan
    epi_mainfolder = dir([data_path sub{i} '_JE/preprocessing_Echo2_SPM/func/run-' run{x}]); 
    curr_epi_subfolder = epi_mainfolder.folder;
    nifti_files = dir([curr_epi_subfolder '/rasub-control' sub{i} '_task-SWG_run-' run{x} '_echo-2_bold_trimmed.nii']);
    func_images_path = [nifti_files.folder '/' nifti_files.name];
        func_vols = spm_vol(func_images_path);
        func_struct = {};
        for iVol = 1:numel(func_vols)
            func_struct{iVol} = [func_images_path ',' num2str(iVol)];
        end
        func_struct = func_struct';

    %% Get VDM
    fmap_path = [data_path sub{i} '_JE/fmaps/run-' run{x}];
    vdm_file = dir([fmap_path '/vdm5_fpm_fieldmap_sub-control' sub{i} '_run-' run{x} '.img']);
    vdm_path = [vdm_file.folder '/' vdm_file.name];

    %% This is the batch for applying the VDM
    clear matlabbatch
    matlabbatch{1}.spm.tools.fieldmap.applyvdm.data.scans = func_struct; % If you used the second method with a 4D-Nifti, put 'func_struct' here
    matlabbatch{1}.spm.tools.fieldmap.applyvdm.data.vdmfile = {vdm_path};
    matlabbatch{1}.spm.tools.fieldmap.applyvdm.roptions.pedir = 2;
    matlabbatch{1}.spm.tools.fieldmap.applyvdm.roptions.which = [2 1];
    matlabbatch{1}.spm.tools.fieldmap.applyvdm.roptions.rinterp = 7;
    matlabbatch{1}.spm.tools.fieldmap.applyvdm.roptions.wrap = [0 0 0];
    matlabbatch{1}.spm.tools.fieldmap.applyvdm.roptions.mask = 1;
    matlabbatch{1}.spm.tools.fieldmap.applyvdm.roptions.prefix = 'u';


    %% RUN THE BATCH
    % Use 'interactive' to get better error messages and to see batch GUI
    % Use 'run' to simply run the batch
    spm_jobman('run', matlabbatch)

end
end