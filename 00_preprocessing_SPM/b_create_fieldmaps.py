#!/usr/bin/env python
# Sandra Martin
# 2018/11/30

import os
#import subprocess
import glob

base_dir = "/data/pt_02004/MDN_LANG"

os.chdir(base_dir)

#participants = ["002","007", "008", "009", "010", "011", "012",
#                   "013", "014", "015", "016", "017", "018",
#                   "019", "020", "021", "022","023","024",
#                   "025","026","027","028","029","030"]
participants = ["020"]
sub = "sub-control{0}"
s = "{0}_JE"

ap_name_run_1 = "sub-control{0}_dir-AP_run-1_epi.nii.gz"
pa_name_run_1 = "sub-control{0}_dir-PA_run-1_epi.nii.gz"
ap_name_run_2 = "sub-control{0}_dir-AP_run-2_epi.nii.gz"
pa_name_run_2 = "sub-control{0}_dir-PA_run-2_epi.nii.gz"

for vp in participants:
    
    ## 1. Use fslmerge to combine the two se-epi images into one 4D file:
    ## fslmerge -t [output] [inputAP] [inputPA].
    ## one ap/pa pair per run. I have 2 runs

    ap_r1 = ap_name_run_1.format(vp)
    pa_r1 = pa_name_run_1.format(vp)
    ap_r2 = ap_name_run_2.format(vp)
    pa_r2 = pa_name_run_2.format(vp)
    
    appa_dir = os.path.join(base_dir, "Nifti", sub.format(vp), "fmap")
    os.chdir(appa_dir)
    output_fm_1 = "fieldmap_run-1"
    output_fm_2 = "fieldmap_run-2"
    print("This is the fslmerge of participant %s"%vp)
    os.system("fslmerge -t /data/pt_02004/MDN_LANG/Derivatives/subjects/%s_JE/fmaps/%s.nii.gz %s %s"%(vp,output_fm_1, ap_r1, pa_r1))
    os.system("fslmerge -t /data/pt_02004/MDN_LANG/Derivatives/subjects/%s_JE/fmaps/%s.nii.gz %s %s"%(vp,output_fm_2, ap_r2, pa_r2))

    
    # 2. Check if image direction and total readout time of epis is the same
    
    nifti_dir = os.path.join(base_dir, "Nifti", sub.format(vp), "fmap")
    os.chdir(nifti_dir)
    
    total_readout_time = "0.0238952"
    phase_encoding_direction_norm = "j-"
    phase_encoding_direction_invPol = "j"
    
    file1 = "*_AP*.json"
    for f in glob.glob(file1):
        if total_readout_time in open(f).read():
            print("total readout time = 0.0238952 for %s in norm*: true"%vp)
        else:
            print("total readout time = 0.0238952 for %s in norm*: false"%vp)
        
        if phase_encoding_direction_norm in open(f).read():
            print("phase encoding in norm j- for %s in norm*: true"%vp)
        else:
            print("phase encoding in norm j- for %s in norm*: false"%vp)
    
    file2 = "*_PA*.json"
    for f in glob.glob(file2):
        if total_readout_time in open(f).read():
            print("total readout time = 0.0238952 for %s in invPol*: true"%vp)
        else:
            print("total readout time = 0.0238952 for %s in invPol*: false"%vp)    
    
        if phase_encoding_direction_invPol in open(f).read():
            print("phase encoding in invPol j for %s in invPol*: true"%vp)
        else:
            print("phase encoding in norm j- for %s in invPol*: false"%vp)
    
    
    ## 3. Run topup. You can use the default config file b02b0.cnf (you do not need to create this file). The command will look something like this:
    ## topup --imain=se_epi_merged --datain=datain.txt --config=b02b0.cnf --fout=my_fieldmap --iout=se_epi_unwarped
       
    fmap_dir = os.path.join(base_dir, "Derivatives", "subjects", s.format(vp), "fmaps")
    os.chdir(fmap_dir)
 
    input_fm_run_1 = "fieldmap_run-1.nii.gz"
    input_fm_run_2 = "fieldmap_run-2.nii.gz"
    input_acqparams = "acqparams.txt"
    output_fm_run_1 = "fpm_fieldmap_subject_%s_run-1"%vp
    output_fm_run_2 = "fpm_fieldmap_subject_%s_run-2"%vp
    
    print("This is topup creating fieldmaps of participant %s"%vp)
    
    os.system("topup --imain=%s --datain=/data/pt_02004/MDN_LANG/Derivatives/subjects/%s --config=b02b0.cnf --fout=%s --iout=se_epi_unwarped_run-1"%(input_fm_run_1, input_acqparams, output_fm_run_1))
    os.system("topup --imain=%s --datain=/data/pt_02004/MDN_LANG/Derivatives/subjects/%s --config=b02b0.cnf --fout=%s --iout=se_epi_unwarped_run-2"%(input_fm_run_2, input_acqparams, output_fm_run_2))
    
    
    ### 4. Your fieldmap was just created. Now create a single magnitude image for each fieldmap since you'll need it for creation of the voxel displacement map - both with SPM and FSL
    ### fslmaths se_epi_unwarped -Tmean my_fieldmap_mag
    
    input_mag_run_1 = "se_epi_unwarped_run-1"
    input_mag_run_2 = "se_epi_unwarped_run-2"
    output_mag_run_1 = "magnitude_img_subject_%s_run-1"%vp
    output_mag_run_2 = "magnitude_img_subject_%s_run-2"%vp
    
    print("This is fslmaths creating magnitude imgs of participant %s"%vp)
    
    os.system("fslmaths %s -Tmean %s"%(input_mag_run_1, output_mag_run_1))
    os.system("fslmaths %s -Tmean %s"%(input_mag_run_2, output_mag_run_2))
    
    
    ### 5. Change file type of fieldmap with fslutils since SPM requires that fieldmap is in a NIFTI_PAIR format for calculation of VDM
    ### fslchfiletype - used to change the file type of an image (e.g. from ANALYZE_GZ to NIFTI). The first argument is the desired file type (one of ANALYZE, ANALYZE_GZ, NIFTI, NIFTI_GZ, NIFTI_PAIR, NIFTI_PAIR_GZ) and the second is the input file. If no third argument is given then the input file is converted in place. This in place conversion removes the original files: e.g. for an Analyze file called stdimg then fslchfiletype NIFTI_GZ stdimg would replace stdimg.hdr and stdimg.img with stdimg.nii.gz. Note that having multiple copies of an image with the same basename and different filetypes (e.g. stdimg.nii.gz and stdimg.hdr and stdimg.img) creates many problems for determining what images to read, and in general will cause FSL programs to stop. 
    
    input_chfiletype_run_1 = output_fm_run_1
    input_chfiletype_run_2 = output_fm_run_2
    output_fm_run1 = "run-1/fpm_fieldmap_sub-control%s_run-1"%vp
    output_fm_run2 = "run-2/fpm_fieldmap_sub-control%s_run-2"%vp
    
    print("This is fslchfiletype creating a NIFTI_PAIR of the fieldmap of participant %s"%vp)
    
    os.system("fslchfiletype NIFTI_PAIR %s %s"%(input_chfiletype_run_1, output_fm_run1))
    os.system("fslchfiletype NIFTI_PAIR %s %s"%(input_chfiletype_run_2, output_fm_run2))
    
    input_chfiletype_run_1 = output_mag_run_1
    input_chfiletype_run_2 = output_mag_run_2
    output_mag_run1 = "run-1/fpm_mag_sub-control%s_run-1"%vp
    output_mag_run2 = "run-2/fpm_mag_sub-control%s_run-2"%vp
    
    print("This is fslchfiletype creating a NIFTI_PAIR of the magnitude image of participant %s"%vp)
    
    fmap_dir = os.path.join(base_dir, "Derivatives", "subjects", s.format(vp), "fmaps")
    os.chdir(fmap_dir)
    os.system("fslchfiletype NIFTI_PAIR %s %s"%(input_chfiletype_run_1, output_mag_run1))
    fmap_dir = os.path.join(base_dir, "Derivatives", "subjects", s.format(vp), "fmaps")
    os.chdir(fmap_dir)
    os.system("fslchfiletype NIFTI_PAIR %s %s"%(input_chfiletype_run_2, output_mag_run2))