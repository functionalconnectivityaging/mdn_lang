## --------------------------------------------------------------------------------------------
## MAIN PROGRAM
## --------------------------------------------------------------------------------------------
export SPM_VERSION=12.7219

nEchoes=2
nPrepVols=30
scriptDir=$HOME/matlab/TM/SPM
seqType=cmrr
doSlicetiming=none

INPUT_PARAMETERS=($@)
c=0
for var in ${INPUT_PARAMETERS[@]}
do
	c="$((c+=1))"
	if [ "$var" == "--nEchoes" ] ; then
		nEchoes=${INPUT_PARAMETERS[$c]}
	fi
	if [ "$var" == "--nPrepVols" ] ; then
		nPrepVols=${INPUT_PARAMETERS[$c]}
	fi
	if [ "$var" == "--scriptDir" ] ; then
		scriptDir=${INPUT_PARAMETERS[$c]}
	fi
	if [ "$var" == "--sequenceType" ] ; then 
		seqType=${INPUT_PARAMETERS[$c]}
	fi
	if [ "$var" == "--slicetime" ] ; then
		doSlicetiming=slicetime
	fi
	if [ "$var" == "--info" ] ; then
		echo
		echo "% This function first converts DICOM data of multi-echo EPI scans ('nEchoes') into single nifti files.
% Both cmrr_mbep2d and cbsepi sequences are supported (use flag --sequenceType cmrr/cbsepi when calling the script).
% Data should be located as raw DICOM files in the directory 'DICOM-Path'. The output is written into subdirectories 
% with the corresponding scan number(s) located in the 'Output-Path' (a single dir plus sub-dirs for each echo for cmrr_mbep2d,
% and multiple dirs corresponding to the scan number of each echo for cbsepi).
% If requested, a slice-time correction with respect to TR/2 is applied as first processing step. For the first echo, a realignment
% using the spm_realign function is performed. Reslicing is done for all echoes based on the information obtained from the first echo.
% Then, tSNR maps for all echoes are calculated and written into the corresponding subdirectories. They are based on the first
% 'nPrepVols' repetitions of the single-echo time series.
% The time series of all echoes are finally weighted by use of the corresponding tSNR values and combined.
% The resulting hybrid EPI time series is written into a separate directory. Note that the first nPrepVols repetitions are cutted off !
% For quality control, a tSNR map of the hybrid EPI time series is also calculated after application of a high-pass filter with
% a cut-off interval of 10*TR."
		echo
	fi
done

if [ $# -lt 3 ] ; then
	echo "Usage: $0 DICOM-Path Output-Path Scan [--nEchoes (def. 2)] [--nPrepVols (def.: 30)]..."
	echo "...[--scriptDir (def.: $HOME/matlab/TM/SPM)] [--sequenceType cmrr(def.)|cbsepi] [--slicetime] [--info]"
	exit
fi

echo
echo "Scripts for combining multi-echo EPI time series into a single hybrid time series..."
echo "provided without warranty by Toralf Mildner, MPI-CBS, Leipzig, 2013, 2017/18."
echo

DICOMPATH=$1
OUTPUTPATH=$2
SCAN=$3

MATLAB --version 9.3 matlab -nosplash -nodesktop -r "path(path,'$scriptDir');combineMultiEchoData('$DICOMPATH','$OUTPUTPATH','$SCAN','$nEchoes','$nPrepVols','$seqType','$doSlicetiming');exit" 2>/dev/null


