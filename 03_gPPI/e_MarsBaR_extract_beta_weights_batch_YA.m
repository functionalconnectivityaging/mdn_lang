% Marsbar batch for ROIs on 1st level PPIs 
% Young adults

clear

%% Paths

%Setup SPM12
addpath('/data/pt_02004/spm12/')

% define main folder
main_folder = '/data/pt_02004/MDN_LANG/Derivatives/';

% Define ROI folder
roi_folder = [main_folder 'group_statistics/gPPI/ROIs_based_on_YA/'];

% define first level directory
subject_folder = [main_folder 'subjects/'];
ppi_folder = 'PPI';

% start up SPM12 in fMRI-modecurrent
spm('Defaults','fMRI');
spm_jobman('initcfg');

% Start marsbar to make sure spm_get works
addpath /data/pt_02004/spm12/toolbox/marsbar/
marsbar('on')

% Subject_folders
sub = {'001_JE','002_JE','003_JE','004_JE','005_JE','006_JE','007_JE','008_JE','009_JE','010_JE','011_JE', ...
          '012_JE','013_JE','014_JE','015_JE','016_JE','017_JE','018_JE','019_JE','020_JE', ...
         '021_JE','022_JE','023_JE','024_JE','025_JE','026_JE','027_JE','028_JE','029_JE','030_JE'};
   

ROI_names = {'Insula' 'Insula1' 'Insula2' 'Insula3' 'Insula4' 'dACC'...
    'RInsula' 'RInsula1' 'SPL' 'SPL1' 'SPL2' 'RMFG' 'RMFG1' 'RMFG2'...
    'RTempPole' 'RTempPole1' 'RPrec' 'RPrec1' 'RPrec2' 'MTG' 'MTG1'...
    'ACing' 'ACing1' 'LAG' 'LAG1' 'LAG2' 'LAG3' 'LAG4' 'RAG' 'RAG1' 'RAG2'};

ROI_names = {'Insula' 'Insula1' 'Insula2' 'Insula3' 'Insula4' 'dACC'...
    'RInsula' 'RInsula1' 'SPL' 'SPL1' 'SPL2' 'RMFG' 'RMFG1' 'RMFG2'...
    'RTempPole' 'RTempPole1' 'RPrec' 'RPrec1' 'RPrec2' 'MTG' 'MTG1'...
    'ACing' 'ACing1' 'LAG' 'LAG1' 'LAG2' 'LAG3' 'LAG4' 'RAG' 'RAG1' 'RAG2'};

%% Make a loop – first subjects, then ROIs
for isub = 1:numel(sub)
   for iRoi = 1:numel(ROI_names)
       
       % open text file and write header information
       fid=fopen(['/data/pt_02004/Beta_weights/ROIs_based_on_YA_calculated_outputs/beta_weights_' ROI_names{iRoi} '_JE.txt'],'a');

       % open text file and write header information 
       %subjectnum = str2double(sub{isub});       
       if isub == 1
          fprintf(fid,'Subject\tROI_seed\tROI_target\tCats_rest\tCats_Counting\n');
       end
       
       
       for itarget = 1:numel(target_ROIs)
           
           if iRoi == itarget
              continue
           end

           % Display which participant and which ROI is currently processed
           disp(['This is subject ' sub{isub} ' and seed ' ROI_names{iRoi} ' and target ROI ' target_ROIs{itarget}]);

           % Get SPM.mat file
           main_sessionfolder = [subject_folder sub{isub} filesep ppi_folder];
           
           % Check if mat file exists, i.e., if subject had any voxels in
           % this ROI
           mat_file = dir([main_sessionfolder filesep 'gPPI_individualVOI_' ROI_names{iRoi} ...
               '_th0.01_5mm' filesep 'VOI_' ROI_names{iRoi} '_double_radius_*.mat']);

           if isempty(mat_file)
               fprintf('No ROI here\n');
               continue;
           end
           
           model_dir = [main_sessionfolder filesep 'gPPI_individualVOI_' ROI_names{iRoi} ...
             '_th0.01_5mm/PPI_VOI_' ROI_names{iRoi} '_double_radius_mask/SPM.mat'];

           % Get _roi.mat
           roi = [roi_folder filesep target_ROIs{itarget} '_transformed_roi.mat'];

%% Start marsbar structure

           % Make marsbar design object
           D  = mardo(model_dir);
           % Make marsbar ROI object
           R  = maroi(roi);
           % Fetch data into marsbar data object
           Y  = get_marsy(R, D, 'mean');
           % Get contrasts from original design
           xCon = get_contrasts(D);
           % Estimate design on ROI data
           E = estimate(D, Y);
           % Put contrasts from original design back into design object
           E = set_contrasts(E, xCon);
           % get design betas
           b = betas(E);
           % get stats and stuff for all contrasts into statistics structure
           marsS = compute_contrasts(E, 1:length(xCon));
           
%% Write beta weights to text file
           
           fprintf(fid, '%s\t%s\t%s\t%d\t%d\n', [sub{isub}], [ROI_names{iRoi}], [target_ROIs{itarget}], marsS.con(4), marsS.con(5));
           
       end
   end
end
