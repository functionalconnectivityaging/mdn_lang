% VOI extraction based on individual peaks
% load first level first, then jump to individual peak within ROI (10 mm radius) and
% draw sphere (5 mm). save and move files to individual gppi folder. 
%*mask.nii will be used in the gPPI analysis to extract eigenvalues

clear all

% VOIs are based on results for young adults 

% VOIs based on contrast "Semantic fluency > Counting" and mask for multiple-demand network:
ROI_names_fluency = {'Insula' 'Insula1' 'Insula2' 'Insula3' 'Insula4' 'dACC' 'RInsula' 'RInsula1' 'SPL' 'SPL1' 'SPL2' 'RMFG' 'RMFG1' 'RMFG2' };
% ROI_coords =  {[-31 25 2] [-4 25 40] [-6 12 51] [13 27 29] [4 20 40] [-4 2 29] [31 27 2] [38 20 -4] [-29 -65 51] [-29 -72 43] [-34 -57 40] [36 42 32] [31 55 26] [33 37 21]};

% VOIs based on contrast "Counting > Semantic fluency" and mask for default mode network:
ROI_names_counting = {'RTempPole' 'RTempPole1' 'RPrec' 'RPrec1' 'RPrec2' 'MTG' 'MTG1' 'ACing' 'ACing1' 'LAG' 'LAG1' 'LAG2' 'LAG3' 'LAG4' 'RAG' 'RAG1' 'RAG2'};
% ROI_coords =  {[51 10 -31] [48 -10 -15] [8 -65 29] [11 -52 35] [-9 -52 35] [-56 2 -20] [-54 10 -31] [-6 27 -6] [-6 42 -4] [-54 -62 35] [-41 -60 26] [-46 -62 18] [-46 -75 35] [-49 -67 43] [51 -57 26] [46 -65 48] [43 -72 35]};

%% Paths
% define main folder
main_folder = '/data/pt_02004/MDN_LANG/Derivatives/subjects/';

addpath /data/pt_02004/spm12

% define first level directory
firstlevel_dir = 'fmriprep/1st_level_fd';
ppi_folder = 'PPI';

% % start up SPM12 in fMRI-mode current
spm('Defaults','fMRI');
spm_jobman('initcfg'); 

% Subject_folders
% OA
%  sub = {'002','003','004','005','006','007','008','009','011', ...
%            '012','013','014','015','016','017','018','019','020', ...
%            '021','022','023','024','025','027','028','029','030','031'};

% YA
sub = {'001_JE','002_JE','003_JE','004_JE','005_JE','006_JE','007_JE','008_JE','009_JE','010_JE','011_JE', ...
          '012_JE','013_JE','014_JE','015_JE','016_JE','017_JE','018_JE','019_JE','020_JE', ...
         '021_JE','022_JE','023_JE','024_JE','025_JE','026_JE','027_JE','028_JE','029_JE','030_JE'};

run = {[1] [2]};

ROI_names = {'preSMA' 'Insula' 'RInsula' 'LMFGv' 'LMFGd' 'RMFG' 'RIntracalcarine' 'LIntracalcarine' 'SPL' 'RTempPole' 'RPrec'};
ROI_coords =  {[-9 15 51] [-31 25 4] [31 27 2] [-44 5 35] [-34 0 57] [43 35 32] [18 -80 7] [-11 -72 10] [-14 -65 51] [51 12 -31] [6 -52 38]};
sphere = 5; % to match original analysis, according to smoothing level
threshold = {[0.01]}; 

%% go through all subfolders and load first level SPM for later VOI extraction
for isub = 1:numel(sub)
    for irun = 1:numel(run)
        for iRoi = 1:numel(ROI_names)
            for ithresh = 1:numel(threshold)
                 % Display which participant and which run is currently processed
                   X = ['This is participant ' sub{isub} ' and run ' num2str(run{irun}) ' and ROI ' ROI_names{iRoi} ' and threshold ' num2str(threshold{ithresh})];
                   disp(X);
            
                   main_sessionfolder = [main_folder filesep sub{isub}];

                   % create gPPI directory 
                   PPI_folder_path = [main_sessionfolder filesep ppi_folder filesep 'gPPI_individualVOI_' ROI_names{iRoi} '_th' num2str(threshold{ithresh}) '_5mm'];
                    if ~exist(PPI_folder_path)
                        mkdir(PPI_folder_path)
                    end 

                   % go into subjects' first level folder
                    firstLevelFolder_path = [main_sessionfolder filesep firstlevel_dir];
                    cd(firstLevelFolder_path);
        
                    % define SPM path
                    SPM_mat_path = [firstLevelFolder_path filesep 'SPM.mat'];

                    % define which contrast to use, depending on the VOI:
                     contrast = 5;
                     if strcmp(ROI_names{iRoi}, ROI_names_counting)
                         contrast = 5
                     else
                    	 contrast = 2;
                     end
      

%%   % VOI: EXTRACTING TIME SERIES
%==========================================================================
%% find nearst activated voxel for each participant (give coordinate from group peak)
             
% 1. It searches the nearest voxel to group level peak
%    for specific contrast and threshold for specific subject in MASK
% 2. It creates a txt file with nearest voxel per subject
%
% IMPORTANT: number of voxels in ROI changes, according to the activation
% cluster size      

                    matlabbatch{1}.spm.util.voi.spmmat = {SPM_mat_path};
                    matlabbatch{1}.spm.util.voi.adjust = 8; % adjust for EOI FULL
                    matlabbatch{1}.spm.util.voi.session = run{irun};
                    matlabbatch{1}.spm.util.voi.name = [ROI_names{iRoi} '_double_radius'];
                    
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.spmmat = {''};
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.contrast = contrast; 
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.conjunction = 1;
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.threshdesc = 'none';
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.thresh = threshold{ithresh}; % 
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.extent = 0;
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.mask = struct('contrast', {}, 'thresh', {}, 'mtype', {});

                    matlabbatch{1}.spm.util.voi.roi{2}.sphere.centre = [ROI_coords{iRoi}];
                    matlabbatch{1}.spm.util.voi.roi{2}.sphere.radius = 10;
                    matlabbatch{1}.spm.util.voi.roi{2}.sphere.move.fixed = 1;
                   
                    % Define smaller inner sphere which jumps to the peak of the outer sphere
                    matlabbatch{1}.spm.util.voi.roi{3}.sphere.centre = [0 0 0]; % Leave this at zero
                    matlabbatch{1}.spm.util.voi.roi{3}.sphere.radius = sphere;       % Set radius here (mm)
                    matlabbatch{1}.spm.util.voi.roi{3}.sphere.move.local.spm = 1;       % Index of SPM within the batch
                    matlabbatch{1}.spm.util.voi.roi{3}.sphere.move.local.mask = 'i2';    % Index of the outer sphere within the batch
                    
                    matlabbatch{1}.spm.util.voi.expression = 'i1 & i3';
                    
                    spm_jobman('run',matlabbatch);
                    
%% find next voxel (give coordinate from group peak)
                    %[xyz] = spm_XYZreg('NearestXYZ',[ROI_coords{iRoi}],xSPM.XYZmm);
                    [xyz] = xY.xyz;
                    % print to txt to know the peak for each subject and
                    % session
                    
                    if isempty(xyz)
                       W = ['This is participant ' sub{isub} ' and run ' num2str(run{irun}) ' and ROI ' ROI_names{iRoi} 'The ROI is empty.'];
                       disp(W);

                       cd(main_folder);
                       fid=fopen(['gPPI_subjects_without_ROI_JE.txt'],'a');
                       fprintf(fid,'%s %d %s\n',[sub{isub}], [run{irun}], [ROI_names{iRoi}]);                    
                       
                       voi_folder = dir([firstLevelFolder_path filesep 'VOI_' ROI_names{iRoi} '_double_radius_mask.nii']);
                       voi_file = [voi_folder.folder '/' voi_folder.name];
                       movefile(voi_file, PPI_folder_path);

                       voi_folder = dir([firstLevelFolder_path filesep 'VOI_' ROI_names{iRoi} '_double_radius_' num2str(run{irun}) '_eigen.nii']);
                       voi_file = [voi_folder.folder '/' voi_folder.name];
                       movefile(voi_file, PPI_folder_path);
                    else
                        cd(main_folder);
                        fid=fopen(['gPPI_indivVOI_' ROI_names{iRoi} '_th' num2str(threshold{ithresh}) '_5mm_double_radius_JE.txt'],'a');
                        fprintf(fid,'%s %d %d %d %d %d\n',[sub{isub}], [run{irun}], size(xY.XYZmm,2), xyz(1), xyz(2), xyz(3));
 
                        %% move files from first level folder to PPI folder
                        % voi_file = dir([firstLevelFolder_path filesep 'VOI_' ROI_names{iRoi} '_th' num2str(threshold{ithresh}) '_nearest_6mm_FINAL_1.mat']);
                        % for some reason the name of the VOIs is not as called above...
                        voi_folder = dir([firstLevelFolder_path filesep 'VOI_' ROI_names{iRoi} '_double_radius_' num2str(run{irun}) '.mat']);
                        voi_file = [voi_folder.folder '/' voi_folder.name];
                        movefile(voi_file, PPI_folder_path);

                        voi_folder = dir([firstLevelFolder_path filesep 'VOI_' ROI_names{iRoi} '_double_radius_mask.nii']);
                        voi_file = [voi_folder.folder '/' voi_folder.name];
                        movefile(voi_file, PPI_folder_path);

                        voi_folder = dir([firstLevelFolder_path filesep 'VOI_' ROI_names{iRoi} '_double_radius_' num2str(run{irun}) '_eigen.nii']);
                        voi_file = [voi_folder.folder '/' voi_folder.name];
                        movefile(voi_file, PPI_folder_path);
                    end

            end
        end 
    end
end
