# Load packages

``` r
library(here)
library(tidyverse)
library(magrittr)
library(lme4)
```

# Read in data

``` r
GM_df <- read.table(here::here("ROIsGM.txt"), header = T)

GM_df$Subject <- as.factor(GM_df$Subject)
GM_df$Age <- as.factor(GM_df$Age)

GM_df <- gather(GM_df, "ROI", "n_voxels", 2:32)
```

# Plot n of voxels by ROI

``` r
ggplot(GM_df, aes(x = ROI, y = n_voxels, fill = ROI)) +
  geom_boxplot(show.legend = F) +
  geom_hline(yintercept = mean(GM_df$n_voxels), color = "black", linetype = "dotted") +
  labs(y = "N voxels") +
  apatheme +
  theme(legend.position=("none"), 
        legend.title=element_blank(), 
        axis.title.x = element_blank(),
        axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1))
```

![](GM_QC_files/figure-markdown_github/unnamed-chunk-2-1.png)
