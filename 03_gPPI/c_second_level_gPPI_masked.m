%% 2nd level analysis for PPI results %%

%path = '/data/pt_02004/MDN_LANG/Derivatives/group_statistics/gPPI/AE/';
path = '/data/pt_02004/MDN_LANG/Derivatives/group_statistics/gPPI/JE/';
spm_path = '/data/pt_02004/spm12';

GM_mask = '/data/pt_02004/Masks&ROIs/GM_masks/GM_spm_0.3.nii';
      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');

gPPI_folders = {'gPPI_individualVOI_RTempPole_th0.01_5mm','gPPI_individualVOI_RPrec_th0.01_5mm','gPPI_individualVOI_Insula_th0.01_5mm','gPPI_individualVOI_RInsula_th0.01_5mm','gPPI_individualVOI_Insula2_th0.01_5mm'};

for i = 1:numel(gPPI_folders)

data_path = [path filesep gPPI_folders{i}];

%% Categories_Counting
con_path = [data_path filesep 'Categories_Counting_masked'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Categories>Counting';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Counting>Categories';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
    
%% Difficult_Easy
con_path = [data_path filesep 'Difficult_Easy_masked'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Difficult>Easy';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Easy>Difficult';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
end
