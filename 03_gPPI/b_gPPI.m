% gPPI – first level

clear

%% Paths

%Setup the PPPI Toolbox
addpath('/data/pt_02004/spm12/toolbox/PPPI')

%Setup SPM12
addpath('/data/pt_02004/spm12/')

% define main folder
main_folder = '/data/pt_02004/MDN_LANG/Derivatives/subjects';

% define first level directory
firstlevel_dir = 'fmriprep/1st_level_fd';
ppi_folder = 'PPI';

% define group directory for second level
secondlevel_dir = '/data/pt_02004/MDN_LANG/Derivatives/group_statistics/gPPI/AE';

% start up SPM12 in fMRI-modecurrent
spm('Defaults','fMRI');
spm_jobman('initcfg');

% Subject_folders
% OA
%  sub = {'002','003','004','005','006','007','008','009','011', ...
%            '012','013','014','015','016','017','018','019','020', ...
%            '021','022','023','024','025','027','028','029','030','031'};

% YA
sub = {'001_JE','002_JE','003_JE','004_JE','005_JE','006_JE','007_JE','008_JE','009_JE','010_JE','011_JE', ...
          '012_JE','013_JE','014_JE','015_JE','016_JE','017_JE','018_JE','019_JE','020_JE', ...
         '021_JE','022_JE','023_JE','024_JE','025_JE','026_JE','027_JE','028_JE','029_JE','030_JE'};

run = {'1' '2'};

% Seeds from contrast Semantic fluency > Counting
%ROI_names = {'Insula' 'Insula1' 'Insula2' 'Insula3' 'Insula4' 'dACC' 'RInsula' 'RInsula1' 'SPL' 'SPL1' 'SPL2' 'RMFG' 'RMFG1' 'RMFG2' };

% Seeds from contrast Counting > Semantic fluency
ROI_names = {'RTempPole' 'RTempPole1' 'RPrec' 'RPrec1' 'RPrec2' 'MTG' 'MTG1' 'ACing' 'ACing1' 'LAG' 'LAG1' 'LAG2' 'LAG3' 'LAG4' 'RAG' 'RAG1' 'RAG2'}

%% Loop over ROIs and subjects
for iRoi = 1:numel(ROI_names)
    for isub = 1:numel(sub)
        % Display which participant and which run is currently processed
        X = ['This is participant ' sub{isub} ' and ROI ' ROI_names{iRoi}];
        disp(X);
        
        %Set the firstlevel directory
        firstlevel_folder = [main_folder filesep sub{isub} filesep firstlevel_dir];
        VOI_folder = [main_folder filesep sub{isub} filesep ppi_folder filesep 'gPPI_individualVOI_' ROI_names{iRoi} '_th0.01_5mm'];
        VOI_file = [VOI_folder filesep 'VOI_' ROI_names{iRoi} '_double_radius_mask.nii'];
        outdir = VOI_folder;    
        mat_file = dir([VOI_folder filesep 'VOI_' ROI_names{iRoi} '_double_radius_*.mat']);
        
        if isempty(mat_file)
           fprintf('No voxels in the VOI\n');
           continue;
        end 
        
        % Set the second level directory according to ROI
        secondlevel_folder = [secondlevel_dir filesep 'gPPI_individualVOI_' ROI_names{iRoi} '_th0.01_5mm'];
   
        % Configure the PPPI Parameter structure
        P.subject = sub{isub};
        P.directory = firstlevel_folder;
        P.VOI = VOI_file;
        %P.region 
        %P.FLmask 
        %P.VOI = regionfile;
        P.outdir = outdir;
        P.Estimate = 1;
        P.contrast = 8; % use FULL F-contrast (for MDN_LANG first level with fmriprep data: 8)
        P.extract = 'eig';
        P.equalroi = 0;
        P.Tasks = {'1' 'Cat_diff' 'Cat_easy' 'Count_forw' 'Count_backw'}; % define all conditions of interest INCLUDING FILLERS AND ERROR TRIALS
        P.Weights = [];
        P.analysis = 'psy';
        P.method = 'cond';
        P.CompContrasts = 1;
        P.Weighted = 0;
        P.GroupDir = secondlevel_folder;
        P.Contrasts(1).left = {'Cat_diff'};
        P.Contrasts(1).right = {'none'};
        P.Contrasts(1).STAT='T';
        P.Contrasts(1).Weighted=0;
        P.Contrasts(1).MinEvents=10;
        P.Contrasts(1).name='Cat_diff>rest';
        P.Contrasts(2).left={'Cat_easy'};
        P.Contrasts(2).right={'none'};
        P.Contrasts(2).STAT='T';
        P.Contrasts(2).Weighted=0;
        P.Contrasts(2).MinEvents=10;
        P.Contrasts(2).name='Cat_easy>rest';
        P.Contrasts(3).left={'Cat_diff'};
        P.Contrasts(3).right={'Cat_easy'};
        P.Contrasts(3).STAT='T';
        P.Contrasts(3).Weighted=0;
        P.Contrasts(3).MinEvents=10;
        P.Contrasts(3).name='Cat_diff>Cat_easy';
        P.Contrasts(4).left = {'Cat_diff' 'Cat_easy'};
        P.Contrasts(4).right = {'none'};
        P.Contrasts(4).STAT='T';
        P.Contrasts(4).Weighted=0;
        P.Contrasts(4).MinEvents=10;
        P.Contrasts(4).name='Cats>rest';
        P.Contrasts(5).left = {'Cat_diff' 'Cat_easy'};
        P.Contrasts(5).right = {'Count_forw' 'Count_backw'};
        P.Contrasts(5).STAT='T';
        P.Contrasts(5).Weighted=0;
        P.Contrasts(5).MinEvents=10;
        P.Contrasts(5).name='Cats>Counting';
        P.Contrasts(6).left = {'Count_forw' 'Count_backw'};
        P.Contrasts(6).right = {'none'};
        P.Contrasts(6).STAT='T';
        P.Contrasts(6).Weighted=0;
        P.Contrasts(6).MinEvents=10;
        P.Contrasts(6).name='Counting>rest';
        
        %% Run PPPI with the command: PPPI(P)
        PPPI(P)
    
        %% Move PPI stuff to PPI folder from firstlevel folder
        d = datevec(now);
        res = sprintf('%d_%d_%d', d(2), d(3), d(1));
        
        for irun = 1:numel(run)
              
            ppi_firstlevel_folder = dir([firstlevel_folder filesep sub{isub} '_VOI_' ROI_names{iRoi} '_double_radius_mask_session' run{irun} '_cond_PPI_regressors.txt']);
            ppi_firstlevel_file = [ppi_firstlevel_folder.folder '/' ppi_firstlevel_folder.name];
            movefile(ppi_firstlevel_file, outdir);
            
            ppi_firstlevel_folder = dir([firstlevel_folder filesep sub{isub} '_VOI_' ROI_names{iRoi} '_double_radius_mask_session' run{irun} '_cond_PPI_regressors.mat']);
            ppi_firstlevel_file = [ppi_firstlevel_folder.folder '/' ppi_firstlevel_folder.name];
            movefile(ppi_firstlevel_file, outdir);
            
            ppi_firstlevel_folder = dir([firstlevel_folder filesep sub{isub} '_session' run{irun} '_VOI_' ROI_names{iRoi} '_double_radius_mask.mat']);
            ppi_firstlevel_file = [ppi_firstlevel_folder.folder '/' ppi_firstlevel_folder.name];
            movefile(ppi_firstlevel_file, outdir);
            
        end
        
            ppi_firstlevel_folder = dir([firstlevel_folder filesep sub{isub} '_VOI_' ROI_names{iRoi} '_double_radius_mask_psycond_PPI_structure_ ' date '.mat']);
            ppi_firstlevel_file = [ppi_firstlevel_folder.folder '/' ppi_firstlevel_folder.name];
            movefile(ppi_firstlevel_file, outdir);
               
            ppi_firstlevel_folder = dir([firstlevel_folder filesep sub{isub} '_PPPI_' res '.log']);
            ppi_firstlevel_file = [ppi_firstlevel_folder.folder '/' ppi_firstlevel_folder.name];
            movefile(ppi_firstlevel_file, outdir);

    end
end


