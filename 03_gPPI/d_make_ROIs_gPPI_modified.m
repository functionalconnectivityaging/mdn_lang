%% MarsBar – Create ROIs

clear all
% close all

%% Paths

%Setup SPM12
addpath('/data/pt_02004/spm12/')

% define main folder
main_folder = '/data/pt_02004/MDN_LANG/Derivatives/';

% Define ROI folder
spm_folder = [main_folder 'group_statistics/fmriprep_fd_JE/Categories_Counting_masked_covRT/'];

% Output directory
out_folder = [main_folder 'group_statistics/gPPI/ROIs_based_on_YA'];

% start up SPM12 in fMRI-modecurrent
spm('Defaults','fMRI');
spm_jobman('initcfg');

% Start marsbar to make sure spm_get works
addpath /data/pt_02004/spm12/toolbox/marsbar/
marsbar('on')

%% Set general options

sphereRadius = 5; % mm

coord_file = [main_folder 'group_statistics/gPPI/ROIs_YA.txt'];
coords = readtable(coord_file);
coords = table2cell(coords);

%% Make rois
fprintf('\n');

for i=1:size(coords, 1)
    
    this_coord = coords(i, 1:3);
    name_coord = coords(i, 4);

    fprintf('Working on ROI %d/%d: %s ... ', i, size(coords,1), cell2mat(name_coord));

    roi_label = sprintf('%s', cell2mat(name_coord));
    
    % Load cluster ROI for trimming
    cluster_roi = [spm_folder roi_label '_roi.mat'];
    act_roi = maroi('load', cluster_roi); % FWE-corrected at peak level (p<0.05) for Categories>Counting; p<0.001, FWE-corrected at cluster level for Counting>Categories
    
    % Make sphere ROI to do trimming
    sphere_centre = cell2mat(this_coord);
    sphere_radius = sphereRadius;
    sphere_roi = maroi_sphere(struct('centre', sphere_centre, 'radius', sphere_radius));

    % Combine for trimmed ROI
    trim_stim = sphere_roi & act_roi;
    trim_stim = label(trim_stim, roi_label);

    outName = fullfile(out_folder, sprintf('%s_transformed_roi', roi_label));

    % save MarsBaR ROI (.mat) file
    saveroi(trim_stim, [outName '.mat']);

    fprintf('done.\n');

end

fprintf('\nAll done. %d ROIs written to %s.', size(coords,1), out_folder);