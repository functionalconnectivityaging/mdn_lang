% Marsbar batch for ROIs on 1st level PPIs 
% Older adults

clear

%% Paths

%Setup SPM12
addpath('/data/pt_02004/spm12/')

% define main folder
main_folder = '/data/pt_02004/MDN_LANG/Derivatives/';

% Define ROI folder
roi_folder = [main_folder 'group_statistics/gPPI/ROIs_based_on_YA/'];

% define first level directory
subject_folder = [main_folder 'subjects/'];
ppi_folder = 'PPI';

% start up SPM12 in fMRI-modecurrent
spm('Defaults','fMRI');
spm_jobman('initcfg');

% Start marsbar to make sure spm_get works
addpath /data/pt_02004/spm12/toolbox/marsbar/
marsbar('on')

% Subject_folders
sub = {'002','003','004','005','006','007','008','009','011', ...
          '012','013','014','015','016','017','018','019','020', ...
          '021','022','023','024','025','027','028','029','030','031'};
      
ROI_names = {'Insula' 'Insula1' 'Insula2' 'Insula3' 'Insula4' 'dACC'...
    'RInsula' 'RInsula1' 'SPL' 'SPL1' 'SPL2' 'RMFG' 'RMFG1' 'RMFG2'...
    'RTempPole' 'RTempPole1' 'RPrec' 'RPrec1' 'RPrec2' 'MTG' 'MTG1'...
    'ACing' 'ACing1' 'LAG' 'LAG1' 'LAG2' 'LAG3' 'LAG4' 'RAG' 'RAG1' 'RAG2'};

target_ROIs = {'Insula' 'Insula1' 'Insula2' 'Insula3' 'Insula4' 'dACC'...
    'RInsula' 'RInsula1' 'SPL' 'SPL1' 'SPL2' 'RMFG' 'RMFG1' 'RMFG2'...
    'RTempPole' 'RTempPole1' 'RPrec' 'RPrec1' 'RPrec2' 'MTG' 'MTG1'...
    'ACing' 'ACing1' 'LAG' 'LAG1' 'LAG2' 'LAG3' 'LAG4' 'RAG' 'RAG1' 'RAG2'};

%% Make a loop – first subjects, then ROIs
for isub = 1:numel(sub)
   for iRoi = 1:numel(ROI_names)

       fid=fopen(['/data/pt_02004/Beta_weights/ROIs_based_on_YA/beta_weights_' ROI_names{iRoi} '_AE.txt'],'a');
       
       % open text file and write header information
        subjectnum = str2double(sub{isub});       
        if subjectnum == 1
               fprintf(fid,'Subject\tROI_seed\tROI_target\tCats_rest\tCats_Counting\n');
        end
       
        for itarget = 1:numel(target_ROIs)
           
           if iRoi == itarget
              continue
           end
           
           % Display which participant and which ROI is currently processed
           disp(['This is subject ' sub{isub} ' and seed ' ROI_names{iRoi} ' and target ROI ' target_ROIs{itarget}]);
           
           % Get SPM.mat file
           main_sessionfolder = [subject_folder sub{isub} filesep ppi_folder];
           
           % Check if mat file exists, i.e., if subject had any voxels in
           % this ROI; if not, continue in subjects loop
           mat_file = dir([main_sessionfolder filesep 'gPPI_individualVOI_' ROI_names{iRoi} ...
               '_th0.01_5mm' filesep 'VOI_' ROI_names{iRoi} '_double_radius_*.mat']);

           if isempty(mat_file)
               fprintf('No ROI here\n');
               continue;
           end
           
           model_dir = [main_sessionfolder filesep 'gPPI_individualVOI_' ROI_names{iRoi} ...
             '_th0.01_5mm/PPI_VOI_' ROI_names{iRoi} '_double_radius_mask/SPM.mat'];

           % Get _roi.mat
           roi = [roi_folder filesep target_ROIs{itarget} '_transformed_roi.mat'];

           % Make marsbar design object
           D  = mardo(model_dir);
           % Make marsbar ROI object
           R  = maroi(roi);
           % Fetch data into marsbar data object
           Y  = get_marsy(R, D, 'mean');
           % Get contrasts from original design
           xCon = get_contrasts(D);
           % Estimate design on ROI data
           E = estimate(D, Y);
           % Put contrasts from original design back into design object
           E = set_contrasts(E, xCon);
           % get design betas
           b = betas(E);
           % get stats and stuff for all contrasts into statistics structure
           marsS = compute_contrasts(E, 1:length(xCon));
           
%% Write beta weights to text file
           
           fprintf(fid, '%s\t%s\t%s\t%d\t%d\n', [sub{isub}], [ROI_names{iRoi}], [target_ROIs{itarget}], marsS.con(4), marsS.con(5));
 
       end
   end
end
