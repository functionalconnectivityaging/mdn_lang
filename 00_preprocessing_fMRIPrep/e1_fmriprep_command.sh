#!/bin/bash

# This script runs fmriprep on the given participants. The fmriprep_config.sh file contains the data path variables.

source fmriprep_config.sh

for x in 020 023 023 025 026 027 028 029 030; do
(singularity run -B /data/pt_02004/MDN_LANG /data/pt_02004/MDN_LANG/fmriprep/fmriprep-1.2.6-1.simg "$data_path" "$output_dir/"control"$x" --fs-license-file /data/pt_02004/MDN_LANG/fmriprep/license.txt -w "$working_dir/"control"$x" participant --participant-label control"$x" --t2s-coreg --output-space T1w template &)
done
wait
echo "All participants are in the pipeline"
