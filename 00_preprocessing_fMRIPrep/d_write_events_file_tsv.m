function write_events_file_tsv(group, sub)

% group can be 'young' or 'old'
% sub should be a string with leading zero(s)

%% Define important paths
data_dir = '/data/pt_02004/MDN_LANG/Derivatives/subjects/';
run = {'1'};
out_dir = '/data/pt_02004/MDN_LANG/graph_theory/subjects/';

for i = 1:numel(sub)
for x = 1:numel(run)
    
     if strcmp('old', group)
         
        % Display which participant and which run is currently processed
        X = ['This is participant ' sub{i} ' and run ' run{x}];
        disp(X);

        % define the folders where the logfiles can be found
        filename_logfolder = [data_dir sub{i} '/logfiles/'];

%% load mat file
        
        mat_path = dir([filename_logfolder sub{i} '_run-' run{x} '_1st_level_block_design_w.oRest.mat']);
        mat_file = [mat_path.folder '/' mat_path.name];
        a = load(mat_file);
        
%% read data from mat file  

        onsets_var = cell2mat(a.onsets);
        onsets_var_col = onsets_var(:);
    
        trial_type = ["cat_diff";"cat_diff";"cat_diff";"cat_diff";"cat_diff";"cat_easy";"cat_easy";"cat_easy";"cat_easy";"cat_easy";"count_forw";"count_forw";"count_forw";"count_forw";"count_forw";"count_backw";"count_backw";"count_backw";"count_backw";"count_backw"];
        duration = 43;
        duration = repmat(duration, 20, 1);

%% write table

        T = table(onsets_var_col, duration, trial_type, 'VariableNames', {'onset', 'duration', 'trial_type'});
        filename = [out_dir 'sub-' sub{i} '/func/sub-' sub{i} '_task-SWG_run-' run{x} '_echo-2_events.csv'];
        writetable(T, filename, 'Delimiter', '\t');
        

%% rename file to tsv suffix        

        gt_folder = [out_dir 'sub-' sub{i} '/func/'];
        % List all events.csv files
        fileList = dir([gt_folder, '*_events.csv']); 
        % Loop through each found file, copy it and give new extension: .tsv
        for i = 1:numel(fileList)
            file = fullfile(gt_folder, fileList(1).name);
            [tempDir, tempFile] = fileparts(file); 
            status = copyfile(file, fullfile(tempDir, [tempFile, '.tsv']))
            % Delete the .out file; but don't do this until you've backed up your data!!
            delete(file)  
        end
        
% %% copy file to func folder in fmriprep folder for completeness of data set
%         
%         gt_folder = [out_dir 'sub-' sub{i} '/func/'];
%         target_dir = ['/data/pt_02004/MDN_LANG/fmriprep/output/' sub{i} '/fmriprep/sub-' sub{i} '/func/'];
%         % List all events.tsv files
%         fileList = dir([gt_folder, '*_events.tsv']); 
%         % Loop through each found file and copy it to target directory
%         for f = 1:numel(fileList)
%             copyfile(fileList{f}, target_dir); 
%         end

    elseif strcmp('young', group)
        
        % Display which participant and which run is currently processed
        X = ['This is participant ' sub{i} ' and run ' run{x}];
        disp(X);

        % define the folders where the logfiles can be found
        filename_logfolder = [data_dir sub{i} '_JE/logfiles/'];

%% load mat file
        
        mat_path = dir([filename_logfolder sub{i} '_JE_run-' run{x} '_1st_level_block_design.mat']);
        mat_file = [mat_path.folder '/' mat_path.name];
        a = load(mat_file);
        
%% read data from mat file  

        onsets_var = cell2mat(a.onsets);
        onsets_var_col = onsets_var(:);
    
        trial_type = ["cat_diff";"cat_diff";"cat_diff";"cat_diff";"cat_diff";"cat_easy";"cat_easy";"cat_easy";"cat_easy";"cat_easy";"count_forw";"count_forw";"count_forw";"count_forw";"count_forw";"count_backw";"count_backw";"count_backw";"count_backw";"count_backw"];
        duration = 43;
        duration = repmat(duration, 20, 1);

%% write table

        T = table(onsets_var_col, duration, trial_type, 'VariableNames', {'onset', 'duration', 'trial_type'});
        filename = [out_dir 'sub-control' sub{i} '/func/sub-control' sub{i} '_task-SWG_run-' run{x} '_echo-2_events.csv'];
        writetable(T, filename, 'Delimiter', '\t');
        

%% rename file to tsv suffix        

        gt_folder = [out_dir 'sub-control' sub{i} '/func/'];
        % List all events.csv files
        fileList = dir([gt_folder, '*_events.csv']); 
        % Loop through each found file, copy it and give new extension: .tsv
        for i = 1:numel(fileList)
            file = fullfile(gt_folder, fileList(1).name);
            [tempDir, tempFile] = fileparts(file); 
            status = copyfile(file, fullfile(tempDir, [tempFile, '.tsv']))
            % Delete the .out file; but don't do this until you've backed up your data!
            delete(file)  
        end

end
end
end
end