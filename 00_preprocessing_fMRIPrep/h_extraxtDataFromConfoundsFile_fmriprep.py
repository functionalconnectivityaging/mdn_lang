# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 18:22:08 2019

@author: martin
"""
import os
import pandas as pd
import csv

base_dir = '/data/pt_02004/MDN_LANG/fmriprep/output'

participants = ['004','024']
#['002','003','005','006','007','009','011',
#        '012','013','014','015','016','017','019','020',
#        '021','022','023','025','027','028','029','030','031']

runs = ['1', '2']

a = 'sub-{0}'
b = 'sub-{0}_task-SWG_run-{1}_echo-2_desc-confounds_regressors.tsv'
c = 'motion_run-{0}_{1}_AE.txt'
d = 'fd_run-{0}.txt'
e = 'outliers_index_run-{0}.txt'
h = '{0}'
g = '{0}'

#a = 'sub-control{0}'
#b = 'sub-control{0}_task-SWG_run-{1}_echo-2_desc-confounds_regressors.tsv'
#c = 'motion_run-{0}_{1}_JE.txt'
#d = 'fd_run-{0}.txt'
#e = 'outliers_index_run-{0}.txt'
#h = 'control{0}'
#g = '{0}_JE'

for vp in participants:
    for run in runs:
        print('This is participant ' + vp + ' and run ' + run)
        confound_dir = os.path.join(base_dir, h.format(vp), 'fmriprep', a.format(vp), 'func')
        os.chdir(confound_dir)


## This part extracts the 6 motion regressors from the confounds.tsv file written by fmriprep
## The first 29 rows are cut here

        # Read confound file for second echo for each participant
        confound_file = pd.read_csv(b.format(vp, run), sep = '\t', header = 0)
#    
        # Extraxt six motion regressors from data frame
        z = confound_file[['trans_x', 'trans_y', 'trans_z', 'rot_x', 'rot_y', 'rot_z']]
        
        # Cut first 29 rows of data frame    
        y = z[29:]

        y.to_csv(os.path.join('/afs/cbs.mpg.de/tmp/internet/MDN_LANG/Motion', c.format(run, vp)), index = False, header = False)

        # Write data frame to text file, don't write index and header    
#        with open(os.path.join('/data/pt_02004/MDN_LANG/Derivatives/subjects', g.format(vp), 'fmriprep', c.format(run)), 'w') as f:
#            f.write(y.to_string(index = False, header = False))   
#
#
### This part extracts the index numbers from the framewise_displacement column that exceed the value of 0.9
### The first 29 rows are cut here 
#
#        w = confound_file[29:]   
#        x = w[w.framewise_displacement >= 0.9]
#        fd = x[['framewise_displacement']]
#          
#        with open(os.path.join('/data/pt_02004/MDN_LANG/Derivatives/subjects', g.format(vp), 'fmriprep', d.format(run)), 'w') as f:
#              f.write(fd.to_string(header=False))
#            
#        with open(os.path.join('/data/pt_02004/MDN_LANG/Derivatives/subjects', g.format(vp), 'fmriprep', d.format(run))) as f:
#            with open(os.path.join('/data/pt_02004/MDN_LANG/Derivatives/subjects', g.format(vp), 'fmriprep', e.format(run)), 'w') as i:
#                for line in f:
#                      if line.strip():
#                          i.write("\t".join(line.split()[:1]) + "\n")
#            