%%  1st level batch script: Segmentation, Coregistration, Normalisation & Smoothing
% This script performs first level analyses on single subject level

%% Specify paths & folders
%  Data folder
script_path =  '/data/pt_02004/Scripts/Matlab/Preprocessing';
data_path = '/data/pt_02004/MDN_LANG/fmriprep/output/';
spm_path = '/data/pt_02004/spm12';

% Subject_folders
sub = {'002','003','004','005','006','007','008','009','011', ...
        '012','013','014','015','016','017','018','019','020' ...
         '021','022','023','024','025','027','028','029','030'};
run = {'1', '2'};
      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');


%% 
for i = 1:numel(sub)
for x = 1:numel(run)

    % Display which participant and which run is currently processed
    X = ['This is participant ' sub{i} ' and run ' run{x}];
        disp(X);
 
        %% Get volumes
    epi_mainfolder = dir([data_path 'control' sub{i} '/fmriprep/sub-control' sub{i} '/func']);
    curr_epi_subfolder = epi_mainfolder.folder;
    nifti_files = dir([curr_epi_subfolder '/sub-control' sub{i} '_task-SWG_run-' run{x} '_echo-2_space-MNI152NLin2009cAsym_desc-preproc_bold_trimmed.nii']);
    func_images_path = [nifti_files.folder '/' nifti_files.name];
        func_vols = spm_vol(func_images_path);
        Vols = {};
        for iVol = 1:numel(func_vols)
            Vols{iVol} = [func_images_path ',' num2str(iVol)];
        end
        Vols = Vols';
%-----------------------------------------------------------------------
clear matlabbatch
matlabbatch{1}.spm.spatial.smooth.data = Vols;
matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
matlabbatch{1}.spm.spatial.smooth.dtype = 0;
matlabbatch{1}.spm.spatial.smooth.im = 0;
matlabbatch{1}.spm.spatial.smooth.prefix = 's';

%% RUN THE BATCH
spm_jobman('run', matlabbatch)
end
end