# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 14:43:31 2019

@author: martin
"""

import os
import json

base_dir = '/data/pt_02004/MDN_LANG/Nifti/'

participants = ['020','023', '024','025','026','027','028','029','030']
#        ,'001','002','003','004','005','006','007','008','009','010','011',
#       '012',,'023', '024','025','026','027','028','029','030','031']
 
a = 'sub-control{0}' 
       
for vp in participants:
    fmap_dir = os.path.join(base_dir, a.format(vp), 'fmap')   
    os.chdir(fmap_dir)
    
    direction = ['AP', 'PA']    
    
    for direct in direction:
        
        ## This adds information to json files for first run        
        
        entry_run_1 = {'IntendedFor' : ["func/sub-control%s_task-SWG_run-1_echo-1_bold.nii.gz" %vp, 
                                        "func/sub-control%s_task-SWG_run-1_echo-2_bold.nii.gz" %vp]}
        
        with open('sub-control%s_dir-%s_run-1_epi.json' %(vp, direct)) as f:
            text = json.load(f)
            
        text.update(entry_run_1)
        
        with open('sub-control%s_dir-%s_run-1_epi.json' %(vp, direct), 'w') as f:    
            json.dump(text, f, sort_keys=True, indent=4, separators=(',', ':'))
        
        
        ## This adds information to json-files for second run
        
        entry_run_2 = {'IntendedFor' : ["func/sub-control%s_task-SWG_run-2_echo-1_bold.nii.gz" %vp,
                                        "func/sub-control%s_task-SWG_run-2_echo-2_bold.nii.gz" %vp]} 

        with open('sub-control%s_dir-%s_run-2_epi.json' %(vp, direct)) as f:
            text = json.load(f)
            
        text.update(entry_run_2)
        
        with open('sub-control%s_dir-%s_run-2_epi.json' %(vp, direct), 'w') as f:    
            json.dump(text, f, sort_keys=True, indent=4, separators=(',', ':'))
    