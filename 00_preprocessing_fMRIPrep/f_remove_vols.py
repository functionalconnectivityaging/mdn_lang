# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 14:16:51 2019

@author: martin
"""
## This script reads a Nifti 4D file and deletes a specified number of volumes. 
## In the current case, 29 scans are deleted from the beginning of the file since they were used as prepVols for
## the dual-echo combination.

import os
import numpy as np
import nibabel as nb

def strip_vols(in_file):
    nii = nb.load(in_file)
    nii.shape
    trimmed_img = nb.Nifti1Image(nii.get_data()[:,:,:,29:], nii.get_affine())
    trimmed_img.shape
    trimmed_img.set_data_dtype(np.float32)
    nb.save(trimmed_img, in_file[:-7] + '_trimmed.nii.gz')
    
base_dir = '/data/pt_02004/MDN_LANG/fmriprep/output/'
#os.chdir(base_dir)

a = 'sub-control{0}'
b = 'sub-control{0}_task-SWG_run-{1}_echo-{2}_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz'
c = 'control{0}'

participants = ['023','024']
#['001','002','003','004','005','006','007','008','009','010', '011',
#        '012','013','014','015','016','017','018','019','020',
#       '021','022','025','026','027','028','029','030']
# '023', '024',

runs = ['1','2']

echoes = ['2'] #'1'

for vp in participants:
    for run in runs:
        for echo in echoes:
            func_dir = os.path.join(base_dir, c.format(vp), 'fmriprep', a.format(vp), 'func')
            os.chdir(func_dir)
            img_func = b.format(vp, run, echo)
            print('This is participant %s, run %s and echo %s.' %(vp, run, echo))
            img_func_trimmed = strip_vols(img_func)


