# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 11:32:25 2019

@author: martin
"""

# This script takes output identified by artDetect (see /data/pt_02004/ArtifactDetect_python_test/artDetect.py)
# and converts volume numbers into arrays of zeros and ones that can be used as additional regressors 
# This script writes one regressor for each bad volume.

import os
import numpy as np

base_dir = '/data/pt_02004/MDN_LANG/Derivatives/subjects'

participants = ['024']

runs = ['1','2']

a = 'outliers_index_run-{0}.txt'
b = '{0}_JE'

for vp in participants:
    for run in runs:
        
        print('This is participant %s and run %s' %(vp, run))        
        
        path = os.path.join(base_dir, b.format(vp), 'fmriprep')
        os.chdir(path)
        
        with open(a.format(run)) as f:
            content = f.readlines()
        
        content = [x.strip() for x in content]
        
        content = map(int, content)
        content[:] = [x - 29 for x in content]        
        
        f = open('fd_regressors_%s.txt' %run, 'w')   
        firstline = content[0]
        arr = np.zeros((587, 1), dtype = int)
        np.put(arr, [firstline - 1], [1])
        np.savetxt('fd_regressors_%s.txt' %run, arr, fmt='%i')
        
        def appendAsColumn(arr):
            fileContent = np.loadtxt('fd_regressors_%s.txt' %run, dtype = int, ndmin = 2)
            fileContent = np.hstack((fileContent, arr.astype(int)))
            np.savetxt('fd_regressors_%s.txt' %run, fileContent, fmt='%i')
        
        for line in content[1:]:
            arr = np.zeros((587, 1), dtype = int)
            np.put(arr, [line - 1], [1])    
            appendAsColumn(arr)   
            

            
    
    

