# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 14:43:31 2019

@author: martin
"""
# This script adds information of 'TaskName' to json files of functional scans

import os
import json

base_dir = '/data/p_02221/MDN_APH/Nifti/'

participants = ['001','002']
#['001','002','003','004','005','006','007','008','009','010', '011',
#        '012','013','014','015','016','017','018','019','020',
#        '021','022','023', '024','025','026','027','028','029','030','031']

runs = ['1', '2']

a = 'sub-control{0}'     

for vp in participants:
    func_dir = os.path.join(base_dir, a.format(vp), 'func')   
    os.chdir(func_dir)
    
    for run in runs:
            
        entry = {'TaskName': 'Localizer'}
        
        with open('sub-control%s_ses-1_task-Localizer_run-%s_bold.json' %(vp, run)) as f:
            data = json.load(f)
        
        data.update(entry)
        
        with open('sub-control%s_ses-1_task-Localizer_run-%s_bold.json' %(vp, run), 'w') as f: 
            json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))

    