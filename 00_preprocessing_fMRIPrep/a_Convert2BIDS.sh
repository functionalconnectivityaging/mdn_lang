#!/bin/bash
set -x 

#BIDS version 1.1.1
#Sandra Martin, 11/2018
# Modified 09/2019 for control group for MDN_LANG (BIDS version 1.2.1)

#First create directory in source destination 
#Division in Dicom and Nifti and subfolders is according to BIDS recommendation
#If you like, create different folders for each run in Dicom but not in Nifti directory - according to BIDS only sessions but not runs should be in separate folders
#for x in {001..031}; do
#    mkdir -p /data/pt_02004/MDN_LANG/{Dicom,Nifti}/sub-"$x"/{anat,func,fmap}
#done


#Copy raw files into Dicom directory
#Here only T1, fieldMaps and functional images are copied - feel free to add e.g. FLAIR, DWI etc.
#set -x
#for x in {001..031}; do
#   cp /data/pt_02004/MRI_data_raw/"$x"_AE/DICOM/{0011*} /data/pt_02004/MDN_LANG/Dicom/"$x"/anat
#   cp /data/pt_02004/MRI_data_raw/"$x"_AE/DICOM/{0002*,0003*,0006*,0007*} /data/pt_02004/MDN_LANG/Dicom/"$x"/fieldMaps
#   cp /data/pt_02004/MRI_data_raw/"$x"_AE/DICOM/{0004*,0005*} /data/pt_02004/MDN_LANG/Dicom/"$x"/func/run-1
#   cp /data/pt_02004/MRI_data_raw/"$x"_AE/DICOM/{0008*,0009*} /data/pt_02004/MDN_LANG/Dicom/"$x"/func/run-2
#done

#Unpack anatomical image, move them one directory up in anat folder and rm empty directory
for x in 023 024 026 027 028 020 030 025; do
#for x in {004..012}; do
cd /data/pt_02004/MDN_LANG/Dicom/"$x"/anat
find -name "*.tar.gz" -exec tar -xzf {} \;
cd "$(find -mindepth 1 -type d)"
mv * ..
cd ..
rm -rf "$(find -mindepth 1 -type d)"
done

#Convert Dicom images to Nifti using dcm2niix
#This will result in a packaged .tar.gz for the Niftis as well as a .json-file with metadata
#for x in 001 003; do
for x in 023 024 026 027 028 029 030; do
#for x in {004..012}; do

# Anatomy
"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -o /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/anat -f "sub-control"$x"_T1w" "/data/pt_02004/MDN_LANG/Dicom/"$x"/anat"

# Functional scans
"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n 5 -o /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/func -f "sub-control"$x"_task-SWG_run-1_echo-%e_bold" "/data/pt_02004/MDN_LANG/Dicom/"$x"/func/run-1"
"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n 4 -o /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/func -f "sub-control"$x"_task-SWG_run-1_sbref" "/data/pt_02004/MDN_LANG/Dicom/"$x"/func/run-1"
"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n 9 -o /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/func -f "sub-control"$x"_task-SWG_run-2_echo-%e_bold" "/data/pt_02004/MDN_LANG/Dicom/"$x"/func/run-2"
"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n 8 -o /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/func -f "sub-control"$x"_task-SWG_run-2_sbref" "/data/pt_02004/MDN_LANG/Dicom/"$x"/func/run-2"

# Fieldmaps
"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n 2 -o /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/fmap -f "sub-control"$x"_dir-PA_run-1_epi" "/data/pt_02004/MDN_LANG/Dicom/"$x"/fieldMaps"
"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n 3 -o /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/fmap -f "sub-control"$x"_dir-AP_run-1_epi" "/data/pt_02004/MDN_LANG/Dicom/"$x"/fieldMaps"
"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n 6 -o /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/fmap -f "sub-control"$x"_dir-PA_run-2_epi" "/data/pt_02004/MDN_LANG/Dicom/"$x"/fieldMaps"
"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n 7 -o /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/fmap -f "sub-control"$x"_dir-AP_run-2_epi" "/data/pt_02004/MDN_LANG/Dicom/"$x"/fieldMaps"
done

for x in 023 024 026 027 028 029 030; do
#for x in {004..012};do
for y in 1 2; do
mv /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/func/sub-control"$x"_task-SWG_run-"$y"_sbref_e2.nii.gz /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/func/sub-control"$x"_task-SWG_run-"$y"_sbref.nii.gz
mv /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/func/sub-control"$x"_task-SWG_run-"$y"_sbref_e2.json /data/pt_02004/MDN_LANG/Nifti/sub-control"$x"/func/sub-control"$x"_task-SWG_run-"$y"_sbref.json
done
done
