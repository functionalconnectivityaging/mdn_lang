# Age-dependent contribution of domain-general network interactions to semantic cognition

This repository holds behavioral source data and analysis code used in our paper *Age-dependent contribution of domain-general network interactions to semantic cognition* (Martin, Saur, & Hartwigsen): https://doi.org/10.1101/2020.11.06.371153

## Content

- 00_preprocessing_fMRIPrep/SPM  
Contains scripts for preprocessing data with both software packages/pipelines. Please note that fMRIPrep results were used for subsequent analyses. Preprocessing with SPM was applied as a reliability check.

- 01_firstLevel  
Contains scripts for individual first level analyses run in SPM12.

- 02_secondLevel  
Contains scripts for second level group analyses in SPM12.

- 03_gPPI  
Contains scripts for generalized psychophysiological interaction analyses using the gPPI toolbox (McLaren et al., 2012) in SPM12.

- 04_stats  
Contains behavioral raw data and extracted beta weights for regions of interest in domain-general systems. Also contains scripts for statistical models run in R.


